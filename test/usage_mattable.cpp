/*
 * License Agreement for CodedLight
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <mvg/logger.h>
#include "../core/common.h"
#include "../core/mattable.h"

using namespace std;
using namespace cv;
using namespace cl3ds;

static void tryAddingMat(const MatTable &table)
{
    Mat m = table["a"];
    logInfo() << "was added with const version";
}

static void tryAddingMat(MatTable &table)
{
    Mat m = table["b"];
    logInfo() << "was added with non const version";
}

int main()
{
    MatTable table;
    Mat      match = table.camMatchMat();
    logInfo() << match;

    match.create(3, 3, CV_64F);
    randu(match, Scalar::all(-5), Scalar::all(5));
    logInfo() << table.camMatchMat();

    Mat &projMatch = table.projMatchMat();
    logInfo() << projMatch;
    projMatch = match;
    logInfo() << projMatch;

    Mat toto(2, 2, CV_8U);
    randu(toto, Scalar::all(0), Scalar::all(255));
    projMatch = toto;
    logInfo() << toto;

    tryAddingMat(table);
    tryAddingMat(MatTable());
}
