/*
 * License Agreement for CodedLight
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "../core/common.h"
#include "../patterns/generators.h"
#include "../patterns/generator_p.h"

using namespace std;
using namespace cv;
using namespace cl3ds;

struct MyGenerator : public Generator
{
    CL3DS_PRIV_IMPL
    CL3DS_CLASS_NAME("mygenerator")

    MyGenerator() : Generator(new Private(this)) { }
    ~MyGenerator();

    friend int main();

    protected:
        struct Private : public AbstractPrivate
        {
            Private(MyGenerator *gen) : AbstractPrivate(gen) { }
            ~Private();

            bool read(const FileNode &) { return true; }
            bool write(FileStorage &) const { return true; }
            bool configure(istream *,
                           ostream *) { return true; }
            size_t hash() const { return 0; }
            const string & className() const { return name; }

            bool calledNext;
            void next() { calledNext = true; logInfo() << "called next"; }

            bool calledGet;
            Mat get() { calledGet = true; logInfo() << "called get"; return Mat(); }

            bool calledReset;
            void reset() { calledReset = true; logInfo() << "called reset"; }

            mutable bool calledCount;
            size_t count() const { calledCount = true; return 2; }

            Size size() const { return Size(); }
            int type() const { return 0; }

            string name;
        };

};
MyGenerator::~MyGenerator() { }
MyGenerator::Private::~Private() { }

int main()
{
    Ptr<Generator>       gen;
    const vector<string> gens = Generator::classFactory()->keys();

    for (size_t i = 0; i < gens.size(); ++i)
    {
        // create default generator
        configure(gen, 0, 0, gens[i]);
        logInfo() << "Default generator" << gens[i] << "created";

        gen->reset();

        size_t h = gen->hash();
        logInfo() << "Hash:" << h;

        // get some patterns if possible
        for (size_t j = 0; j < size_t(5) && gen->hasNext(); ++j)
        {
            Mat tmp = gen->get();
            if (tmp.size() != gen->size())
            {
                logWarning() << "Generated pattern has wrong size" << tmp.size() <<
                    gen->size();

                return 1;
            }
        }

        // write it
        string buf;
        {
            FileStorage fs(".xml", FileStorage::WRITE + FileStorage::MEMORY);
            fs << "generator" << gen;
            buf = fs.releaseAndGetString();
        }
        logInfo() << "Default generator" << gens[i] << "written";

        // read it back ..
        {
            FileStorage fs(buf, FileStorage::READ + FileStorage::MEMORY);
            fs["generator"] >> gen;
        }
        logInfo() << "Default generator" << gens[i] << "read";
        size_t h2 = gen->hash();
        if (h != h2)
        {
            logWarning() << "Hash mismatched" << h2 << h;

            return 1;
        }
    }

    MyGenerator myGen;
    myGen.reset();
    if (!myGen.privPtr()->calledReset)
    {
        logError() << "Reset was not called..";
    }

    myGen.privPtr()->calledCount = false;
    logInfo() << "calling count";
    myGen.hasNext();
    if (!myGen.privPtr()->calledCount)
    {
        logError() << "Count was not called..";
    }

    myGen.privPtr()->calledGet = false;
    logInfo() << "calling get";
    myGen.get(false);
    if (!myGen.privPtr()->calledGet)
    {
        logError() << "Get was not called..";
    }

    myGen.privPtr()->calledGet = false;
    logInfo() << "calling get again";
    myGen.get(false);
    if (myGen.privPtr()->calledGet)
    {
        logError() << "Get was called..";
    }

    myGen.privPtr()->calledCount = false;
    myGen.privPtr()->calledNext  = false;
    myGen.privPtr()->calledGet   = false;
    logInfo() << "calling get and next";
    myGen.get();
    if (myGen.privPtr()->calledGet)
    {
        logError() << "Get was called..";
    }
    if (!myGen.privPtr()->calledCount)
    {
        logError() << "Count was called..";
    }
    if (!myGen.privPtr()->calledNext)
    {
        logError() << "Next was not called..";
    }

    myGen.privPtr()->calledCount = false;
    myGen.privPtr()->calledNext  = false;
    myGen.privPtr()->calledGet   = false;
    logInfo() << "calling get and next";
    myGen.get();
    if (!myGen.privPtr()->calledGet)
    {
        logError() << "Get was not called..";
    }
    if (!myGen.privPtr()->calledCount)
    {
        logError() << "Count was not called..";
    }
    if (!myGen.privPtr()->calledNext)
    {
        logError() << "Next was not called..";
    }

    SequenceBuffer buf;
    logInfo() << buf.hash();

    vector<Mat> images(2);
    images[0].create(3, 3, CV_8U);
    images[1].create(3, 3, CV_16U);
    SequenceBuffer buf2(images.begin(), images.end());
    logInfo() << buf2.hash();

    Ptr<GammaFilter> filter = makePtr<GammaFilter>();
    filter->setGamma(2);
    size_t h1 = filter->hash();

    Ptr<GrayCodeGenerator> gc = makePtr<GrayCodeGenerator>();
    filter->setGenerator(gc);
    size_t h2 = filter->hash();

    filter->setGamma(1);
    size_t h3 = filter->hash();

    logInfo() << h1 << h2 << h3;
    if ((h1 == h2) && (h1 == h3))
    {
        logInfo() << "Hash should mismatch";
    }

    ElapsedTimer t;
    images.resize(10);
    for (size_t i = 0; i < 10; ++i)
    {
        images[i].create(Size(2000, 2000), CV_8UC3);
        randu(images[i], Scalar::all(0), Scalar::all(255));
    }
    Ptr<SequenceBuffer> vecGen = makePtr<SequenceBuffer>(images.begin(), images.end());
    t.restart();
    logInfo() << vecGen->hash() << t.elapsed() << "ms";

    for (size_t i = 0; i < 10; ++i)
    {
        images[i].create(Size(1280, 800), CV_8UC3);
        randu(images[i], Scalar::all(0), Scalar::all(255));
    }
    vecGen = makePtr<SequenceBuffer>(images.begin(), images.end());
    t.restart();
    logInfo() << vecGen->hash() << t.elapsed() << "ms";

    return 0;
}

