/*
 * License Agreement for CodedLight
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "../core/common.h"
#include "../patterns/seq_reader.h"
#include "../patterns/graycode.h"

using namespace cl3ds;
using namespace cv;

static int w, h;

int main()
{
    SequenceReader  reader /*(the actual parameters of the reader)*/;
    GrayCodeMatcher matcher /*(the actual parameters of the gray code matching method)*/;

    // computes the match between the camera captured images and the projector
    // implicitly described patterns : no generator needed for the projector
    MatTable result = matcher.match(&reader, 0);

    // access the camera-projector correspondence map
    Mat match = result.camMatchMat();

    // extract the correspondence for a particular point (xc, yc) in the camera image
    int   xc  = 10, yc = 20;
    Vec3w cor = match.at<Vec3w>(xc, yc); // match is a 3 channels 16 bit color image
    // represented by a cv::Vec3w object

    // convert it to floating point coordinates using the projector dimension w x h
    double xp, yp;
    pointFromMatch(cor, Size(w, h), xp, yp);

    return 0;
}
