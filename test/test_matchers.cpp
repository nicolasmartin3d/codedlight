/*
 * License Agreement for CodedLight
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "../core/common.h"
#include "../patterns/matchers.h"

using namespace std;
using namespace cv;
using namespace cl3ds;

int main()
{
    Ptr<Matcher>         matcher;
    const vector<string> matchers = Matcher::classFactory()->keys();

    for (size_t i = 0; i < matchers.size(); ++i)
    {
        // create default matcher
        logInfo() << "matcher is " << static_cast<void *>(matcher);
        configure(matcher, 0, 0, matchers[i]);
        logInfo() << "Default matcher" << matchers[i] << "created";

        size_t h = matcher->hash();
        logInfo() << "Hash:" << h;

        string buf;
        // write it
        {
            FileStorage fs(".xml", FileStorage::WRITE + FileStorage::MEMORY);
            fs << "matcher" << matcher;
            buf = fs.releaseAndGetString();
        }
        logInfo() << "Default matcher" << matchers[i] << "written";

        // read it back ..
        {
            FileStorage fs(buf, FileStorage::READ + FileStorage::MEMORY);
            fs["matcher"] >> matcher;
        }
        logInfo() << "Default matcher" << matchers[i] << "read" << endl;

        size_t h2 = matcher->hash();
        if (h != h2)
        {
            logWarning() << "Hash mismatched" << h2 << h;

            return 1;
        }
    }

    return 0;
}
