if( NOT cmd )
    message( FATAL_ERROR "Variable cmd not defined" )
endif( NOT cmd )

if( NOT args )
    set (args "")
    #message( FATAL_ERROR "Variable args not defined" )
endif( NOT args )
separate_arguments(args)

if( NOT true_output)
    message( FATAL_ERROR "Variable true_output not defined" )
endif( NOT true_output)

if( NOT output)
    message( FATAL_ERROR "Variable output not defined" )
endif( NOT output)

if (genfile)
    if (NOT input)
        execute_process(COMMAND ${cmd} ${args} 
            RESULT_VARIABLE different)
    else (NOT input)
        execute_process(COMMAND ${cmd} ${args} 
            INPUT_FILE ${input}
            RESULT_VARIABLE different)
    endif (NOT input)
else (genfile)
    if (NOT input)
        execute_process(COMMAND ${cmd} ${args} 
            OUTPUT_FILE ${output}
            RESULT_VARIABLE different)
    else (NOT input)
        execute_process(COMMAND ${cmd} ${args} 
            OUTPUT_FILE ${output}
            RESULT_VARIABLE different
            INPUT_FILE ${input})
    endif (NOT input)
endif (genfile)

if( different )
    message( SEND_ERROR "Could not execute ${cmd} ${out} ${err}")
endif( different )

execute_process(COMMAND ${CMAKE_COMMAND}
    -E compare_files ${true_output} ${output}
    RESULT_VARIABLE different
    OUTPUT_QUIET
    ERROR_QUIET
    )

if( different )
    message( SEND_ERROR "${output} does not match ${true_output}!" )
endif( different )
