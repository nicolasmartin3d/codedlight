/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GRAYCODE_H
#define GRAYCODE_H

#include "generator.h"
#include "matcher.h"

namespace cl3ds
{
    /** Implements Gray codes structured light generation. */
    class CL3DS_DECLSPEC GrayCodeGenerator : public Generator
    {
        public:
            enum GrayCodeType { DEFAULT, LONG_RUN };
            enum BinarizationMethod {
                BLACK_AND_WHITE,
                PER_PIXEL_THRESHOLD,
                INVERSE_PATTERNS
            };

            GrayCodeGenerator(cv::Size           size=defaultSize(),
                              Direction          direction=defaultDirection(),
                              GrayCodeType       codeType=defaultCodeType(),
                              BinarizationMethod method=defaultBinarizationMethod());
            ~GrayCodeGenerator();

            Direction direction() const;
            void setDirection(const Direction &dir);
            static Direction defaultDirection();

            /** Pattern size of this Gray code generator. */
            cv::Size size() const;
            void setSize(cv::Size size);
            static cv::Size defaultSize();

            GrayCodeType codeType() const;
            void setCodeType(GrayCodeType type);
            static GrayCodeType defaultCodeType();

            BinarizationMethod binarizationMethod() const;
            void setBinarizationMethod(BinarizationMethod method);
            static BinarizationMethod defaultBinarizationMethod();

            /** The number of bits used to encode a Gray code is basically log2(dimension)
             *  where dimension corresponds to the pattern width or height. */
            size_t nbBitsX() const;
            size_t nbBitsY() const;
            size_t nbBits() const;

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("graycode")
    };

    /** Implements Gray codes structured light matching. */
    class CL3DS_DECLSPEC GrayCodeMatcher : public Matcher
    {
        public:
            GrayCodeMatcher(const cv::Ptr<GrayCodeGenerator> &generator=
                                nonOwningPtr(defaultGenerator()));
            ~GrayCodeMatcher();

            /** Current Gray code parameters of this matcher. */
            GrayCodeGenerator * generator() const;
            /** Sets the Gray code parameters for matching. */
            void setGenerator(const cv::Ptr<GrayCodeGenerator> &);
            /** Typical Gray code default generation parameters, mostly used for
             *  creating default matcher. */
            static GrayCodeGenerator * defaultGenerator();

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("graycode")
    };
}

#endif /* #ifndef GRAYCODE_H */
