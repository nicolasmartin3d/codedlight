/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/highgui/highgui.hpp>
#include <mvg/logger.h>
#include "generator_p.h"
#include "dummy.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
    struct DummyGenerator::Private : public AbstractPrivate
    {
        Private(DummyGenerator *ptr) :
            AbstractPrivate(ptr)
        { }
        ~Private();

        bool read(const FileNode &)
        {
            return true;
        }

        bool write(FileStorage &) const
        {
            return true;
        }

        bool configure(istream *,
                       ostream *)
        {
            return true;
        }

        size_t hash() const
        {
            return 0;
        }

        void next()
        { }

        Mat get()
        {
            return Mat();
        }

        void reset()
        { }

        size_t count() const
        {
            return 0;
        }

        Size size() const
        {
            return Size();
        }

        int type() const
        {
            return 0;
        }

        CL3DS_PUB_IMPL(DummyGenerator)
    };

    DummyGenerator::Private::~Private() { }

    DummyGenerator::DummyGenerator() :
        Generator(new Private(this))
    { }

    DummyGenerator::~DummyGenerator() { }
}
