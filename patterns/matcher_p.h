/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MATCHER_P_H
#define MATCHER_P_H

#include "../core/object_p.h"
#include "../core/matdispatcher.h"
#include "matcher.h"

namespace cl3ds
{
    /** Base class for an opaque implementation of a Matcher. */
    struct CL3DS_DECLSPEC Matcher::AbstractPrivate : public Object::AbstractPrivate
    {
        AbstractPrivate(Matcher *matcher);
        ~AbstractPrivate();

        /** Matching with knowledge of epipolar geometry simplifies the routine by
         *  making use of the scene geometry to only compute correspondences in one
         *  direction indicated by the last parameters : useHorizontalPatterns. */
        virtual MatTable matchWithEpipolarGeometry(
            Generator *const                 camGenerator,
            Generator *const                 projGenerator,
            const cv::Mat                   &camMask,
            const cv::Mat                   &projMask,
            const CameraGeometricParameters &camParameters,
            const CameraGeometricParameters &projParameters,
            bool                             useHorizPatterns)
            = 0;

        /** Matching without knowledge of epipolar geometry requires the method to
         *  find correspondences with more information (most of the time twice as much).
         **/
        virtual MatTable matchWithoutEpipolarGeometry(Generator *const camGenerator,
                                                      Generator *const projGenerator,
                                                      const cv::Mat   &camMask,
                                                      const cv::Mat   &projMask) = 0;
    };

    /** Typical data needed for a Matcher to implement its matching routine. */
    struct MatcherData
    {
        MatcherData(Generator *const                 camGenerator,
                    Generator *const                 projGenerator,
                    const cv::Mat                   &camMask,
                    const cv::Mat                   &projMask,
                    const CameraGeometricParameters &camParameters=CameraGeometricParameters(),
                    const CameraGeometricParameters &projParameters=CameraGeometricParameters(),
                    bool                             useHorizontalPatterns=true);

        Generator *const                 camGenerator;
        Generator *const                 projGenerator;
        const cv::Mat                   &camMask;
        const cv::Mat                   &projMask;
        const CameraGeometricParameters &camParameters;
        const CameraGeometricParameters &projParameters;
        bool                             useHorizontalPatterns;
    };

    namespace internal
    {
        // This struct is used internally to dispatch to
        // Matcher::Private::matchWithEpipolarGeometry<T>
        template <class Executor>
        struct KnownEpipolarGeometryMatcherProxy
        {
            KnownEpipolarGeometryMatcherProxy(Executor *p) : ptr(p) { }
            template <class T> MatTable operator()(const MatcherData &data)
            {
                return ptr->template matchWithEpipolarGeometry<T>(data);
            }
            Executor *ptr;
        };
    }

    /** Helper function to call with an Executor that defines a template function
     * matchWithEpipolarGeometry<T>.
     *  Uses MatDispatcher under the hood, but simplifies Matcher::Private implementation
     **/
    template <class Executor>
    MatTable dispatchWithGeometry(Executor          *exec,
                                  const MatcherData &data,
                                  int                type)
    {
        typedef internal::KnownEpipolarGeometryMatcherProxy<Executor> Proxy;
        Proxy                                                 p(exec);
        MatDispatcher1D<Proxy, const MatcherData &, MatTable> dispatcher(p);

        return dispatcher(type, data);
    }

    /** Helper function to call with an Executor that defines a template function
     * matchWithEpipolarGeometry<T1, T2>.
     *  Uses MatDispatcher under the hood, but simplifies Matcher::Private implementation
     **/
    template <class Executor>
    MatTable dispatchWithGeometry(Executor          *exec,
                                  const MatcherData &data,
                                  int                type1,
                                  int                type2)
    {
        typedef internal::KnownEpipolarGeometryMatcherProxy<Executor> Proxy;
        Proxy                                                 p(exec);
        MatDispatcher2D<Proxy, const MatcherData &, MatTable> dispatcher(p);

        return dispatcher(type1, type2, data);
    }

    namespace internal
    {
        // This struct is used internally to dispatch to
        // Matcher::Private::matchWithoutEpipolarGeometry<T>
        template <class Executor>
        struct UnknownEpipolarGeometryMatcherProxy
        {
            UnknownEpipolarGeometryMatcherProxy(Executor *p) : ptr(p) { }
            template <class T> MatTable operator()(const MatcherData &data)
            {
                return ptr->template matchWithoutEpipolarGeometry<T>(data);
            }
            Executor *ptr;
        };
    }

    /** Helper function to call with an Executor that defines a template function
     * matchWithoutEpipolarGeometry<T>.
     *  Uses MatDispatcher under the hood, but simplifies Matcher::Private implementation
     **/
    template <class Executor>
    MatTable dispatchWithoutGeometry(Executor          *exec,
                                     const MatcherData &data,
                                     int                type)
    {
        typedef internal::UnknownEpipolarGeometryMatcherProxy<Executor> Proxy;
        Proxy                                                 p(exec);
        MatDispatcher1D<Proxy, const MatcherData &, MatTable> dispatcher(p);

        return dispatcher(type, data);
    }

    /** Helper function to call with an Executor that defines a template function
     * matchWithoutEpipolarGeometry<T1, T2>.
     *  Uses MatDispatcher under the hood, but simplifies Matcher::Private implementation
     **/
    template <class Executor>
    MatTable dispatchWithoutGeometry(Executor          *exec,
                                     const MatcherData &data,
                                     int                type1,
                                     int                type2)
    {
        typedef internal::UnknownEpipolarGeometryMatcherProxy<Executor> Proxy;
        Proxy                                                 p(exec);
        MatDispatcher2D<Proxy, const MatcherData &, MatTable> dispatcher(p);

        return dispatcher(type1, type2, data);
    }
}

#endif // MATCHER_P_H

