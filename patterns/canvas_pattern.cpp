/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/highgui/highgui.hpp>
#include "../core/parameter.h"
#include "generator.h"
#include "pattern_p.h"
#include "canvas_pattern.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
    struct CanvasPattern::Private : public AbstractPrivate
    {
        Private(CanvasPattern *canvas,
                Scalar         c) :
            AbstractPrivate(canvas)
        {
            paramColor = TypeParameter<Scalar>("color",
                                               "Color of the canvas",
                                               CanvasPattern::defaultColor());
            setColor(c);
        }
        ~Private();

        Scalar color() const
        {
            return paramColor;
        }

        void setColor(Scalar c)
        {
            paramColor = c;
            nbColors   = 1;
            if ((std::abs(c[0] - c[1]) < 1) &&
                (std::abs(c[1] - c[2]) < 1)) { nbColors = 1; }
            else if (c[3] > 0) { nbColors = 4; }
            else { nbColors = 3; }
        }

        bool read(const FileNode &node)
        {
            paramColor.read(node);
            setColor(paramColor);

            return true;
        }

        bool write(FileStorage &fs) const
        {
            paramColor.write(fs);

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            paramColor.configure(is, os);
            setColor(paramColor);

            return true;
        }

        size_t hash() const
        {
            return Hasher() << paramColor;
        }

        Mat get()
        {
            return Mat(pubPtr()->size(), CV_MAKE_TYPE(CV_8U, nbColors), paramColor);
        }

        int type() const
        {
            return CV_MAKE_TYPE(CV_8U, nbColors);
        }

        int                   nbColors;
        TypeParameter<Scalar> paramColor;

        CL3DS_PUB_IMPL(CanvasPattern)
    };

    CanvasPattern::Private::~Private()
    { }

    CanvasPattern::CanvasPattern(Size   size,
                                 Scalar color) :
        Pattern(new Private(this, color), size)
    { }

    CanvasPattern::~CanvasPattern()
    { }

    Scalar CanvasPattern::color() const
    {
        CL3DS_PRIV_CONST_PTR(CanvasPattern);

        return priv->color();
    }

    void CanvasPattern::setColor(Scalar color)
    {
        CL3DS_PRIV_PTR(CanvasPattern);

        priv->setColor(color);
    }

    Scalar CanvasPattern::defaultColor()
    {
        return Scalar::all(0);
    }
}
