/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENERATOR_H
#define GENERATOR_H

#include <opencv2/core/core.hpp>
#include "../core/object.h"
#include "../core/factory.h"

namespace cl3ds
{
    /** Base class for pattern generation. */
    class CL3DS_DECLSPEC Generator : public Object
    {
        public:
            /** Enumeration used by generators who can generates horizontal
             *  and/or vertical directions of their patterns. */
            enum Direction {
                HORIZONTAL = 0x1,
                VERTICAL   = 0x2,
                BOTH       = HORIZONTAL | VERTICAL
            };

            /** The factory responsible for creating generators based on
             *  associated class names. */
            static Factory<std::string, Generator> * classFactory();

            virtual ~Generator();

            /** Resets the generator to its initial state (i.e to the first pattern). */
            virtual void reset();

            /** Changes the generator state to the next pattern. */
            virtual void next();

            /** Generates the next pattern in the sequence.
             *  Note that the pattern generated depends on the current position and the
             *  parameters of the generator (like the direction chosen, size of the
             *  pattern, etc...). Refer to the documentation of the generator to
             *  determine in which order the patterns are generated. */
            virtual cv::Mat get(bool callNext=true);

            /** Returns the index of the pattern the generator is currently generating
             *  through a call to get(). */
            virtual size_t index() const;

            /** Returns the number of patterns the generator can produce. */
            virtual size_t count() const;

            /** Returns true while the generator can generate new patterns. */
            virtual bool hasNext() const;

            /** Returns the size of a generated pattern. */
            virtual cv::Size size() const;

            /** Returns the OpenCV type of a generated pattern. */
            virtual int type() const;

        protected:
            struct AbstractPrivate;
            Generator(AbstractPrivate *ptr);

        protected:
            void setPrivateImplementation(AbstractPrivate *pimpl);
            AbstractPrivate * privateImplementation();
            const AbstractPrivate * privateImplementation() const;

        private:
            struct Private;
    };
}

#endif /* #ifndef GENERATOR_H */
