/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GRADIENT_H
#define GRADIENT_H

#include "generator.h"

namespace cl3ds
{
    /** A generator that stores sequence of image in memory. */
    class CL3DS_DECLSPEC GradientGenerator : public Generator
    {
        public:
            /** Empty buffer. */
            GradientGenerator(cv::Size size=defaultSize(),
                              int      min=defaultMin(),
                              int      max=defaultMax(),
                              int      steps=defaultSteps());
            ~GradientGenerator();

            int min() const;
            void setMin(int min);
            static int defaultMin();

            int max() const;
            void setMax(int max);
            static int defaultMax();

            int steps() const;
            void setSteps(int steps);
            static int defaultSteps();

            cv::Size size() const;
            void setSize(cv::Size size);
            static cv::Size defaultSize();

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("gradient")
    };
}

#endif /* #ifndef GRADIENT_H */
