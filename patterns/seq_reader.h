/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SEQ_READER_H
#define SEQ_READER_H

#include "generator.h"

namespace cl3ds
{

    /** A generator that reads sequence of image from disk. */
    class CL3DS_DECLSPEC SequenceReader : public Generator
    {
        public:
            /** Will read length images from a certain offset and by
             *  a certain step. Each image path is found by formatting the given
             *  string pattern with the current index. */
            SequenceReader(const std::string &pathFormat=defaultPathFormat(),
                           int                length=defaultLength(),
                           int                offset=defaultOffset(),
                           int                step=defaultStep());
            ~SequenceReader();

            /** Format for the images path (must include a %d for the index). */
            const std::string & pathFormat() const;
            void setPathFormat(const std::string &pathFormat);
            static std::string defaultPathFormat();

            /** Length of the sequence = number of images that will be read. */
            int length() const;
            void setLength(int length);
            static int defaultLength();

            /** Offset from which the index count will begin in the format. */
            int offset() const;
            void setOffset(int offset);
            static int defaultOffset();

            /** Step by which the index is incremented in the format. */
            int step() const;
            void setStep(int step);
            static int defaultStep();

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("seqreader")
    };
}

#endif /* #ifndef SEQ_READER_H */
