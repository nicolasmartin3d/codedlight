/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace cl3ds
{

    struct ContinuousImageTransposer : public cv::ParallelLoopBody
    {
        ContinuousImageTransposer(Generator *const g,
                                  cv::Mat         &d,
                                  int              o) :
            gen(g),
            dst(d),
            offset(o) { }
        ~ContinuousImageTransposer();

        virtual void operator()(const cv::Range &range) const
        {
            gen->reset();
            for (int i = 0; i < offset; ++i)
            {
                gen->next();
            }
            for (int i = range.start; i < range.end; ++i)
            {
                cv::Mat row = dst.row(i);
                // flatten the matrix to one row and n columns
                gen->get().reshape(1, 1).convertTo(row, dst.type());
            }
        }

        Generator *const gen;
        cv::Mat         &dst;
        int              offset;
    };

    template <typename T, typename U>
    struct ImageTransposer : public cv::ParallelLoopBody
    {
        ImageTransposer(Generator *const g,
                        cv::Mat         &d,
                        int              n,
                        int              o) :
            gen(g),
            dst(d),
            nbPixels(n),
            offset(o) { }

        virtual void operator()(const cv::Range &range) const
        {
            gen->reset();
            for (int i = 0; i < offset; ++i)
            {
                gen->next();
            }
#if defined(_MSC_VER) && _MSC_VER >= 1400
#pragma warning(push)
#pragma warning(disable:4996)
#endif
            for (int i = range.start; i < range.end; ++i)
            {
                cv::Mat                  m    = gen->get();
                cv::MatConstIterator_<T> mIt  = m.begin<T>(), mItE = m.end<T>();
                cv::MatIterator_<U>      mItD = dst.begin<U>() + i * nbPixels;
                std::copy(mIt, mItE, mItD);
            }
#if defined(_MSC_VER) && _MSC_VER >= 1400
#pragma warning(pop)
#endif
        }

        Generator *const gen;
        cv::Mat         &dst;
        int              nbPixels;
        int              offset;
    };

    template <typename T, typename U>
    void transposeImages(Generator *const gen,
                         cv::Mat         &dst,
                         int              count,
                         int              offset)
    {
        if (count < 0)
        {
            count = static_cast<int>(gen->count());
        }

        gen->reset();
        cv::Mat tmp = gen->get(false);
        if (tmp.empty())
        {
            logError() << "Got empty matrix from generator";
        }

        cv::Size size     = tmp.size();
        int      nbPixels = size.width * size.height;
        dst.create(count, nbPixels, MatType2Int<U>::value);

        const bool matContinuous = tmp.isContinuous();
        if (matContinuous)
        {
            ContinuousImageTransposer transposer(gen, dst, offset);
            // parallel_for_(cv::Range(0,count), transposer);
            /** until we find a way to make thread safe generators .. this is not a good idea */
            transposer(cv::Range(0, count));
        }
        else
        {
            ImageTransposer<T, U> transposer(gen, dst, offset, nbPixels);
            // parallel_for_(cv::Range(0,count), transposer);
            /** until we find a way to make thread safe generators .. this is not a good idea */
            transposer(cv::Range(0, count));
        }
        transpose(dst, dst);
    }

    template <typename T>
    void transposeImages(Generator *const gen,
                         cv::Mat         &dst,
                         int              count,
                         int              offset)
    {
        if (count < 0)
        {
            count = static_cast<int>(gen->count());
        }

        gen->reset();
        cv::Mat tmp = gen->get(false);
        if (tmp.empty())
        {
            logError() << "Got empty matrix from generator";
        }

        cv::Size size     = tmp.size();
        int      nbPixels = size.width * size.height;
        dst.create(count, nbPixels, MatType2Int<T>::value);

        const bool matContinuous = tmp.isContinuous();
        if (matContinuous)
        {
            ContinuousImageTransposer transposer(gen, dst, offset);
            /** until we find a way to make thread safe generators .. this is not a good idea */
            // parallel_for_(cv::Range(0,count), transposer);
            transposer(cv::Range(0, count));
        }
        else
        {
            ImageTransposer<T, T> transposer(gen, dst, offset, nbPixels);
            /** until we find a way to make thread safe generators .. this is not a good idea */
            // parallel_for_(cv::Range(0,count), transposer);
            transposer(cv::Range(0, count));
        }
        transpose(dst, dst);
    }

}
