/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CANVAS_PATTERN_H
#define CANVAS_PATTERN_H

#include "pattern.h"

namespace cl3ds
{
    /** A single image generator from a color. */
    class CL3DS_DECLSPEC CanvasPattern : public Pattern
    {
        public:
            CanvasPattern(cv::Size   size=defaultSize(),
                          cv::Scalar color=defaultColor());
            ~CanvasPattern();

            cv::Scalar color() const;
            void setColor(cv::Scalar color);
            static cv::Scalar defaultColor();

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("canvas")
    };
}

#endif /* #ifndef CANVAS_H */
