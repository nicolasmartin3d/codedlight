/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "generator_p.h"
#include "generators.h"

using namespace std;
using namespace cv;

namespace cl3ds
{
#define PRIV_PTR Private * const priv = \
    static_cast<Private *>(Object::privateImplementation());
#define PRIV_CONST_PTR const Private * const priv = \
    static_cast<const Private *>(Object::privateImplementation());

    struct Generator::Private : public Object::AbstractPrivate
    {
        Private(Generator                  *pubImpl,
                Generator::AbstractPrivate *privImpl) :
            Object::AbstractPrivate(pubImpl),
            ind(0),
            dirtyCurrentPattern(true),
            impl(privImpl)
        { }
        ~Private();

        void checkImplPtr() const
        {
            if (!impl)
            {
                logError() << "Private pointer is null ! "
                    "You must pass a valid pointer to "
                    "the private implementation of your "
                    "object to the Object constructor";
            }
        }

        bool read(const cv::FileNode &node)
        {
            checkImplPtr();

            return impl->read(node);
        }

        bool write(cv::FileStorage &fs) const
        {
            checkImplPtr();

            return impl->write(fs);
        }

        bool configure(std::istream *is,
                       std::ostream *os)
        {
            checkImplPtr();

            return impl->configure(is, os);
        }

        size_t hash() const
        {
            checkImplPtr();

            return impl->hash();
        }

        bool hasNext() const
        {
            return ind < count();
        }

        void next()
        {
            if (!hasNext())
            {
                logWarning() << "No more patterns, unable to call next.";
            }

            ind++;
            dirtyCurrentPattern = true;

            checkImplPtr();

            impl->next();
        }

        void reset()
        {
            ind                 = 0;
            dirtyCurrentPattern = true;

            checkImplPtr();
            impl->reset();
        }

        Mat get(bool callNext)
        {
            if (!hasNext())
            {
                logWarning() << "No more patterns to generate.";

                return Mat();
            }

            checkImplPtr();

            if (dirtyCurrentPattern)
            {
                currentPattern = impl->get();
                if (currentPattern.type() != type())
                {
                    logError() << "The pattern generated is not of "
                        "the same type as the ones previously "
                        "generated.";
                }
                if (currentPattern.size() != size())
                {
                    logError() << "The pattern generated is not of "
                        "the same size as the ones previously "
                        "generated.";
                }
                dirtyCurrentPattern = false;
            }
            if (callNext) { next(); }

            return currentPattern;
        }

        size_t index() const
        {
            return ind;
        }

        size_t count() const
        {
            checkImplPtr();

            return impl->count();
        }

        int type() const
        {
            checkImplPtr();

            return impl->type();
        }

        Size size() const
        {
            checkImplPtr();

            return impl->size();
        }

        size_t                      ind;
        bool                        dirtyCurrentPattern;
        Mat                         currentPattern;
        Generator::AbstractPrivate *impl;
    };

    Generator::Private::~Private()
    {
        delete impl;
    }

    Generator::Generator(AbstractPrivate *ptr) :
        Object(new Private(this, ptr))
    { }

    Generator::~Generator()
    { }

    void Generator::setPrivateImplementation(Generator::AbstractPrivate *pimpl)
    {
        PRIV_PTR;

        delete priv->impl;
        priv->impl = pimpl;
    }

    Generator::AbstractPrivate * Generator::privateImplementation()
    {
        PRIV_PTR;

        return priv->impl;
    }

    const Generator::AbstractPrivate * Generator::privateImplementation() const
    {
        PRIV_CONST_PTR;

        return priv->impl;
    }

    static Ptr< Factory<string, Generator> > makeFactory()
    {
        Ptr< Factory<string, Generator> > f = makePtr< Factory<string, Generator> >();
        f->reg<DummyGenerator>();
        f->reg<CanvasPattern>();
        f->reg<GammaFilter>();
        f->reg<Combiner>();
        f->reg<GradientGenerator>();
        f->reg<SequenceReader>();
        f->reg<SequenceBuffer>();
        f->reg<GrayCodeGenerator>();
        f->reg<PhaseShiftGenerator>();
        f->reg<XorBinaryGenerator>();

        return f;
    }

    Factory<string, Generator> *Generator::classFactory()
    {
        static Ptr< Factory<string, Generator> > factory = makeFactory();

        return factory;
    }

    bool Generator::hasNext() const
    {
        PRIV_CONST_PTR;

        return priv->hasNext();
    }

    void Generator::next()
    {
        PRIV_PTR;

        priv->next();
    }

    void Generator::reset()
    {
        PRIV_PTR;

        priv->reset();
    }

    Mat Generator::get(bool callNext)
    {
        PRIV_PTR;

        return priv->get(callNext);
    }

    size_t Generator::index() const
    {
        PRIV_CONST_PTR;

        return priv->index();
    }

    size_t Generator::count() const
    {
        PRIV_CONST_PTR;

        return priv->count();
    }

    int Generator::type() const
    {
        PRIV_CONST_PTR;

        return priv->type();
    }

    Size Generator::size() const
    {
        PRIV_CONST_PTR;

        return priv->size();
    }

    Generator::AbstractPrivate::AbstractPrivate(Generator *generator) :
        Object::AbstractPrivate(generator)
    { }

    Generator::AbstractPrivate::~AbstractPrivate()
    { }
}
