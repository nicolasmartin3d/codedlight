/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/highgui/highgui.hpp>
#include <mvg/logger.h>
#include "../core/parameter.h"
#include "generator_p.h"
#include "gradient.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
    struct GradientGenerator::Private : public AbstractPrivate
    {
        Private(GradientGenerator *ptr,
                Size               size,
                int                min,
                int                max,
                int                steps) :
            AbstractPrivate(ptr)
        {
            paramSize = TypeParameter<Size>("size",
                                            "Size of the pattern to generate",
                                            GradientGenerator::defaultSize(),
                                            makePtr< BoundConstraint<Size> >(Size(0,
                                                                                  0),
                                                                             Size(10000,
                                                                                  10000)));
            paramSize = size;
            paramMin  = TypeParameter<int>("min",
                                           "Minimum value of the gradient",
                                           GradientGenerator::defaultMin(),
                                           makePtr< BoundConstraint<int> >(-1, 256));
            paramMin = min;
            paramMax = TypeParameter<int>("max",
                                          "Maximum value of the gradient",
                                          GradientGenerator::defaultMax(),
                                          makePtr< BoundConstraint<int> >(-1, 256));
            paramMax   = max;
            paramSteps = TypeParameter<int>("steps",
                                            "Number of steps of the gradient",
                                            GradientGenerator::defaultSteps(),
                                            makePtr< PositiveConstraint<int> >(256));
            paramSteps = steps;
        }
        ~Private();

        bool read(const FileNode &node)
        {
            paramSize.read(node);
            paramMin.read(node);
            paramMax.read(node);
            paramSteps.read(node);

            return true;
        }

        bool write(FileStorage &fs) const
        {
            paramSize.write(fs);
            paramMin.write(fs);
            paramMax.write(fs);
            paramSteps.write(fs);

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            paramSize.configure(is, os);
            paramMin.configure(is, os);
            paramMax.configure(is, os);
            paramSteps.configure(is, os);

            return true;
        }

        size_t hash() const
        {
            return Hasher() << paramSize
                            << paramMin
                            << paramMax
                            << paramSteps;
        }

        void next()
        { }

        Mat get()
        {
            const double step =
                (paramMax - paramMin) / static_cast<double>(paramSteps - 1);

            return Mat(paramSize, CV_8U,
                       Scalar::all(static_cast<uchar>(pubPtr()->index() * step)));
        }

        void reset()
        { }

        size_t count() const
        {
            return static_cast<size_t>(paramSteps);
        }

        Size size() const
        {
            return paramSize;
        }

        int type() const
        {
            return CV_8U;
        }

        TypeParameter<Size> paramSize;
        TypeParameter<int>  paramMin;
        TypeParameter<int>  paramMax;
        TypeParameter<int>  paramSteps;

        CL3DS_PUB_IMPL(GradientGenerator)
    };

    GradientGenerator::Private::~Private() { }

    GradientGenerator::GradientGenerator(Size size,
                                         int  min,
                                         int  max,
                                         int  steps) :
        Generator(new Private(this, size, min, max, steps))
    { }

    GradientGenerator::~GradientGenerator()
    { }

    int GradientGenerator::min() const
    {
        CL3DS_PRIV_CONST_PTR(GradientGenerator);

        return priv->paramMin;
    }

    void GradientGenerator::setMin(int min)
    {
        CL3DS_PRIV_PTR(GradientGenerator);

        priv->paramMin = min;
    }

    int GradientGenerator::defaultMin()
    {
        return 0;
    }

    int GradientGenerator::max() const
    {
        CL3DS_PRIV_CONST_PTR(GradientGenerator);

        return priv->paramMax;
    }

    void GradientGenerator::setMax(int max)
    {
        CL3DS_PRIV_PTR(GradientGenerator);

        priv->paramMax = max;
    }

    int GradientGenerator::defaultMax()
    {
        return 255;
    }

    int GradientGenerator::steps() const
    {
        CL3DS_PRIV_CONST_PTR(GradientGenerator);

        return priv->paramSteps;
    }

    void GradientGenerator::setSteps(int steps)
    {
        CL3DS_PRIV_PTR(GradientGenerator);

        priv->paramSteps = steps;
    }

    int GradientGenerator::defaultSteps()
    {
        return 10;
    }

    Size GradientGenerator::size() const
    {
        CL3DS_PRIV_CONST_PTR(GradientGenerator);

        return priv->paramSize;
    }

    void GradientGenerator::setSize(Size size)
    {
        CL3DS_PRIV_PTR(GradientGenerator);

        priv->paramSize = size;
    }

    Size GradientGenerator::defaultSize()
    {
        return Size(800, 600);
    }
}
