/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SEQ_OPERATIONS_HPP
#define SEQ_OPERATIONS_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <mvg/logger.h>
#include "../core/mattable.h"
#include "generator.h"

namespace cl3ds
{
    class CL3DS_DECLSPEC SequenceStats
    {
        public:
            SequenceStats();

            bool minAsked() const;
            SequenceStats & min();

            bool maxAsked() const;
            SequenceStats & max();

            bool amplAsked() const;
            SequenceStats & ampl();

            bool meanAsked() const;
            SequenceStats & mean();

            bool devAsked() const;
            SequenceStats & dev();

            bool medAsked() const;
            SequenceStats & med();

        private:
            bool op(int oper) const;
            int m_operations;
    };

	CL3DS_DECLSPEC MatTable computeSequenceStats(Generator *const gen,
                                  SequenceStats    stats);

    template <class T>
    void transposeImages(Generator *const gen,
                         cv::Mat         &dst,
                         int              count=-1,
                         int              offset=0);

    template <class T, class U>
    void transposeImages(Generator *const gen,
                         cv::Mat         &dst,
                         int              count=-1,
                         int              offset=0);
}

#include "seq_operations.tpp"

#endif /* #ifndef SEQ_OPERATIONS_HPP */
