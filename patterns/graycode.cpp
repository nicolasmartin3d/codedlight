/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/highgui/highgui.hpp>
#include "../core/parameter.h"
#include "seq_operations.h"
#include "seq_buffer.h"
#include "generator_p.h"
#include "matcher_p.h"
#include "graycode.h"
#include "graycode_p.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
    typedef GrayCodeGenerator::BinarizationMethod BinarizationMethod;
    typedef GrayCodeGenerator::GrayCodeType       GrayCodeType;
    typedef GrayCodeGenerator::Direction          Direction;

    struct GrayCodeGenerator::Private : public AbstractPrivate
    {
        Private(GrayCodeGenerator *ptr,
                Size               size,
                Direction          direction,
                GrayCodeType       type,
                BinarizationMethod method) :
            AbstractPrivate(ptr)
        {
            paramSize = TypeParameter<Size>(
                "size",
                "Size of the pattern to generate",
                GrayCodeGenerator::defaultSize(),
                makePtr< BoundConstraint<Size> >(Size(0, 0), Size(10000, 10000)));

            setSize(size);

            paramBinarizationMethod = TypeParameter<int>(
                "bin-method",
                "Type of binarization :\n"
                "\t0 -> projects black and white pattern to estimate per pixel threshold\n"
                "\t1 -> estimates best threshold from min and max values of captured images\n"
                "\t2 -> projects inverted patterns and binarize based on sign of differences\n",
                GrayCodeGenerator::defaultBinarizationMethod(),
                makePtr< BoundConstraint<int> >(-1, 3));
            paramBinarizationMethod = method;

            paramCodeType = TypeParameter<int>(
                "code-type",
                "Type of Gray code : 0->default, 1->maximum long run",
                GrayCodeGenerator::defaultCodeType(),
                makePtr< BoundConstraint<int> >(-1, 2));
            paramCodeType = type;
            enc.type      = paramCodeType;

            vector<int> valuesDirection;
            valuesDirection.push_back(Generator::HORIZONTAL);
            valuesDirection.push_back(Generator::VERTICAL);
            valuesDirection.push_back(Generator::BOTH);
            paramDirection =
                TypeParameter<int>("direction",
                                   "Direction for the pattern",
                                   GrayCodeGenerator::defaultDirection(),
                                   makePtr< EnumerationConstraint<int> >(valuesDirection));
            paramDirection = direction;
        }
        ~Private();

        void next()
        {
            switch (paramBinarizationMethod)
            {
                case GrayCodeGenerator::BLACK_AND_WHITE:
                {
                    if (!black) { black = true; }
                    else if (!white) { white = true; }
                    else { bit++; }
                    break;
                }
                case GrayCodeGenerator::INVERSE_PATTERNS:
                {
                    if (inv) { bit++; }
                    inv = !inv;
                    break;
                }
                case GrayCodeGenerator::PER_PIXEL_THRESHOLD:
                {
                    bit++;
                    break;
                }
            }
            if (((paramDirection == Generator::VERTICAL) && (bit >= nbBitsY)) ||
                ((paramDirection != Generator::VERTICAL) && (bit >= nbBitsX)))
            {
                bit = 0;
                inv = false;
                dir = Generator::VERTICAL;
                enc.setNbBits(nbBitsY);
            }
        }

        /** Patterns are generated in the order : b0x-i0x-b1x-i1x-...b0y-i0y-b1y-i1y-...
         *  where b<d>x is the dth bit of the Gray code in the horizontal direction
         *        i<d>x is the dth inverse bit of the Gray code in the horizontal direction
         *        b<d>y is the dth bit of the Gray code in the vertical direction
         *        i<d>y is the dth inverse bit of the Gray code in the vertical direction. */
        Mat get()
        {
            if (paramBinarizationMethod == GrayCodeGenerator::BLACK_AND_WHITE)
            {
                if (!black) { return Mat(paramSize, CV_8U, Scalar::all(0)); }
                else if (!white) { return Mat(paramSize, CV_8U, Scalar::all(255)); }
            }

            Mat pattern;

            Size size = static_cast<Size>(paramSize);
            bool vert = (dir == Generator::VERTICAL);
            pattern.create(Size(vert ? size.width : size.height,
                                vert ? size.height : size.width), CV_8U);

            MatIterator_<uchar> it = pattern.begin<uchar>();
            for (size_t j = 0; j < static_cast<size_t>(pattern.rows); ++j)
            {
                uchar val = enc.bit(j, bit, inv) * 255;
                fill(it, it + pattern.cols, val);
                it += pattern.cols;
            }

            if (!vert) { pattern = pattern.t(); }

            return pattern;
        }

        void reset()
        {
            bit = 0;
            inv = false;
            dir = static_cast<Direction>(static_cast<int>(paramDirection));
            enc.setNbBits(dir == Generator::VERTICAL ? nbBitsY : nbBitsX);
        }

        void setSize(Size size)
        {
            paramSize = size;
            nbBitsX   = static_cast<size_t>(ceil(log2(size.width)));
            nbBitsY   = static_cast<size_t>(ceil(log2(size.height)));
            nbBits    = nbBitsX + nbBitsY;
        }

        Size size() const
        {
            return paramSize;
        }

        int type() const
        {
            return CV_8U;
        }

        bool read(const FileNode &node)
        {
            paramSize.read(node);
            setSize(paramSize);
            paramDirection.read(node);
            paramBinarizationMethod.read(node);
            paramCodeType.read(node);
            enc.type = paramCodeType;

            return true;
        }

        bool write(FileStorage &fs) const
        {
            paramSize.write(fs);
            paramDirection.write(fs);
            paramBinarizationMethod.write(fs);
            paramCodeType.write(fs);

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            if (os) { *os << "Configuring Gray code generator parameters" << endl; }
            paramSize.configure(is, os);
            setSize(paramSize);
            paramDirection.configure(is, os);
            paramBinarizationMethod.configure(is, os);
            paramCodeType.configure(is, os);
            enc.type = paramCodeType;

            return true;
        }

        size_t hash() const
        {
            return Hasher() << paramSize
                            << paramDirection
                            << paramBinarizationMethod
                            << paramCodeType;
        }

        size_t count() const
        {
            size_t c = 0;

            switch (paramDirection)
            {
                case GrayCodeGenerator::HORIZONTAL:
                {
                    c = nbBitsX;
                    break;
                }
                case GrayCodeGenerator::VERTICAL:
                {
                    c = nbBitsY;
                    break;
                }
                case GrayCodeGenerator::BOTH:
                {
                    c = nbBitsX + nbBitsY;
                    break;
                }
            }

            if (paramBinarizationMethod == GrayCodeGenerator::BLACK_AND_WHITE)
            {
                c += 2;
            }
            else if (paramBinarizationMethod == GrayCodeGenerator::INVERSE_PATTERNS)
            {
                c *= 2;
            }

            return c;
        }

        bool            inv;
        bool            black;
        bool            white;
        size_t          bit;
        Direction       dir;
        GrayCodeEncoder enc;

        size_t nbBitsX;
        size_t nbBitsY;
        size_t nbBits;

        TypeParameter<Size> paramSize;
        TypeParameter<int>  paramDirection;
        TypeParameter<int>  paramCodeType;
        TypeParameter<int>  paramBinarizationMethod;

        CL3DS_PUB_IMPL(GrayCodeGenerator)
    };

    GrayCodeGenerator::Private::~Private()
    { }

    GrayCodeGenerator::GrayCodeGenerator(Size               size,
                                         Direction          direction,
                                         GrayCodeType       codeType,
                                         BinarizationMethod method) :
        Generator(new Private(this, size, direction, codeType, method))
    { }

    GrayCodeGenerator::~GrayCodeGenerator()
    { }

    Direction GrayCodeGenerator::direction() const
    {
        CL3DS_PRIV_CONST_PTR(GrayCodeGenerator);

        return static_cast<Direction>(int(priv->paramDirection));
    }

    void GrayCodeGenerator::setDirection(const Direction &dir)
    {
        CL3DS_PRIV_PTR(GrayCodeGenerator);

        priv->paramDirection = dir;
    }

    Direction GrayCodeGenerator::defaultDirection()
    {
        return HORIZONTAL;
    }

    Size GrayCodeGenerator::size() const
    {
        CL3DS_PRIV_CONST_PTR(GrayCodeGenerator);

        return priv->size();
    }

    BinarizationMethod GrayCodeGenerator::binarizationMethod() const
    {
        CL3DS_PRIV_CONST_PTR(GrayCodeGenerator);

        return static_cast<BinarizationMethod>(
            static_cast<int>(priv->paramBinarizationMethod));
    }

    void GrayCodeGenerator::setBinarizationMethod(BinarizationMethod method)
    {
        CL3DS_PRIV_PTR(GrayCodeGenerator);

        priv->paramBinarizationMethod = method;
    }

    BinarizationMethod GrayCodeGenerator::defaultBinarizationMethod()
    {
        return PER_PIXEL_THRESHOLD;
    }

    void GrayCodeGenerator::setSize(Size size)
    {
        CL3DS_PRIV_PTR(GrayCodeGenerator);

        priv->setSize(size);
    }

    Size GrayCodeGenerator::defaultSize()
    {
        return Size(800, 600);
    }

    GrayCodeType GrayCodeGenerator::codeType() const
    {
        CL3DS_PRIV_CONST_PTR(GrayCodeGenerator);

        return static_cast<GrayCodeType>(static_cast<int>(priv->paramCodeType));
    }

    void GrayCodeGenerator::setCodeType(GrayCodeType type)
    {
        CL3DS_PRIV_PTR(GrayCodeGenerator);

        priv->paramCodeType = type;
    }

    GrayCodeType GrayCodeGenerator::defaultCodeType()
    {
        return DEFAULT;
    }

    size_t GrayCodeGenerator::nbBitsX() const
    {
        CL3DS_PRIV_CONST_PTR(GrayCodeGenerator);

        return priv->nbBitsX;
    }

    size_t GrayCodeGenerator::nbBitsY() const
    {
        CL3DS_PRIV_CONST_PTR(GrayCodeGenerator);

        return priv->nbBitsY;
    }

    size_t GrayCodeGenerator::nbBits() const
    {
        CL3DS_PRIV_CONST_PTR(GrayCodeGenerator);

        return priv->nbBits;
    }

    struct GrayCodeMatcher::Private : public AbstractPrivate
    {
        Private(GrayCodeMatcher              *ptr,
                const Ptr<GrayCodeGenerator> &generator) :
            AbstractPrivate(ptr),
            gen(generator)
        { }
        ~Private();

        template <class T> MatTable matchWithEpipolarGeometry(const MatcherData &);
        MatTable matchWithEpipolarGeometry(Generator *const                 camGenerator,
                                           Generator *const                 projGenerator,
                                           const Mat                       &camMask,
                                           const Mat                       &projMask,
                                           const CameraGeometricParameters &camParameters,
                                           const CameraGeometricParameters &projParameters,
                                           bool                             useHorizontalPatterns)
        {
            MatcherData data(camGenerator, projGenerator,
                             camMask, projMask,
                             camParameters, projParameters,
                             useHorizontalPatterns);

            return dispatchWithGeometry(this, data, camGenerator->type());
        }

        template <class T> MatTable matchWithoutEpipolarGeometry(const MatcherData &);
        MatTable matchWithoutEpipolarGeometry(Generator *const camGenerator,
                                              Generator *const projGenerator,
                                              const Mat       &camMask,
                                              const Mat       &projMask)
        {
            MatcherData data(camGenerator, projGenerator,
                             camMask, projMask);

            return dispatchWithoutGeometry(this, data, camGenerator->type());
        }

        template <class T>
        void decode(const Mat         &camMask,
                    BinarizationMethod method,
                    GrayCodeType       type,
                    SequenceBuffer    &buf,
                    size_t             pos,
                    const MatTable    &result,
                    size_t             nbBits,
                    Mat               &codes);

        bool read(const FileNode &node)
        {
            // the global read function will possibly read embedded parameters
            // or read from another file if a configFilePath and configNodeName
            // tags are available
            node["generator"] >> gen;

            return true;
        }

        bool write(FileStorage &fs) const
        {
            // the global write function will possibly embed the parameters
            // or link to another file if a configFilePath() and configNodeName()
            // are available
            fs << "generator" << gen;

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            if (os) { *os << "Configuring Gray code matcher parameters" << endl; }
            // the global configure function will allocate the pointer for us
            // and possibly read the parameters from another file if they are not embedded
            // in this one;
            cl3ds::configure(gen, is, os,
                             string(),
                             makePtr<GrayCodeGenerator>());

            return true;
        }

        size_t hash() const
        {
            return Hasher() << gen->hash();
        }

        Ptr<GrayCodeGenerator> gen;
        GrayCodeDecoder        dec;

        CL3DS_PUB_IMPL(GrayCodeMatcher)
    };

    GrayCodeMatcher::Private::~Private()
    { }

    template <class T>
    void GrayCodeMatcher::Private::decode(const Mat         &camMask,
                                          BinarizationMethod method,
                                          GrayCodeType       type,
                                          SequenceBuffer    &buf,
                                          size_t             pos,
                                          const MatTable    &result,
                                          size_t             nbBits,
                                          Mat               &codes)
    {
        Size   camSize = camMask.size();
        RNGBit rng;
        dec.type = type;
        dec.setNbBits(nbBits);

        for (size_t i = 0; i < nbBits; ++i)
        {
            Mat m  = buf[pos++];
            Mat tm = (method == GrayCodeGenerator::INVERSE_PATTERNS ?
                      buf[pos++] : result["thresh"]);

            for (int y = 0; y < camSize.height; ++y)
            {
                const uchar *mptr = (camMask.empty() ? 0 : camMask.ptr<uchar>(y));
                ushort      *cptr = codes.ptr<ushort>(y);
                const T     *xptr = m.ptr<T>(y);
                const T     *tptr = tm.ptr<T>(y);

                for (size_t x = 0; x < static_cast<size_t>(camSize.width); ++x)
                {
                    if (mptr && !mptr[x]) { continue; }
                    if (xptr[x] > tptr[x])
                    {
                        cptr[x] |= (1 << i);
                    }
                    else if (std::abs<double>(xptr[x] - tptr[x]) <= 1.0)
                    {
                        // random si les valeurs sont égales
                        if (rng()) { cptr[x] |= (1 << i); }
                    }
                    // else nothing to do
                }
            }
        }

        MatIterator_<ushort> it;
        for (it = codes.begin<ushort>(); it != codes.end<ushort>(); ++it)
        {
            *it = static_cast<ushort>(dec(*it));
        }
    }

    template <class T>
    MatTable GrayCodeMatcher::Private::matchWithEpipolarGeometry(const MatcherData &data)
    {
        MatTable result;

        Generator *const camGen = data.camGenerator;
        Size             camSize, projSize;
        camSize  = camGen->size();
        projSize = gen->size();

        BinarizationMethod method = gen->binarizationMethod();
        SequenceBuffer     buf(camGen, gen->count());
        threshold<T>(method, buf, result);

        GrayCodeType type    = gen->codeType();
        const Mat   &camMask = data.camMask;
        size_t       pos     = 0;
        if (method == GrayCodeGenerator::BLACK_AND_WHITE)
        {
            // skip black and white
            pos = 2;
        }

        // only do one direction !
        bool   useHorizontalPatterns = data.useHorizontalPatterns;
        size_t nbBits                =
            (useHorizontalPatterns ? gen->nbBitsX() : gen->nbBitsY());
        Mat codes(camSize, CV_16U, Scalar::all(0));
        decode<T>(camMask, method, type, buf, pos, result, nbBits, codes);

        Mat camMatch(camSize, CV_16UC3);
        for (int y = 0; y < camSize.height; ++y)
        {
            const uchar *mptr = (camMask.empty() ? 0 : camMask.ptr<uchar>(y));
            Vec3w       *ptr  = camMatch.ptr<Vec3w>(y);
            ushort      *ptrC = codes.ptr<ushort>(y);

            for (int x = 0; x < camSize.width; ++x)
            {
                if (mptr && !mptr[x]) { continue; }
                pointToMatch(useHorizontalPatterns ? ptrC[x] : 0,
                             useHorizontalPatterns ? 0 : ptrC[x],
                             projSize, ptr[x]);
            }
        }

        // if neeeded by the caller ...
        if (useHorizontalPatterns)
        {
            codes.convertTo(codes, CV_16U, 65535. / projSize.width, 0);
            result.camHorizontalCodes() = codes;
        }
        else
        {
            codes.convertTo(codes, CV_16U, 65535. / projSize.height, 0);
            result.camVerticalCodes() = codes;
        }
        result.camMatchMat() = camMatch;

        return result;
    }

    template <class T>
    MatTable GrayCodeMatcher::Private::matchWithoutEpipolarGeometry(
        const MatcherData &data)
    {
        MatTable result;

        Generator *const camGen = data.camGenerator;
        Size             camSize, projSize;
        camSize  = camGen->size();
        projSize = gen->size();

        BinarizationMethod method = gen->binarizationMethod();
        SequenceBuffer     buf(camGen, gen->count());
        threshold<T>(method, buf, result);

        GrayCodeType type    = gen->codeType();
        const Mat   &camMask = data.camMask;
        size_t       pos     = 0;
        if (method == GrayCodeGenerator::BLACK_AND_WHITE)
        {
            // skip black and white
            pos = 2;
        }

        size_t nbBitsX = gen->nbBitsX();
        size_t nbBitsY = gen->nbBitsY();
        Mat    codesX(camSize, CV_16U, Scalar::all(0));
        Mat    codesY(camSize, CV_16U, Scalar::all(0));
        decode<T>(camMask, method, type, buf, pos, result, nbBitsX, codesX);
        decode<T>(camMask, method, type, buf, pos + nbBitsX, result, nbBitsY, codesY);

        Mat camMatch(camSize, CV_16UC3);
        for (int y = 0; y < camSize.height; ++y)
        {
            const uchar *mptr = (camMask.empty() ? 0 : camMask.ptr<uchar>(y));
            Vec3w       *ptr  = camMatch.ptr<Vec3w>(y);
            ushort      *ptrX = codesX.ptr<ushort>(y);
            ushort      *ptrY = codesY.ptr<ushort>(y);

            for (int x = 0; x < camSize.width; ++x)
            {
                if (mptr && !mptr[x]) { continue; }
                pointToMatch(ptrX[x], ptrY[x], projSize, ptr[x]);
            }
        }

        // if neeeded by the caller ...
        codesX.convertTo(codesX, CV_16U, 65535. / projSize.width, 0);
        codesY.convertTo(codesY, CV_16U, 65535. / projSize.height, 0);

        result.camHorizontalCodes() = codesX;
        result.camVerticalCodes()   = codesY;
        result.camMatchMat()        = camMatch;

        return result;
    }

    GrayCodeMatcher::GrayCodeMatcher(const Ptr<GrayCodeGenerator> &generator) :
        Matcher(new Private(this, generator))
    { }

    GrayCodeMatcher::~GrayCodeMatcher()
    { }

    GrayCodeGenerator * GrayCodeMatcher::generator() const
    {
        CL3DS_PRIV_CONST_PTR(GrayCodeMatcher);

        return priv->gen;
    }

    void GrayCodeMatcher::setGenerator(const Ptr<GrayCodeGenerator> &gen)
    {
        CL3DS_PRIV_PTR(GrayCodeMatcher);

        priv->gen = gen;
    }

    GrayCodeGenerator * GrayCodeMatcher::defaultGenerator()
    {
        static Ptr<GrayCodeGenerator> gen = makePtr<GrayCodeGenerator>();

        return gen;
    }
}

