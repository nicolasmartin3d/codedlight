/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "generator_p.h"
#include "filter_p.h"
#include "dummy.h"

using namespace std;
using namespace cv;

namespace cl3ds
{

#define CONST_PRIV_PTR const Private * priv = \
    static_cast<const Private *>(Generator::privateImplementation());
#define PRIV_PTR Private * priv = \
    static_cast<Private *>(Generator::privateImplementation());

    struct Filter::Private : public Generator::AbstractPrivate
    {
        Private(Filter                  *p,
                Filter::AbstractPrivate *i,
                Ptr<Generator>           g) :
            Generator::AbstractPrivate(p),
            gen(g),
            impl(i)
        { }
        ~Private();

        void checkGenPtr() const
        {
            if (gen.empty())
            {
                logError() << "No generator to filter";
            }
        }

        void checkImplPtr() const
        {
            if (!impl)
            {
                logError() << "Private pointer is null ! "
                    "You must pass a valid pointer to "
                    "the private implementation of your "
                    "object to the Filter constructor";
            }
        }

        bool read(const FileNode &node)
        {
            node["filteredGenerator"] >> gen;

            checkImplPtr();

            return impl->read(node);
        }

        bool write(cv::FileStorage &fs) const
        {
            checkGenPtr();

            fs << "filteredGenerator" << gen;

            checkImplPtr();

            return impl->write(fs);
        }

        bool configure(std::istream *is,
                       std::ostream *os)
        {
            if (os) { *os << "Which generator should be filtered ?" << endl; }
            cl3ds::configure(gen, is, os,
                             string(),
                             makePtr<DummyGenerator>());

            checkImplPtr();

            return impl->configure(is, os);
        }

        size_t hash() const
        {
            checkGenPtr();

            size_t h = gen->hash();

            checkImplPtr();

            return Hasher() << h << impl->hash();
        }

        void reset()
        {
            checkGenPtr();

            gen->reset();
        }

        void next()
        {
            checkGenPtr();

            gen->next();
        }

        cv::Mat get()
        {
            checkGenPtr();

            checkImplPtr();

            return impl->get();
        }

        size_t count() const
        {
            checkGenPtr();

            return gen->count();
        }

        cv::Size size() const
        {
            checkGenPtr();

            return gen->size();
        }

        int type() const
        {
            checkGenPtr();

            return gen->type();
        }

        Ptr<Generator>           gen;
        Filter::AbstractPrivate *impl;
    };

    Filter::Private::~Private()
    {
        delete impl;
    }

    Filter::~Filter()
    { }

    Filter::Filter(AbstractPrivate *ptr,
                   Ptr<Generator>   gen) :
        Generator(new Private(this, ptr, gen))
    { }

    Generator * Filter::generator()
    {
        PRIV_PTR;

        return priv->gen;
    }

    void Filter::setGenerator(Ptr<Generator> gen)
    {
        PRIV_PTR;

        priv->gen = gen;
    }

    cv::Ptr<Generator> Filter::defaultGenerator()
    {
        return makePtr<DummyGenerator>();
    }

    void Filter::setPrivateImplementation(AbstractPrivate *pimpl)
    {
        PRIV_PTR;

        delete priv->impl;
        priv->impl = pimpl;
    }

    Filter::AbstractPrivate * Filter::privateImplementation()
    {
        PRIV_PTR;

        return priv->impl;
    }

    const Filter::AbstractPrivate * Filter::privateImplementation() const
    {
        CONST_PRIV_PTR;

        return priv->impl;
    }

    Filter::AbstractPrivate::AbstractPrivate(Filter *filter) :
        Object::AbstractPrivate(filter)
    { }

    Filter::AbstractPrivate::~AbstractPrivate()
    { }
}
