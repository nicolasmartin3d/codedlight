/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/highgui/highgui.hpp>
#include "../core/parameter.h"
#include "generator.h"
#include "filter_p.h"
#include "gamma_filter.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
    struct GammaFilter::Private : public AbstractPrivate
    {
        Private(GammaFilter *filter,
                double       gamma) :
            AbstractPrivate(filter)
        {
            paramGamma = TypeParameter<double>("gamma",
                                               "Gamma coefficient value to apply on patterns",
                                               GammaFilter::defaultGamma(),
                                               makePtr< PositiveConstraint<double> >(3.5));
            paramGamma = gamma;
        }
        ~Private();

        bool read(const FileNode &node)
        {
            paramGamma.read(node);

            return true;
        }

        bool write(FileStorage &fs) const
        {
            paramGamma.write(fs);

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            paramGamma.configure(is, os);

            return true;
        }

        size_t hash() const
        {
            return Hasher() << paramGamma;
        }

        Mat get()
        {
            if (paramGamma == 1.0)
            {
                return pubPtr()->generator()->get(false);
            }

            work = Mat_<double>(pubPtr()->generator()->get(false) / 255.);
            pow(work, 1.0 / paramGamma, work);

            return Mat_<uchar>(work * 255.);
        }

        TypeParameter<double> paramGamma;
        Mat                   work;

        CL3DS_PUB_IMPL(GammaFilter)
    };

    GammaFilter::Private::~Private()
    { }

    GammaFilter::GammaFilter(Ptr<Generator> generator,
                             double         gamma) :
        Filter(new Private(this, gamma), generator)
    { }

    GammaFilter::~GammaFilter()
    { }

    double GammaFilter::gamma() const
    {
        CL3DS_PRIV_CONST_PTR(GammaFilter);

        return priv->paramGamma;
    }

    void GammaFilter::setGamma(double gamma)
    {
        CL3DS_PRIV_PTR(GammaFilter);

        priv->paramGamma = gamma;
    }

    double GammaFilter::defaultGamma()
    {
        return 1.0;
    }
}
