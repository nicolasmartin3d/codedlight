/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "matcher_p.h"
#include "matchers.h"

using namespace std;
using namespace cv;

namespace cl3ds
{
    /*
     * #define CONST_PRIV_PTR const AbstractPrivate * priv = \
     *     static_cast<const AbstractPrivate>(Object::privateImplementation());
     */

#define PRIV_PTR AbstractPrivate * priv = \
    static_cast<AbstractPrivate *>(Object::privateImplementation());

    struct Matcher::PrivateData
    {
        PrivateData() { }
    };

    Matcher::Matcher(AbstractPrivate *ptr) :
        Object(ptr),
        m_privData(new PrivateData())
    { }

    Matcher::~Matcher()
    {
        delete m_privData;
    }

    static Ptr< Factory<string, Matcher> > makeFactory()
    {
        Ptr< Factory<string, Matcher> > f = makePtr< Factory <string, Matcher> >();
        f->reg<GrayCodeMatcher>();
        f->reg<PhaseShiftMatcher>();
        f->reg<XorBinaryMatcher>();

        return f;
    }

    Factory<string, Matcher> *Matcher::classFactory()
    {
        static Ptr< Factory<string, Matcher> > factory = makeFactory();

        return factory;
    }

    MatTable Matcher::match(Generator *const                 camGenerator,
                            Generator *const                 projGenerator,
                            const Mat                       &camMask,
                            const Mat                       &projMask,
                            const CameraGeometricParameters &camParameters,
                            const CameraGeometricParameters &projParameters)
    {
        PRIV_PTR

        // this is important ! resets the generator to the first index and let
        // it reinitialize
        camGenerator->reset();

        if (!camGenerator->hasNext())
        {
            logWarning() << "Camera generator has no images, nothing to match!";

            return MatTable();
        }

        // if both camera and projector geometry is available, try to optimize the match
        // by making use of this information.
        // ! TODO : make some additional checks, e.g radial distortion might make this
        // impossible to do.

        MatTable result;
        if (!camParameters.empty() && !projParameters.empty())
        {
            Mat     c1 = -camParameters.R().t() * camParameters.t();
            Mat     c2 = -projParameters.R().t() * projParameters.t();
            Mat     u  = c2 - c1;
            double *_u = u.ptr<double>();

            bool useHorizontalPatterns = std::abs(_u[0]) > std::abs(_u[1]);
            result = priv->matchWithEpipolarGeometry(camGenerator,
                                                     projGenerator,
                                                     camMask, projMask,
                                                     camParameters,
                                                     projParameters,
                                                     useHorizontalPatterns);
        }
        else
        {
            result = priv->matchWithoutEpipolarGeometry(camGenerator,
                                                        projGenerator,
                                                        camMask, projMask);
        }

        return result;
    }

    Matcher::AbstractPrivate::AbstractPrivate(Matcher *matcher) :
        Object::AbstractPrivate(matcher)
    { }

    Matcher::AbstractPrivate::~AbstractPrivate()
    { }

    MatcherData::MatcherData(Generator *const                 cGen,
                             Generator *const                 pGen,
                             const Mat                       &cMask,
                             const Mat                       &pMask,
                             const CameraGeometricParameters &cParams,
                             const CameraGeometricParameters &pParams,
                             bool                             pUseHorizontalPatterns) :
        camGenerator(cGen),
        projGenerator(pGen),
        camMask(cMask),
        projMask(pMask),
        camParameters(cParams),
        projParameters(pParams),
        useHorizontalPatterns(pUseHorizontalPatterns)
    { }
}
