/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MATCHER_H
#define MATCHER_H

#include <iostream>
#include <opencv2/core/core.hpp>
#include <mvg/mvg.h>
#include "../core/object.h"
#include "../core/factory.h"
#include "../core/mattable.h"
#include "generator.h"

namespace cl3ds
{
    /** Base class for pattern matching. */
    class CL3DS_DECLSPEC Matcher : public Object
    {
        public:
            static Factory<std::string, Matcher> * classFactory();

            virtual ~Matcher();

            /** Computes the correspondence maps between the camera and
             *  the projector. This effectively matches camera pixels to
             *  projector pixels by finding the deformation that would
             *  transform the sequence of projected patterns in the sequence
             *  of camera captured images. */
            virtual MatTable match(Generator *const                 camGenerator,
                                   Generator *const                 projGenerator,
                                   const cv::Mat                   &camMask=cv::Mat(),
                                   const cv::Mat                   &projMask=cv::Mat(),
                                   const CameraGeometricParameters &camParameters=
                                       CameraGeometricParameters(),
                                   const CameraGeometricParameters &projParameters=
                                       CameraGeometricParameters());

        protected:
            struct AbstractPrivate;
            Matcher(AbstractPrivate *ptr);

        private:
            struct PrivateData;
            PrivateData *m_privData;
    };
}

#endif /* #ifndef MATCHER_H */
