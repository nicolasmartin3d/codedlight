/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "common.h"

using namespace cv;

namespace cl3ds
{
    struct RNGBit::Private
    {
        Private(uint64 seed);
        ~Private();

        cv::RNG      rng;
        unsigned int nb;
        size_t       usableBits;
        cv::Mutex    mutex;
    };

    RNGBit::Private::Private(uint64 seed) :
        rng(seed),
        nb(rng.next()),
        usableBits(sizeof(unsigned int) * CHAR_BIT)
    { }

    RNGBit::Private::~Private()
    { }

    RNGBit::RNGBit(uint64 seed)
    {
        m_privImpl = new Private(seed);
    }

    RNGBit::RNGBit(const RNGBit &rng) :
        m_privImpl(new Private(*rng.m_privImpl))
    { }

    RNGBit & RNGBit::operator=(const RNGBit &rng)
    {
        if (this != &rng)
        {
            delete m_privImpl;
            m_privImpl = new Private(*rng.m_privImpl);
        }

        return *this;
    }

    RNGBit::~RNGBit()
    {
        delete m_privImpl;
    }

    /*
     * Uses as many bits as there are in a random unsigned int, shifting bits
     * one at a time until they are no more, rinse and repeat.
     */
    bool RNGBit::operator()()
    {
        AutoLock lock(m_privImpl->mutex);
        if (m_privImpl->usableBits == 0)
        {
            m_privImpl->nb         = m_privImpl->rng.next();
            m_privImpl->usableBits = sizeof(unsigned int) * CHAR_BIT;
        }
        bool bit = static_cast<bool>(m_privImpl->nb & 1);
        m_privImpl->nb >>= 1;
        --m_privImpl->usableBits;

        return bit;
    }

    ElapsedTimer::ElapsedTimer()
    {
        restart();
    }

    void ElapsedTimer::restart()
    {
        m_start = static_cast<double>(getTickCount());
    }

    double ElapsedTimer::elapsed() const
    {
        return 1000. * (getTickCount() - m_start) / getTickFrequency();
    }

    void sleep_for(unsigned int milliseconds)
    {
#ifdef _MSC_VER
        Sleep(milliseconds);
#else
        struct timespec t, r;

        t.tv_sec  = milliseconds / 1000;
        t.tv_nsec = (milliseconds % 1000) * 1000000;

        while (nanosleep(&t, &r) == -1)
        {
            t = r;
        }
#endif
    }

    double log2(double n)
    {
        return log(n) / log(2.);
    }

    void pointFromMatch(const Vec3w &color,
                        Size         size,
                        double      &x,
                        double      &y)
    {
        x = color[2] * (size.width - 1) / 65535.;
        y = color[1] * (size.height - 1) / 65535.;
    }

    void pointToMatch(double x,
                      double y,
                      Size   size,
                      Vec3w &color)
    {
        x = min(std::max(x, 0.0), size.width - 1.0);
        y = min(std::max(y, 0.0), size.height - 1.0);
        ushort r = static_cast<ushort>(0.5 + x * 65535 / (size.width - 1));
        ushort g = static_cast<ushort>(0.5 + y * 65535 / (size.height - 1));
        color = cv::Vec3w(0, g, r);
    }

    void pointsFromMatchMap(const Mat  &match,
                            const Size &projSize,
                            Mat        &camPoints,
                            Mat        &projPoints)
    {
        Size camSize = match.size();
        camPoints.create(camSize, CV_64FC2);
        for (int y = 0; y < camSize.height; ++y)
        {
            Point2d *ptr = camPoints.ptr<Point2d>(y);
            for (int x = 0; x < camSize.width; ++x)
            {
                ptr[x] = Point2d(x + 0.5, y + 0.5);
            }
        }

        Mat codes[2]  = { Mat(camSize, CV_16U), Mat(camSize, CV_16U) };
        int from_to[] = { 2, 0, 1, 1 };
        mixChannels(&match, 1, codes, 2, from_to, 2);
        codes[0].convertTo(codes[0], CV_64F, (projSize.width - 1) / 65535., 0.5);
        codes[1].convertTo(codes[1], CV_64F, (projSize.height - 1) / 65535., 0.5);
        merge(codes, 2, projPoints);
    }

}
