/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

// architecure defines ..
#cmakedefine OS_APPLE
#cmakedefine OS_WIN
#cmakedefine OS_UNIX

#cmakedefine ARCH_64

// path to use <functional>
#cmakedefine FUNCTIONAL_IN_STD_HEADER
#cmakedefine FUNCTIONAL_IN_TR1_HEADER

// namespace to use std::hash
#cmakedefine HASH_IN_STD_NAMESPACE
#cmakedefine HASH_IN_TR1_NAMESPACE

// env{HOME}
#define HOME "${HOME}"

// path to where the datas should be found ..
#define CODEDLIGHT_HOME "${CODEDLIGHT_HOME}"

// opencv is compiled with Qt support
#cmakedefine OPENCV_SUPPORTS_QT

// qt support
#cmakedefine HAVE_QT
#cmakedefine HAVE_QT4
#cmakedefine HAVE_QT5

// qt test support
#cmakedefine HAVE_QT_TEST

// PvAPI support
#cmakedefine HAVE_PVAPI

// TinyTag support
#cmakedefine HAVE_TINYTAG

// lcr4500 support
#cmakedefine HAVE_LCRAPI

// Modbus support
#cmakedefine HAVE_MODBUS

// OSG support
#cmakedefine HAVE_OSG

// Boost support
#cmakedefine HAVE_BOOST

// are we building the library as a static one ?
#cmakedefine CODEDLIGHT_STATIC_LIBRARY
