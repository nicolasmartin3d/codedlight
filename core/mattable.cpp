/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <mvg/logger.h>
#include "common.h"
#include "mattable.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
    struct MatTable::Private
    {
        Private();
        ~Private();
        map<string, Mat> tab;
    };

    MatTable::Private::Private()
    { }

    MatTable::Private::~Private()
    { }

    MatTable::MatTable()
    {
        m_privImpl = new Private();
    }

    MatTable::MatTable(const MatTable &rng) :
        m_privImpl(new Private(*rng.m_privImpl))
    { }

    MatTable & MatTable::operator=(const MatTable &rng)
    {
        if (this != &rng)
        {
            delete m_privImpl;
            m_privImpl = new Private(*rng.m_privImpl);
        }

        return *this;
    }

    MatTable::~MatTable()
    {
        delete m_privImpl;
    }

    Mat & MatTable::operator[](const string &name)
    {
        return namedMat(name);
    }

    vector<string> MatTable::names() const
    {
        vector<string> allNames(m_privImpl->tab.size());
        size_t         i = 0;
        for (map<string, Mat>::const_iterator it = m_privImpl->tab.begin(),
             end = m_privImpl->tab.end(); it != end; ++it, ++i)
        {
            allNames[i] = it->first;
        }

        return allNames;
    }

    const Mat & MatTable::operator[](const string &name) const
    {
        return namedMat(name);
    }

    Mat & MatTable::namedMat(const string &name)
    {
        return m_privImpl->tab[name];
    }

    const Mat & MatTable::namedMat(const string &name) const
    {
        map<string, Mat>::const_iterator it = m_privImpl->tab.find(name);
        if (it == m_privImpl->tab.end())
        {
            logError() << "Requested mat named" << name << "does not exist";
        }

        return it->second;
    }

    const Mat & MatTable::camMatchMat() const
    {
        return namedMat("cam match");
    }

    Mat & MatTable::camMatchMat()
    {
        return namedMat("cam match");
    }

    const Mat & MatTable::camHorizontalCodes() const
    {
        return namedMat("cam horizontal codes");
    }

    Mat & MatTable::camHorizontalCodes()
    {
        return namedMat("cam horizontal codes");
    }

    const Mat & MatTable::camVerticalCodes() const
    {
        return namedMat("cam vertical codes");
    }

    Mat & MatTable::camVerticalCodes()
    {
        return namedMat("cam vertical codes");
    }

    const Mat & MatTable::projMatchMat() const
    {
        return namedMat("proj match");
    }

    Mat & MatTable::projMatchMat()
    {
        return namedMat("proj match");
    }

    const Mat & MatTable::projHorizontalCodes() const
    {
        return namedMat("proj horizontal codes");
    }

    Mat & MatTable::projHorizontalCodes()
    {
        return namedMat("proj horizontal codes");
    }

    const Mat & MatTable::projVerticalCodes() const
    {
        return namedMat("proj vertical codes");
    }

    Mat & MatTable::projVerticalCodes()
    {
        return namedMat("proj vertical codes");
    }
}
