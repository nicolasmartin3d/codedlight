/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBJECT_H
#define OBJECT_H

#include <iostream>
#include <opencv2/core/core.hpp>
#include <mvg/logger.h>
#include <mvg/sfinae.h>
#include "common.h"
#include "export.h"

namespace cl3ds
{
    /** This macro is a convenience function that cast the pimpl
     *  to your own private implementation type. */
#define CL3DS_PRIV_PTR_GETTERS \
    virtual Private * privPtr() \
    { \
        return reinterpret_cast<Private *>(privateImplementation()); \
    } \
    virtual const Private *privPtr() const \
    { \
        return reinterpret_cast<const Private *>(privateImplementation()); \
    }

    /** These macros are used to implement the opaque pointer idiom and are adapted
     * from the source code of Qt (qobject.h). */
#define CL3DS_PRIV_IMPL \
    protected: \
        struct Private; \
    private: \
        friend struct Private; \
        CL3DS_PRIV_PTR_GETTERS

    /** Use this macro to provide the class name of an object. */
#define CL3DS_CLASS_NAME(name) \
    public: \
        static const std::string & className() \
        { \
            static std::string key(name); \
            return key; \
        } \
        const std::string &objectName() const \
        { \
            return className(); \
        } \
    private:

    /** Abstract base class for every class in CodedLight.
     *
     *  In particular, an object can be :
     *  - generated on the fly via configure() which "ask questions" to configure the
     *  object
     *  - written to a FileStorage via write()
     *  - read from a XML file via read()
     *  - hashed to an integral representation (for e.g quick comparison) via hash()
     *
     *  In order to customize the object's behavior, you should reimplement the virtual
     *  functions in a class derived from Object::Private, and pass an instance of this object
     *  to the constructor of Object. You can also reimplement the virtual functions directly
     *  in your class derived from Object, but you will not benefit from some common checks
     *  done in the base class.
     */
    class CL3DS_DECLSPEC Object
    {
        public:
            virtual ~Object();

            /** Reads the object from a specific FileNode. This implementation
             *  forwards the call to the pimpl implementation. */
            virtual bool read(const cv::FileNode &fn);

            /** Writes the object to a FileStorage. This implementation
             *  forwards the call to the pimpl implementation. */
            virtual bool write(cv::FileStorage &fs) const;

            /** Configures the object by asking questions to the user.
             *  This implementation forwards the call to the pimpl implementation. */
            virtual bool configure(std::istream *is,
                                   std::ostream *os);

            /** Returns a unique identifier based on the parameters of the object.
             *  This implementation forwards the call to the pimpl implementation. */
            virtual size_t hash() const;

            /** Returns the class name of this object, providing some kind of
             *  introspection. */
            virtual const std::string & objectName() const = 0;

            /** The object parameters can be embedded or read from an external
             *  configuration file that embedds them.
             *  If the parameters are not embedded, this functions returns the path to the
             *  external configuration file. Otherwise it returns an empty string. */
            const std::string & configFilePath() const;

            /** Set the path to the configuration file that embedds this objects parameters. */
            void setConfigFilePath(const std::string &);

            /** The object parameters can be embedded or read from an external configuration file.
             *  If the parameters are not embedded, this functions returns the node name from
             *  which to read the object parameters in the path returned by configFilePath().
             *  Otherwise it returns an empty string. */
            const std::string & configNodeName() const;

            /** Set the name of the node from which to read the embedded parameters of this object
             *  in the file returned by configFilePath(). */
            void setConfigNodeName(const std::string &);

        protected:
            struct AbstractPrivate;

            /** Constructor that must be called from derived instances with a
             *  properly initialized pointer to the pimpl. */
            Object(AbstractPrivate *pimpl);
            AbstractPrivate *m_privImpl;

            /** Pimpl access */
            void setPrivateImplementation(AbstractPrivate *pimpl);
            AbstractPrivate * privateImplementation();
            const AbstractPrivate * privateImplementation() const;

        private:
            // own personal pimpl
            struct Private;

            // copy and assignment operator are prohibited
            Object(const Object &) { }
            void operator=(const Object &) { }

#if CV_MAJOR_VERSION < 3
            template<class T> friend void cv::Ptr<T>::delete_obj();
#endif
    };

    /** Writes an object to a FileStorage. This template function is required by the
     *  operator<<(FileStorage&, const Ptr<T>&);
     *  to properly serialize an Object to a XML file.
     *  This template function will only be instantiated when called with a pointer
     *  to an Object. */
    template <class T>
    void write(cv::FileStorage &fs,
               const std::string &n,
               const cv::Ptr<T> &o,
               typename EnableIf<IsBaseOf<Object, T> >::type *t=0)
    {
        CL3DS_UNUSED(n);
        CL3DS_UNUSED(t);

        if (o.empty()) { return; }

        fs << "{";
        if (!o->configFilePath().empty() && !o->configNodeName().empty())
        {
            fs << "configFilePath_" << o->configFilePath();
            fs << "configNodeName_" << o->configNodeName();
        }
        else
        {
            fs << "name" << o->objectName();
            fs << "param";
            o->write(fs);
        }
        fs << "}";
    }

    /** Reads an object from a FileNode. This template function is required by the
     *  operator>>(FileNode&, Ptr<T>&);
     *  to properly deserialize an Object to a XML file.
     *  This template function will only be instantiated when called with a pointer
     *  to a class T that derives from Object.
     *  The T class must provide a static classFactory() member function that returns
     *  a factory that can create a class derived from T. */
    template <class T, class U>
    void read(const cv::FileNode &node,
              cv::Ptr<T> &o,
              const cv::Ptr<U> &d,
              typename EnableIf<IsBaseOf<T, U> >::type *t=0,
              typename EnableIf<IsBaseOf<Object, T> >::type *t2=0)
    {
        CL3DS_UNUSED(t);
        CL3DS_UNUSED(t2);

        if (node.empty()) { o = d; return; }

        std::string configFilePath;
        std::string configNodeName;
        if (!node["configFilePath_"].empty() && !node["configNodeName_"].empty())
        {
            node["configFilePath_"] >> configFilePath;
            if (configFilePath.empty())
            {
                logError() << "Config file path given is empty";
            }

            node["configNodeName_"] >> configNodeName;
            if (configNodeName.empty())
            {
                logError() << "Config node name in config file is empty";
            }

            cv::FileStorage fs(configFilePath, cv::FileStorage::READ);
            if (!fs.isOpened())
            {
                logError() << "Unable to read object from file path";
            }

            read(fs[configNodeName], o, d);

            o->setConfigFilePath(configFilePath);
            o->setConfigNodeName(configNodeName);

            return;
        }

        std::string name;
        node["name"] >> name;

        if (name.empty())
        {
            logWarning() << "No class name found in the node";
            o = d;

            return;
        }

        o = dynamicPtrCast<T>(T::classFactory()->create(name));
        if (o.empty())
        {
            logWarning() << "Could not create the object requested";
            o = d;

            return;
        }

        cv::FileNode param = node["param"];
        if (param.empty())
        {
            logWarning() << "No params found in the node";
            o = d;

            return;
        }

        o->read(param);
        o->setConfigFilePath(std::string());
        o->setConfigNodeName(std::string());
    }

    /** Configures an object by asking questions. This template function will only be
     *  instantiated when called with a pointer to an Object. The T class must provide
     *  a static classFactory() member function that returns a factory that can create
     *  a class derived from T. By default the input and output stream are left null
     *  which corresponds to creating an Object with default values. */
    // this may be need some cleaning ! e.g useFactory could at least use SFINAE to
    // determine is T has a factory and/or is derived by some class...
    template <class T, class U>
    void configure(cv::Ptr<T> &o,
                   std::istream *is=0,
                   std::ostream *os=0,
                   const std::string &name=std::string(),
                   const cv::Ptr<U> &d=cv::Ptr<T>(),
                   bool askForEmbedded=true,
                   bool useFactory=true,
                   typename EnableIf<IsBaseOf<T, U> >::type *t=0,
                   typename EnableIf<IsBaseOf<Object, T> >::type *t2=0)
    {
        CL3DS_UNUSED(t);
        CL3DS_UNUSED(t2);

        std::string n = name;
        if (name.empty())
        {
            if (!is || !os)
            {
                o = d;
            }
            else if (askForEmbedded)
            {
                *os << "Are the parameters of the object "
                    "embedded in an existing XML file ? (y/n)" << std::endl;
                char c;
                do
                {
                    *is >> c;
                    if ((c == 'y') || (c == 'Y'))
                    {
                        *os << "What is the path to the XML "
                            "configuration for this object ?" << std::endl;
                        std::string configFilePath;
                        *is >> configFilePath;

                        *os << "What is the name of the node "
                            "which embedds this object parameters "
                            "in the XML file ?" << std::endl;
                        std::string configNodeName;
                        *is >> configNodeName;

                        cv::FileStorage fs(configFilePath, cv::FileStorage::READ);
                        if (!fs.isOpened())
                        {
                            logError() << "Unable to read object from file path";
                        }

                        read(fs[configNodeName], o, d);

                        o->setConfigFilePath(configFilePath);
                        o->setConfigNodeName(configNodeName);

                        return;
                    }
                }
                while (c != 'n' && c != 'y' && c != 'Y' && c != 'N');
            }
        }

        if (useFactory)
        {
            const std::vector<std::string> &names = T::classFactory()->keys();
            o = dynamicPtrCast<T>(T::classFactory()->create(n));
            while (o.empty())
            {
                if (!n.empty())
                {
                    logWarning() << "Could not create the object requested";
                }

                if (os)
                {
                    *os << "Available identifiers are : " << std::endl;

                    for (std::vector<std::string>::const_iterator it =
                             names.begin(),  itE = names.end();
                         it != itE; ++it)
                    {
                        *os << "\t" << *it << std::endl;
                    }

                    *os << "Which object should be created ? " << std::endl;
                }

                if (is) { *is >> n; }
                else
                {
                    // can't query the method name .. exit !
                    o = d;
                    break;
                }

                o = dynamicPtrCast<T>(T::classFactory()->create(n));
            }
        }
        else
        {
            o = d;
        }

        if (!o.empty()) { o->configure(is, os); }
    }

    /** Overloaded configure for when no default value is explicitly given. */
    template <class T>
    void configure(cv::Ptr<T> &o,
                   std::istream *is=0,
                   std::ostream *os=0,
                   const std::string &name=std::string(),
                   const cv::Ptr<T> &d=cv::Ptr<T>(),
                   bool askForEmbedded=true,
                   bool useFactory=true,
                   typename EnableIf<IsBaseOf<Object, T> >::type *t2=0)
    {
        CL3DS_UNUSED(t2);
        configure<T, T>(o, is, os, name, d, askForEmbedded, useFactory);
    }
}

#endif // OBJECT_H
