/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARAMETER_H
#define PARAMETER_H

#include <iostream>
#include <memory>
#include <list>

#include <opencv2/core/core.hpp>
#include "export.h"
#include "common.h"

namespace cl3ds
{
    /** Base class that represents any kind of parameter. */
    class CL3DS_DECLSPEC Parameter
    {
        public:
            Parameter();
            virtual ~Parameter();

            virtual std::string type() const        = 0;
            virtual std::string description() const = 0;

            virtual std::string longDescription() const;
    };

    template <class T>
    class ParameterConstraint
    {
        public:
            virtual ~ParameterConstraint();

            virtual bool validate(const T &t) const = 0;
            virtual std::string description() const = 0;
    };

    /** Constraint that verifies that a value is within the specified
     *  bounds. Both bounds are exclusive. */
    template <class T> struct BoundComparator;
    template <class T, class Comparator = BoundComparator<T> >
    class BoundConstraint : public ParameterConstraint<T>
    {
        public:
            BoundConstraint(const T &min,
                            const T &max);

            virtual bool validate(const T &t) const;
            virtual std::string description() const;

        protected:
            T          m_min;
            T          m_max;
            Comparator m_comp;
    };

    template <class T>
    class PositiveConstraint : public BoundConstraint<T>
    {
        public:
            PositiveConstraint(const T &max=std::numeric_limits<T>::max());
    };

    /** Constraint that verifies that a value is inside the associated
     *  enumeration. */
    template <class T>
    class EnumerationConstraint : public ParameterConstraint<T>
    {
        public:
            EnumerationConstraint(const std::vector<T> &values);

            virtual bool validate(const T &t) const;
            virtual std::string description() const;

        protected:
            std::vector<T> m_values;
    };

    /* *INDENT-OFF* */
    /** Templated class that represents a parameter encapsulating the
     *  specified template type T. It provides custom assignation and
     *  that checks if constraint are enforced, and custom conversion
     *  to a T. It can easily be serialized to opencv's storage classes. */
    template <class T>
    class TypeParameter : public Parameter
    {
        public:
            TypeParameter(
                const std::string                 &name=std::string(),
                const std::string                 &description=std::string(),
                const T                           &defaultValue=T(),
                cv::Ptr< ParameterConstraint<T> > constraint=cv::Ptr< ParameterConstraint<T> >());

            TypeParameter<T>& operator=(const T &val);
            operator const T&() const;

            virtual std::string type() const;
            virtual std::string defaultValue() const;
            virtual std::string description() const;

            virtual TypeParameter<T>& read(const cv::FileNode &node);
            virtual void write(cv::FileStorage &fs) const;
            virtual TypeParameter<T>& configure(std::istream *in=&std::cin,
                                                std::ostream *out=&std::cout);

        private:
            T m_value;

        public:
            std::string                       name;
            std::string                       desc;
            T                                 def;
            cv::Ptr< ParameterConstraint<T> > constr;
    };
    /* *INDENT-ON* */
}

// missings persistence operations for cv::Rect and cv::Size in OpenCV v.2
#if CV_MAJOR_VERSION < 3
namespace cv
{
    static inline void write(cv::FileStorage   &fs,
                             const std::string &n,
                             const cv::Size    &s)
    {
        CL3DS_UNUSED(n);
        fs << "{" << "width" <<  s.width << "height" << s.height << "}";
    }

    static inline void read(const cv::FileNode &node,
                            cv::Size           &s,
                            const cv::Size     &def=cv::Size())
    {
        if (node.empty()) { s = def; }
        else
        {
            s.width  = node["width"];
            s.height = node["height"];
        }
    }

    static inline void write(cv::FileStorage &fs,
                             const std::string & /*n*/,
                             const cv::Rect  &s)
    {
        fs << "{" << "width" << s.width << "height" << s.height << "x" << s.x
           << "y" << s.y << "}";
    }

    static inline void read(const cv::FileNode &node,
                            cv::Rect           &s,
                            const cv::Rect     &def=cv::Rect())
    {
        if (node.empty()) { s = def; }
        else
        {
            s.width  = node["width"];
            s.height = node["height"];
            s.x      = node["x"];
            s.y      = node["y"];
        }
    }
}
#endif

#include "parameter.tpp"

#endif // PARAMETER_H
