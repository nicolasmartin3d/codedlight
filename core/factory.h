/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FACTORY_H
#define FACTORY_H

#include <map>
#include <vector>
#include <cstring>
#include <iostream>
#include "object.h"
#include "export.h"

namespace cl3ds
{
    /** Templated factory class.
     *
     * This class implements the factory design pattern that can register
     * mapping between keys of any type (Key) and object (Base) creators.
     * Whenever the factory has to create an object based on a key, it will
     * look for registered creators and select the right one to create a
     * new object derived from the base class.
     *
     */
    template <class Key, class Base>
    class Factory
    {
        typedef cv::Ptr<Base> (*Creator)();
        typedef typename std::map<Key, Creator>::const_iterator MappingConstIterator;

        public:
            virtual ~Factory()
            { }

            /** Registers a mapping between a key and a creator. */
            template <typename Derived>
            bool reg(const Key     &k,
                     const Derived &d=Derived())
            {
                CL3DS_UNUSED(d);

                m_reg[k] = creator<Derived>;

                return true;
            }

            /** Registers a mapping for a class derived from Object. */
            template <typename Derived>
            bool reg(typename EnableIf<IsBaseOf<Object, Derived> >::type *t=0)
            {
                CL3DS_UNUSED(t);

                m_reg[Derived::className()] = creator<Derived>;

                return true;
            }

            /** Unregisters a mapping. */
            template <typename Derived>
            bool unreg(const Key &k)
            {
                return m_reg.erase(k) == 1;
            }

            /** Create an object based on a key. */
            cv::Ptr<Base> create(const Key &k)
            {
                MappingConstIterator it = m_reg.find(k);
                if (it == m_reg.end())
                {
                    return cv::Ptr<Base>();
                }

                return it->second();
            }

            /** Return all the registered keys. */
            std::vector<Key> keys() const
            {
                std::vector<Key>     allKeys(m_reg.size());
                MappingConstIterator it  = m_reg.begin(),
                                     itE = m_reg.end();
                for (size_t i = 0; it != itE; ++it, ++i)
                {
                    allKeys[i] = it->first;
                }

                return allKeys;
            }

        protected:
            template <class Derived>
            static cv::Ptr<Base> creator()
            {
                return cv::Ptr<Base>(static_cast<Base *>(new Derived));
            }

            // the mapping registry
            std::map<Key, Creator> m_reg;
    };
}

#endif // FACTORY_H
