/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MATDISPATCHER_H
#define MATDISPATCHER_H

#include "common.h"
#include <map>
#include <vector>
#include <utility>
#if defined FUNCTIONAL_IN_STD_HEADER
#include <functional>
#else
#include <tr1/functional>
#endif
#include <opencv2/core/core.hpp>
#include <mvg/logger.h>
#include "foreach.h"

namespace cl3ds
{
    /* *INDENT-OFF* */

    // ! TODO : clean this code to use templated class, rather than templated member. Add
    // a template parameter of MatDispatcher to represent the callback and permit free
    // functions or const member functors. */

    namespace internal
    {
        template <typename Executor,
                  typename ResultType,
                  typename ArgumentType>
        struct CallbackRegister1D
        {
            typedef std::mem_fun1_ref_t<ResultType, Executor, const ArgumentType&> Callback;
            typedef std::map<int, cv::Ptr<Callback> >        Map;

            CallbackRegister1D(Map &cbs) : callbacks(cbs) {}

            template <typename T> void operator()()
            {
                int p = MatType2Int<T>::value;
                callbacks[p] =
#ifdef _MSC_VER
                        // visual studio does not seem to allow explicit template qualifier...
                        cv::makePtr<Callback>(static_cast<Callback>(&Executor::operator()<T>));
#else
                        cv::makePtr<Callback>(&Executor::template operator()<T>);
#endif
            }

            Map &callbacks;
        };
    }

    /** Dispatcher class that can be used to automatically decide which templated functor to
     *  call based on the type passed to the function call operator (which should be the
     *  depth value of a cv::Mat). */
    template <class Executor,
              class ArgumentType,
              class ResultType,
              class TList = MAT_TYPES1D_LIST>
    class MatDispatcher1D
    {
        typedef std::mem_fun1_ref_t<ResultType,
                                    Executor,
                                    const ArgumentType&>           Callback;
        typedef internal::CallbackRegister1D<Executor,
                                             ResultType,
                                             const ArgumentType&>  CallbackRegister;
        typedef std::map<int, cv::Ptr<Callback> >                  Map;

        public:
            MatDispatcher1D(Executor &e) : exec(e)
            {
                CallbackRegister reg(callbacks);
                for_each<TList>(reg);
            }

            ResultType operator()(int                type,
                                  const ArgumentType &arg)
            {
                typename Map::const_iterator it = callbacks.find(type);
                if (it == callbacks.end())
                {
                    logError() << "Mat type not supported in typelist :" <<
                        type;
                }

                return (*it->second)(exec, arg);
            }

        private:
            Map      callbacks;
            Executor &exec;
    };

    namespace internal
    {
        template <typename Executor,
                  typename ResultType,
                  typename ArgumentType>
        struct CallbackRegister2D
        {
            typedef std::mem_fun1_ref_t<ResultType,
                                        Executor,
                                        const ArgumentType&>          Callback;
            typedef std::map<std::pair<int, int>, cv::Ptr<Callback> > Map;

            CallbackRegister2D(Map &cbs) : callbacks(cbs) {}

            template <typename T1, typename T2> void operator()()
            {
                std::pair<int, int> p(MatType2Int<T1>::value,
                                      MatType2Int<T2>::value);
                callbacks[p] =
#ifdef _MSC_VER
                        // visual studio does not seem to allow explicit template qualifier...
                        cv::makePtr<Callback>(static_cast<Callback>(&Executor::operator()<T1, T2>));
#else
                        cv::makePtr<Callback>(&Executor::template operator()<T1, T2>);
#endif
            }

            Map &callbacks;
        };
    }

    /** Dispatcher class that can be used to automatically decide which templated functor to
     *  call based on the type passed to the function call operator (which should be the
     *  depth value of a cv::Mat). */
    template <class Executor,
              class ArgumentType,
              class ResultType,
              class TList1 = MAT_TYPES1D_LIST,
              class TList2 = MAT_TYPES1D_LIST>
    class MatDispatcher2D
    {
        typedef std::mem_fun1_ref_t<ResultType,
                                    Executor,
                                    const ArgumentType&>            Callback;
        typedef internal::CallbackRegister2D<Executor,
                                             ResultType,
                                             const ArgumentType&>   CallbackRegister;
        typedef std::map<std::pair<int, int>, cv::Ptr<Callback> >   Map;

        public:
            MatDispatcher2D(Executor &e) : exec(e)
            {
                CallbackRegister reg(callbacks);
                for_each<TList1, TList2>(reg);
            }

            ResultType operator()(int                type1,
                                  int                type2,
                                  const ArgumentType &arg)
            {
                std::pair<int, int> p(type1, type2);
                typename Map::const_iterator it = callbacks.find(p);
                if (it == callbacks.end())
                {
                    logError() << "Mat types not supported in typelist :" <<
                        type1 << type2;
                }

                return (*it->second)(exec, arg);
            }

        private:
            Map      callbacks;
            Executor &exec;
    };

    /** Convenience function to automatically dispatch a call to a templated
     *  function call operator of the executor, with the template type deduced
     *  from the type argument (which should be a cv::Mat depth). */
    template <class ResultType, class Executor, class ArgumentType>
    ResultType dispatch(Executor           &executor,
                        const ArgumentType &argument,
                        int                type)
    {
        MatDispatcher1D<Executor,
                        ArgumentType,
                        ResultType> dispatcher(executor);

        return dispatcher(type, argument);
    }

    /** Convenience function to automatically dispatch a call to a templated
     *  function call operator of the executor, with the templates types deduced
     *  from the types argument (which should be cv::Mat depths). */
    template <class ResultType, class Executor, class ArgumentType>
    ResultType dispatch(Executor           &executor,
                        const ArgumentType &argument,
                        int                type1,
                        int                type2)
    {
        MatDispatcher2D<Executor,
                        ArgumentType,
                        ResultType> dispatcher(executor);

        return dispatcher(type1, type2, argument);
    }
}

#endif
