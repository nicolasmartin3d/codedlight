/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FOREACH_HPP
#define FOREACH_HPP

#include "config.h"
#include "typelist.h"

namespace cl3ds
{

    // ! TODO : clean this code to use templated class, rather than templated member. */

    /** Meta templated class Foreach that can be used to call a templated
     *  functor with several types available as a TypeList at compile time.
     *  This is especially useful to provide implementations of a function for
     *  different types or to automatically asks the compiler to register a callback
     *  for a lot of types known at compile time.
     */

    // T
    template <typename T, typename F>
    struct Foreach
    {
        // static void run(F f) { f.template operator()<T>(); }
#ifdef _MSC_VER
        // visual studio does not seem to allow explicit template quantifier...
        Foreach(F f)
        {
            f.operator()<T>();
        }
#else
        Foreach(F f)
        {
            f.template operator()<T>();
        }
#endif
    };

    // <H,0>
    template <typename Head, typename F>
    struct Foreach<TypeList<Head, NullType>, F> : public Foreach<Head, F>
    {
        Foreach<TypeList<Head, NullType>, F>(F f) :
        Foreach<Head, F>(f) { }
    };

    // <H,T>
    template <typename Head, typename Tail, typename F>
    struct Foreach<TypeList<Head, Tail>, F> :
        public Foreach<Head, F>,
        public Foreach<Tail, F>
    {
        Foreach<TypeList<Head, Tail>, F>(F f) :
        Foreach<Head, F>(f),
        Foreach<Tail, F>(f) { }
    };

    // T,T
    template <typename T1, typename T2, typename F>
    struct Foreach2D
    {
        // static void run(F f) { f.template operator()<T1, T2>(); }
#ifdef _MSC_VER
        // visual studio does not seem to allow explicit template quantifier...
        Foreach2D(F f)
        {
            f.operator()<T1, T2>();
        }
#else
        Foreach2D(F f)
        {
            f.template operator()<T1, T2>();
        }
#endif
    };

    // T,<H,0>
    template <typename Head1, typename Head2, typename F>
    struct Foreach2D<Head1, TypeList<Head2, NullType>, F> :
        public Foreach2D<Head1, Head2, F>
    {
        Foreach2D(F f) : Foreach2D<Head1, Head2, F>(f)
        { }
    };

    // T,<H,T>
    template <typename Head1, typename Head2, typename Tail2, typename F>
    struct Foreach2D<Head1, TypeList<Head2, Tail2>, F> :
        public Foreach2D<Head1, Head2, F>,
        public Foreach2D<Head1, Tail2, F>
    {
        Foreach2D(F f) :
            Foreach2D<Head1, Head2, F>(f),
            Foreach2D<Head1, Tail2, F>(f)
        { }
    };

    // <H,0>,T
    template <typename Head1, typename Head2, typename F>
    struct Foreach2D<TypeList<Head1, NullType>, Head2, F> :
        public Foreach2D<Head1, Head2, F>
    {
        Foreach2D(F f) :
            Foreach2D<Head1, Head2, F>(f)
        { }
    };

    // <H,0>,<H,0>
    template <typename Head1, typename Head2, typename F>
    struct Foreach2D<TypeList<Head1, NullType>, TypeList<Head2, NullType>, F> :
        public Foreach2D<Head1, Head2, F>
    {
        Foreach2D(F f) : Foreach2D<Head1, Head2, F>(f)
        { }
    };

    // <H,0>,<H,T>
    template <typename Head1, typename Head2, typename Tail2, typename F>
    struct Foreach2D<TypeList<Head1, NullType>, TypeList<Head2, Tail2>, F> :
        public Foreach2D<Head1, Head2, F>,
        public Foreach2D<Head2, Tail2, F>
    {
        Foreach2D(F f) :
            Foreach2D<Head1, Head2, F>(f),
            Foreach2D<Head2, Tail2, F>(f)
        { }
    };

    // <H,T>,T
    template <typename Head1, typename Tail1, typename Head2, typename F>
    struct Foreach2D<TypeList<Head1, Tail1>, Head2, F> :
        public Foreach2D<Head1, Head2, F>,
        public Foreach2D<Tail1, Head2, F>
    {
        Foreach2D(F f) :
            Foreach2D<Head1, Head2, F>(f),
            Foreach2D<Tail1, Head2, F>(f)
        { }
    };

    // <H,T>,<H,0>
    template <typename Head1, typename Tail1, typename Head2, typename F>
    struct Foreach2D<TypeList<Head1, Tail1>, TypeList<Head2, NullType>, F> :
        public Foreach2D<Head1, Head2, F>,
        public Foreach2D<Tail1, Head2, F>
    {
        Foreach2D(F f) :
            Foreach2D<Head1, Head2, F>(f),
            Foreach2D<Tail1, Head2, F>(f)
        { }
    };

    // <H,T>,<H,T>
    template <typename Head1, typename Tail1, typename Head2, typename Tail2,
              typename F>
    struct Foreach2D<TypeList<Head1, Tail1>, TypeList<Head2, Tail2>, F> :
        public Foreach2D<Head1, Head2, F>,
        public Foreach2D<Head1, Tail2, F>,
        public Foreach2D<Tail1, Head2, F>,
        public Foreach2D<Tail1, Tail2, F>
    {
        Foreach2D(F f) :
            Foreach2D<Head1, Head2, F>(f),
            Foreach2D<Head1, Tail2, F>(f),
            Foreach2D<Tail1, Head2, F>(f),
            Foreach2D<Tail1, Tail2, F>(f)
        { }
    };

    template <typename TList, typename F>
    void for_each(F t)
    {
        Foreach<TList, F> f(t);
    }

    template <typename TList1, typename TList2, typename F>
    void for_each(F t)
    {
        Foreach2D<TList1, TList2, F> f(t);
    }
}

#endif /* #ifndef FOREACH_HPP */
