/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <mvg/logger.h>

#include <QApplication>
#include <QDesktopWidget>
#include <QWidget>
#include <QPainter>
#include <QTimer>

#include "../patterns/generator.h"
#include "projector_device_p.h"
#include "qt_projector_device.h"

using namespace cv;
using namespace std;

namespace cl3ds
{

    class DummyEventFilteringObject : public QObject
    {
        public:
            DummyEventFilteringObject(QObject *parent) :
                QObject(parent) { }
            ~DummyEventFilteringObject();

            bool eventFilter(QObject *o,
                             QEvent  *e)
            {
                if (e->type() == QEvent::KeyPress)
                {

                    // do nothing
                    return true;
                }

                return QObject::eventFilter(o, e);
            }
    };

    DummyEventFilteringObject::~DummyEventFilteringObject()
    { }

    class FullScreenWindow : public QWidget
    {
        public:
            ~FullScreenWindow();
            static FullScreenWindow * instance()
            {
                static FullScreenWindow *win = 0;
                if (!win)
                {
                    if (!qApp)
                    {
                        int *argc = new int;
                        *argc = 1;
                        char **argv = new char *[1];
                        argv[0]    = new char[1];
                        argv[0][0] = '\0';
                        new QApplication(*argc, argv);
                    }

                    if (QApplication::desktop()->screenCount() == 1)
                    {
                        logWarning() <<
                            "Only one screen is available, no screen can be used for the projector";

                        return 0;
                    }

                    win = new FullScreenWindow;
                    win->installEventFilter(new DummyEventFilteringObject(win));
                    win->setWindowState(Qt::WindowFullScreen
                                        | win->windowState());
                    win->setWindowFlags(win->windowFlags()
                                        | Qt::FramelessWindowHint
                                        | Qt::CustomizeWindowHint
                                        | Qt::WindowStaysOnTopHint);
                    win->setGeometry(QApplication::desktop()->screenGeometry(1));
                    win->showFullScreen();
                }

                return win;
            }

            void showImage(const Mat &image_,
                           double     exposure)
            {
                if ((image_.rows != height()) || (image_.cols != width()))
                {
                    logWarning() << "Displaying image of size" << image_.cols << "x" <<
                        image_.rows <<
                        "when screen has size" << width() << "x" << height();
                }

                Mat image;
                switch (image_.channels())
                {
                    case 1:
                    {
                        cvtColor(image_, image, COLOR_GRAY2RGB);
                        break;
                    }
                    case 3:
                    {
                        cvtColor(image_, image, COLOR_BGR2RGB);
                        break;
                    }
                    case 4:
                    {
                        cvtColor(image_, image, COLOR_BGRA2RGB);
                        break;
                    }
                }

                m_image = QImage(
                    image.data, image.cols, image.rows,
                    static_cast<int>(image.step),
                    QImage::Format_RGB888);

                update();

                QEventLoop loop;
                QTimer::singleShot(static_cast<int>(exposure / 1000.), &loop,
                                   SLOT(quit()));
                loop.exec();
            }

        protected:
            void paintEvent(QPaintEvent *event)
            {
                CL3DS_UNUSED(event);
                QPainter painter(this);
                painter.drawImage(0, 0, m_image);
            }

        private:
            FullScreenWindow() { }

            QImage m_image;
    };

    FullScreenWindow::~FullScreenWindow()
    { }

    struct QtProjectorDevice::Private : public AbstractPrivate
    {
        Private(QtProjectorDevice *ptr) :
            AbstractPrivate(ptr),
            first(true),
            deactivateScreenSaver(true),
            window(0)
        {
            open(0);
        }
        ~Private();

        bool read(const FileNode &)
        {
            close();
            open(0);

            return true;
        }

        bool write(FileStorage &) const
        {
            return true;
        }

        bool configure(istream *,
                       ostream *)
        {
            close();
            open(0);

            return true;
        }

        size_t hash() const
        {
            return 0;
        }

        bool synchronized()
        {
            return false;
        }

        bool setSynchronized(bool)
        {
            return false;
        }

        void clear()
        {
            first = true;
        }

        bool open(int)
        {
            window = FullScreenWindow::instance();

            return window;
        }

        void close()
        { }

        bool isOpened() const
        {
            return window;
        }

        bool put(const Mat &image)
        {
            window->showImage(image, pubPtr()->exposureTime());

            /*
             *   if (first)
             *   {
             *    waitKey(500);
             *    first = false;
             *   }
             */

            return true;
        }

        bool put(Generator *const gen)
        {
            first = false;

            gen->reset();
            while (gen->hasNext())
            {
                put(gen->get());
            }

            first = true;

            return true;
        }

        bool setProperty(int    propId,
                         double value)
        {
            if (propId == ProjectorDevice::DEACTIVATE_SCREENSAVER)
            {
                deactivateScreenSaver = (value > 0);
            }

            return true;
        }

        double getProperty(int propId)
        {
            if (propId == ProjectorDevice::DEACTIVATE_SCREENSAVER)
            {
                return deactivateScreenSaver;
            }

            return -1;
        }

        bool              first;
        bool              deactivateScreenSaver;
        FullScreenWindow *window;

        CL3DS_PUB_IMPL(QtProjectorDevice)
    };

    QtProjectorDevice::Private::~Private()
    { }

    QtProjectorDevice::QtProjectorDevice(double exposureTime,
                                         double gamma) :
        ProjectorDevice(new Private(this), exposureTime, gamma)
    { }

    QtProjectorDevice::~QtProjectorDevice()
    { }
}
