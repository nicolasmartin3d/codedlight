/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/highgui/highgui.hpp>
#include "../core/config.h"
#include "../core/common.h"
#include "../core/parameter.h"
#include "camera_device.h"
#include "camera_device_p.h"
#include "camera_devices.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
#define PRIV_PTR Private * const priv = \
    static_cast<Private *>(Object::privateImplementation());
#define PRIV_CONST_PTR const Private * const priv = \
    static_cast<const Private *>(Object::privateImplementation());

    struct CameraDevice::Private : public Object::AbstractPrivate
    {
        Private(CameraDevice                  *ptr,
                CameraDevice::AbstractPrivate *pimpl,
                int                            retries,
                double                         exposureTime,
                int                            repeats) :
            Object::AbstractPrivate(ptr),
            impl(pimpl)
        {
            paramRetries = TypeParameter<int>("retries",
                                              "Number of retries before giving up if the camera does not respond",
                                              CameraDevice::defaultRetries(),
                                              makePtr< PositiveConstraint<int> >(100));
            paramRetries = retries;

            paramExposure = TypeParameter<double>("exposure",
                                                  "Time in microseconds each captured image will be exposed",
                                                  CameraDevice::defaultExposureTime(),
                                                  makePtr< PositiveConstraint<double> >());
            paramExposure = exposureTime;

            paramRepeats = TypeParameter<int>("repeats",
                                              "If >1 each frame will averaged from repeats frame (to alleviate noise)",
                                              CameraDevice::defaultRepeats(),
                                              makePtr< PositiveConstraint<int> >(100));
            paramRepeats = repeats;
        }
        ~Private();

        void checkImplPtr() const
        {
            if (!impl)
            {
                logError() << "Private pointer is null ! "
                    "You must pass a valid pointer to "
                    "the private implementation of your "
                    "object to the Object constructor";
            }
        }

        void setFps(double fps)
        {
            while (fps > 1 && !impl->setProperty(CAP_PROP_FPS, fps))
            {
                logDebug() << "Trying to set the fps to" << fps;
                fps--;
            }
            if (fps < 1)
            {
                logDebug() << "Unable to set the fps or find an appropriate fps";
            }
        }

        bool read(const FileNode &fn)
        {
            paramExposure.read(fn);
            paramRepeats.read(fn);
            paramRetries.read(fn);

            checkImplPtr();

            impl->read(fn);
            impl->setProperty(CAP_PROP_EXPOSURE, paramExposure);

            // adjust the camera's fps (if available) to be consistent with the exposure
            // note that this can fail (e.g the exposure is very quick, but the camera's bandwidth
            // is not high enough)...
            // so we are trying some (decreasing) fps until one is accepted ...
            double fps = 1E6 / paramExposure;
            setFps(fps);

            return true;
        }

        bool write(FileStorage &fs) const
        {
            paramExposure.write(fs);
            paramRepeats.write(fs);
            paramRetries.write(fs);

            checkImplPtr();

            impl->write(fs);

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            paramExposure.configure(is, os);
            paramRepeats.configure(is, os);
            paramRetries.configure(is, os);

            checkImplPtr();

            impl->configure(is, os);
            impl->setProperty(CAP_PROP_EXPOSURE, paramExposure);

            // adjust the camera's fps (if available) to be consistent with the exposure
            // note that this can fail (e.g the exposure is very quick, but the camera's bandwidth
            // is not high enough)...
            // so we are trying some (decreasing) fps until one is accepted ...
            double fps = 1E6 / paramExposure;
            setFps(fps);

            return true;
        }

        size_t hash() const
        {
            checkImplPtr();

            return impl->hash();
        }

        bool open(int device)
        {
            checkImplPtr();

            return impl->open(device);
        }

        void close()
        {
            checkImplPtr();

            impl->close();
        }

        bool isOpened() const
        {
            checkImplPtr();

            return impl->isOpened();
        }

        bool getFrame(Mat &image)
        {

            ElapsedTimer t;
            int          retry = 0;

            checkImplPtr();
            // accept a frame only if it tooks more than the exposure time to grab it
            // the idea is if it gets here quicker, it probably
            // was exposed a long time ago and just sitting in the buffer
            do
            {
                t.restart();
                // if getting an image fails .. allow some retries
                do
                {
                    if (impl->get(image) && (image.rows > 0) && (image.cols > 0))
                    {
                        break;
                    }
                    retry++;
                    sleep_for(20);
                }
                while (retry < pubPtr()->retries());

                if (retry >= pubPtr()->retries())
                {
                    return false;
                }
                // logInfo() << "elapsed" << t.elapsed()*1000 << exposureTime();
            }
            while (t.elapsed() * 1000 < pubPtr()->exposureTime());

            return true;
        }

        bool get(Mat &image)
        {
            if (pubPtr()->repeats() == 1)
            {
                return getFrame(image);
            }

            if (!getFrame(image))
            {
                return false;
            }
            tmp = Mat_<double>(image);
            for (int r = 1; r < pubPtr()->repeats(); ++r)
            {
                if (!getFrame(image))
                {
                    return false;
                }
                tmp += Mat_<double>(image);
            }
            image = Mat_<uchar>(tmp / pubPtr()->repeats());

            return true;
        }

        bool setProperty(int    propId,
                         double value)
        {
            checkImplPtr();

            return impl->setProperty(propId, value);
        }

        double getProperty(int propId)
        {
            checkImplPtr();

            return impl->getProperty(propId);
        }

        bool synchronized()
        {
            checkImplPtr();

            return impl->synchronized();
        }

        bool setSynchronized(bool sync)
        {
            checkImplPtr();

            return impl->setSynchronized(sync);
        }

        TypeParameter<int>             paramRetries;
        TypeParameter<double>          paramExposure;
        TypeParameter<int>             paramRepeats;
        CameraDevice::AbstractPrivate *impl;
        Mat                            tmp;

        CL3DS_PUB_IMPL(CameraDevice)
    };

    CameraDevice::Private::~Private()
    {
        delete impl;
    }

    static Ptr< Factory<string, CameraDevice> > makeFactory()
    {
        Ptr< Factory<string,
                     CameraDevice> > f = makePtr< Factory<string, CameraDevice> >();
        f->reg<CvCameraDevice>();
#ifdef HAVE_PVAPI
        f->reg<GigeCameraDevice>();
#endif

        return f;
    }

    Factory<string, CameraDevice> *CameraDevice::classFactory()
    {
        static Ptr< Factory<string, CameraDevice> > factory = makeFactory();

        return factory;
    }

    CameraDevice::CameraDevice(AbstractPrivate *ptr,
                               int              retries,
                               double           exposureTime,
                               int              repeats) :
        Object(new Private(this, ptr, retries, exposureTime, repeats))
    { }

    CameraDevice::~CameraDevice()
    { }

    void CameraDevice::setPrivateImplementation(CameraDevice::AbstractPrivate *pimpl)
    {
        PRIV_PTR;

        delete priv->impl;
        priv->impl = pimpl;
    }

    CameraDevice::AbstractPrivate * CameraDevice::privateImplementation()
    {
        PRIV_PTR;

        return priv->impl;
    }

    const CameraDevice::AbstractPrivate * CameraDevice::privateImplementation() const
    {
        PRIV_CONST_PTR;

        return priv->impl;
    }

    bool CameraDevice::open(int device)
    {
        PRIV_PTR;

        return priv->open(device);
    }

    void CameraDevice::close()
    {
        PRIV_PTR;

        priv->close();
    }

    bool CameraDevice::isOpened() const
    {
        PRIV_CONST_PTR;

        return priv->isOpened();
    }

    bool CameraDevice::get(Mat &image)
    {
        PRIV_PTR;

        return priv->get(image);
    }

    bool CameraDevice::setProperty(int    propId,
                                   double value)
    {
        PRIV_PTR;

        return priv->setProperty(propId, value);
    }

    double CameraDevice::getProperty(int propId)
    {
        PRIV_PTR;

        return priv->getProperty(propId);
    }

    bool CameraDevice::synchronized()
    {
        PRIV_PTR;

        return priv->synchronized();
    }

    bool CameraDevice::setSynchronized(bool sync)
    {
        PRIV_PTR;

        return priv->setSynchronized(sync);
    }

    void CameraDevice::setExposureTime(double exposure)
    {
        PRIV_PTR;

        priv->paramExposure = exposure;
        setProperty(CAP_PROP_EXPOSURE, exposure);
    }

    double CameraDevice::exposureTime() const
    {
        PRIV_CONST_PTR;

        return priv->paramExposure;
    }

    double CameraDevice::defaultExposureTime()
    {
        return 40000;
    }

    void CameraDevice::setRepeats(int repeats)
    {
        PRIV_PTR;

        priv->paramRepeats = repeats;
    }

    int CameraDevice::repeats() const
    {
        PRIV_CONST_PTR;

        return priv->paramRepeats;
    }

    int CameraDevice::defaultRepeats()
    {
        return 1;
    }

    void CameraDevice::setRetries(int retries)
    {
        PRIV_PTR;

        priv->paramRetries = retries;
    }

    int CameraDevice::retries() const
    {
        PRIV_CONST_PTR;

        return priv->paramRetries;
    }

    int CameraDevice::defaultRetries()
    {
        return 5;
    }

    CameraDevice::AbstractPrivate::AbstractPrivate(CameraDevice *device) :
        Object::AbstractPrivate(device)
    { }

    CameraDevice::AbstractPrivate::~AbstractPrivate()
    { }
}
