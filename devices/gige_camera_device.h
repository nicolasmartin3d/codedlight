/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GIGE_CAMERA_DEVICE_H
#define GIGE_CAMERA_DEVICE_H

#include "../core/config.h"
#include "camera_device.h"

#ifdef HAVE_PVAPI

#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "../core/export.h"

namespace cl3ds
{
    class CL3DS_DECLSPEC GigeCameraDevice : public CameraDevice
    {
        public:
            enum GigeCaptureMode
            {
                CAPTURE_TRIGGER_FREERUN  = 0,
                CAPTURE_TRIGGER_SOFTWARE = 1,
                CAPTURE_TRIGGER_SYNCIN1  = 2
            };

            GigeCameraDevice(int    camId=defaultCamId(),
                             int    retries=defaultRetries(),
                             double exposureTime=defaultExposureTime(),
                             int    repeats=defaultRepeats());
            ~GigeCameraDevice();

            int camId() const;
            void setCamId(int camId);
            static int defaultCamId();

            struct CallbackData;
            friend struct CallbackData;

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("gige_cam")
    };
}

#endif

#endif // GIGE_CAMERA_DEVICE_H
