/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROJECTOR_DEVICE_H
#define PROJECTOR_DEVICE_H

#include <opencv2/core/core.hpp>
#include "../core/factory.h"
#include "../core/export.h"

namespace cl3ds
{

    class Generator;
    class CL3DS_DECLSPEC ProjectorDevice : public Object
    {
        public:
            enum { DEACTIVATE_SCREENSAVER };
            enum { PROJECT_SEQUENCE_IN_LOOP };

            static Factory<std::string, ProjectorDevice> * classFactory();

            ~ProjectorDevice();

            virtual bool open(int);
            virtual void close();
            virtual bool isOpened() const;

            virtual bool put(const cv::Mat &image);
            virtual bool put(cv::Ptr<Generator> gen);
            virtual void clear();

            virtual bool setProperty(int    propId,
                                     double value);
            virtual double getProperty(int propId);

            virtual bool synchronized();
            virtual bool setSynchronized(bool);

            void setExposureTime(double ms);
            double exposureTime() const;
            static double defaultExposureTime();

            void setGamma(double gamma);
            double gamma() const;
            static double defaultGamma();

        protected:
            struct AbstractPrivate;

            ProjectorDevice(AbstractPrivate *ptr,
                            double           exposureTime=defaultExposureTime(),
                            double           gamma=defaultGamma());

        protected:
            void setPrivateImplementation(AbstractPrivate *pimpl);
            AbstractPrivate * privateImplementation();
            const AbstractPrivate * privateImplementation() const;

        private:
            struct Private;
    };
}
#endif // PROJECTOR_DEVICE_H
