/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/highgui/highgui.hpp>
#include "../core/config.h"

#include "../core/parameter.h"
#include "translation_stage.h"
#include "translation_stage_p.h"
#include "translation_stages.h"

using namespace cv;
using namespace std;

namespace cl3ds
{
#define PRIV_PTR Private * const priv = \
    static_cast<Private *>(Object::privateImplementation());
#define PRIV_CONST_PTR const Private * const priv = \
    static_cast<const Private *>(Object::privateImplementation());

    struct TranslationStage::Private : public Object::AbstractPrivate
    {
        Private(TranslationStage                  *ptr,
                TranslationStage::AbstractPrivate *childPtr) :
            Object::AbstractPrivate(ptr),
            impl(childPtr)
        { }
        ~Private();

        void checkImplPtr() const
        {
            if (!impl)
            {
                logError() << "Private pointer is null ! "
                    "You must pass a valid pointer to "
                    "the private implementation of your "
                    "object to the Object constructor";
            }
        }

        bool read(const FileNode &fn)
        {
            checkImplPtr();

            impl->read(fn);

            return true;
        }

        bool write(FileStorage &fs) const
        {
            checkImplPtr();

            impl->write(fs);

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            checkImplPtr();

            impl->configure(is, os);

            return true;
        }

        size_t hash() const
        {
            checkImplPtr();

            return impl->hash();
        }

        bool open(int device)
        {
            checkImplPtr();

            return impl->open(device);
        }

        void close()
        {
            checkImplPtr();

            impl->close();
        }

        bool isOpened() const
        {
            checkImplPtr();

            return impl->isOpened();
        }

        bool reset()
        {
            checkImplPtr();

            return impl->reset();
        }

        bool home()
        {
            checkImplPtr();

            return impl->home();
        }

        bool setPositionUnchecked(double mm)
        {
            checkImplPtr();

            return impl->setPositionUnchecked(mm);
        }

        bool setPositionChecked(double mm)
        {
            checkImplPtr();

            return impl->setPositionChecked(mm);
        }

        bool position(double &mm)
        {
            checkImplPtr();

            return impl->position(mm);
        }

        double minPosition() const
        {
            checkImplPtr();

            return impl->minPosition();
        }

        double maxPosition() const
        {
            checkImplPtr();

            return impl->maxPosition();
        }

        bool setProperty(int    propId,
                         double value)
        {
            checkImplPtr();

            return impl->setProperty(propId, value);
        }

        double getProperty(int propId)
        {
            checkImplPtr();

            return impl->getProperty(propId);
        }

        TranslationStage::AbstractPrivate *impl;
    };

    TranslationStage::Private::~Private()
    {
        delete impl;
    }

    TranslationStage::AbstractPrivate::AbstractPrivate(TranslationStage *device) :
        Object::AbstractPrivate(device)
    { }

    TranslationStage::AbstractPrivate::~AbstractPrivate()
    { }

    bool TranslationStage::AbstractPrivate::setPositionChecked(double)
    {
        return false;
    }

    static Ptr< Factory<string, TranslationStage> > makeFactory()
    {
        Ptr< Factory<string, TranslationStage> > f =
            makePtr< Factory<string, TranslationStage> >();
#ifdef HAVE_MODBUS
        f->reg<ModbusTranslationStage>();
#endif

        return f;
    }

    Factory<string, TranslationStage> *TranslationStage::classFactory()
    {
        static Ptr< Factory<string, TranslationStage> > factory = makeFactory();

        return factory;
    }

    TranslationStage::TranslationStage(AbstractPrivate *ptr) :
        Object(new Private(this, ptr))
    { }

    TranslationStage::~TranslationStage()
    { }

    void TranslationStage::setPrivateImplementation(
        TranslationStage::AbstractPrivate *pimpl)
    {
        PRIV_PTR;

        delete priv->impl;
        priv->impl = pimpl;
    }

    TranslationStage::AbstractPrivate *
    TranslationStage::privateImplementation()
    {
        PRIV_PTR;

        return priv->impl;
    }

    const TranslationStage::AbstractPrivate *
    TranslationStage::privateImplementation() const
    {
        PRIV_CONST_PTR;

        return priv->impl;
    }

    bool TranslationStage::open(int device)
    {
        PRIV_PTR;

        return priv->open(device);
    }

    void TranslationStage::close()
    {
        PRIV_PTR;

        priv->close();
    }

    bool TranslationStage::isOpened() const
    {
        PRIV_CONST_PTR;

        return priv->isOpened();
    }

    bool TranslationStage::reset()
    {
        PRIV_PTR;

        return priv->reset();
    }

    bool TranslationStage::home()
    {
        PRIV_PTR;

        return priv->home();
    }

    bool TranslationStage::move(double mm)
    {
        double pos;
        if (!position(pos)) { return false; }

        return setPosition(pos + mm);
    }

    bool TranslationStage::setPosition(double mm,
                                       bool   waitForCompletion)
    {
        PRIV_PTR;

        if (waitForCompletion)
        {
            if (!priv->setPositionChecked(mm))
            {
                // no checked position setter, doing the check ourselves
                if (!priv->setPositionUnchecked(mm))
                {
                    return false;
                }

                double pos;
                do
                {
                    if (!priv->position(pos))
                    {
                        return false;
                    }
                    sleep_for(5);
                }
                while (std::abs(pos - mm) > 0.001); // resolution ?
            }
        }
        else
        {
            return priv->setPositionUnchecked(mm);
        }

        return true;
    }

    bool TranslationStage::position(double &mm)
    {
        PRIV_PTR;

        return priv->position(mm);
    }

    double TranslationStage::minPosition() const
    {
        PRIV_CONST_PTR;

        return priv->minPosition();
    }

    double TranslationStage::maxPosition() const
    {
        PRIV_CONST_PTR;

        return priv->maxPosition();
    }

    bool TranslationStage::setProperty(int    propId,
                                       double value)
    {
        PRIV_PTR;

        return priv->setProperty(propId, value);
    }

    double TranslationStage::getProperty(int propId)
    {
        PRIV_PTR;

        return priv->getProperty(propId);
    }
}
