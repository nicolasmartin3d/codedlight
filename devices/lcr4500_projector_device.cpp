/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>
#include <mvg/logger.h>
#include <lcr-api/lcr-api.h>
#include "../core/parameter.h"
#include "../patterns/combiner.h"
#include "projector_device_p.h"
#include "lcr4500_projector_device.h"

using namespace cv;
using namespace std;

static LcrPattern * createLcrPattern(int flashNum,
                                     int patNum,
                                     int depth,
                                     int led)
{
    LcrPattern *pat = static_cast<LcrPattern *>(malloc(sizeof(LcrPattern)));
    pat->flashImgNum = flashNum;
    pat->patNum      = patNum;
    if ((depth != 1) && (depth != 8))
    {
        logError() << "Depth should either be 1 or 8";
    }
    pat->depth   = depth;
    pat->led     = led;
    pat->invert  = false;
    pat->trigOut = false;
    pat->trigIn  = 3;
    pat->swap    = true;
    pat->black   = true;

    return pat;
}

// to serialize LCRPattern ..
static void write(FileStorage      &fs,
                  const std::string &,
                  const LcrPattern &p)
{
    fs << "[";
    fs << p.flashImgNum << p.trigIn << p.patNum << p.depth << p.led << int(p.invert) <<
        int(p.black) << int(p.swap) << int(p.trigOut);
    fs << "]";
}

static void read(const FileNode   &node,
                 LcrPattern       &p,
                 const LcrPattern &defP=LcrPattern())
{
    if (node.empty() || (node.type() != FileNode::SEQ))
    {
        p = defP;
    }
    else
    {
        FileNodeIterator it = node.begin(), itE = node.end();

        if (it == itE) { logWarning() << "Missing value for pattern"; return; }
        p.flashImgNum = *it++;

        if (it == itE) { logWarning() << "Missing value for pattern"; return; }
        p.trigIn = *it++;

        if (it == itE) { logWarning() << "Missing value for pattern"; return; }
        p.patNum = *it++;

        if (it == itE) { logWarning() << "Missing value for pattern"; return; }
        p.depth = *it++;

        if (it == itE) { logWarning() << "Missing value for pattern"; return; }
        p.led = *it++;

        if (it == itE) { logWarning() << "Missing value for pattern"; return; }
        p.invert = (static_cast<int>(*it++) != 0);

        if (it == itE) { logWarning() << "Missing value for pattern"; return; }
        p.black = (static_cast<int>(*it++) != 0);

        if (it == itE) { logWarning() << "Missing value for pattern"; return; }
        p.swap = (static_cast<int>(*it++) != 0);

        if (it == itE) { logWarning() << "Missing value for pattern"; return; }
        p.trigOut = (static_cast<int>(*it++) != 0);
    }
}

namespace cl3ds
{
    struct LCR4500ProjectorDevice::Private : public AbstractPrivate
    {
        Private(LCR4500ProjectorDevice *ptr) :
            AbstractPrivate(ptr),
            opened(false)
        {
            combiner          = makePtr<Combiner>();
            paramFirmwarePath = TypeParameter<string>("firmwarePath",
                                                      "Path to the .BIN file used to upload the firmware.",
                                                      "");

            paramPatternWidth = TypeParameter<int>("patternWidth",
                                                   "Width of a pattern image", 912);

            paramPatternHeight = TypeParameter<int>("patternHeight",
                                                    "Height of a pattern image", 1140);

            paramLoopSequence = TypeParameter<bool>("loopSequence",
                                                    "True if sequence should be projected in loop",
                                                    false);
            open(0);
        }

        ~Private();

        bool read(const FileNode &fn)
        {
            close();

            fn["patterns"] >> combiner;

            paramFirmwarePath.read(fn);
            paramPatternWidth.read(fn);
            paramPatternHeight.read(fn);
            paramLoopSequence.read(fn);

            registerFirmware();

            open(0);

            return true;
        }

        bool write(FileStorage &fs) const
        {
            fs << "patterns" << combiner;

            paramFirmwarePath.write(fs);
            paramPatternWidth.write(fs);
            paramPatternHeight.write(fs);
            paramLoopSequence.write(fs);

            return true;
        }

        bool configure(istream *is,
                       ostream *os)
        {
            close();

            // the global configure function will allocate the pointer for us
            // and possibly read the parameters from another file if they are not embedded
            // in this one;
            cl3ds::configure(combiner, is, os,
                             string(),
                             makePtr<Combiner>(),
                             true, false);

            paramFirmwarePath.configure(is, os);
            paramPatternWidth.configure(is, os);
            paramPatternHeight.configure(is, os);
            paramLoopSequence.configure(is, os);

            registerFirmware();

            open(0);

            return true;
        }
        size_t hash() const
        {
            return Hasher() << paramFirmwarePath
                            << paramPatternHeight
                            << paramPatternWidth
                            << paramLoopSequence;
        }

        bool synchronized()
        {
            return true;
        }

        bool setSynchronized(bool)
        {
            return true;
        }

        void clear()
        {
            lcrStopSequence();
        }

        bool open(int)
        {
            int k = lcrInit();
            opened = (k == 0);

            // lcrReset();
            return opened;
        }

        void close() { lcrClose(); }
        bool isOpened() const { return opened; }

        bool put(const Mat &image);
        bool put(Generator *const gen);

        bool setProperty(int    prop,
                         double value)
        {
            if (prop == ProjectorDevice::PROJECT_SEQUENCE_IN_LOOP)
            {
                if ((value >= 0) && (value <= 1))
                {
                    paramLoopSequence = (fabs(value) <= 1E-4 ? false : true);
                }
            }

            return true;
        }

        double getProperty(int prop)
        {
            if (prop == ProjectorDevice::PROJECT_SEQUENCE_IN_LOOP)
            {
                return paramLoopSequence;
            }

            return -1;
        }

        void registerFirmware();

        bool opened;
        // mandatory ugly raw pointers because of C API of lcr-api
        std::map<size_t, pair<Ptr<Generator>, LcrPattern *> > hashMap;
        std::map<size_t, vector<LcrPattern *> >               hashGeneratorsMap;
        std::map<size_t, vector<LcrPattern *> >               hashImageMap;
        Ptr<Combiner>                                         combiner;

        TypeParameter<string> paramFirmwarePath;
        TypeParameter<int>    paramPatternWidth;
        TypeParameter<int>    paramPatternHeight;
        TypeParameter<bool>   paramLoopSequence;

        CL3DS_PUB_IMPL(LCR4500ProjectorDevice)
    };

    LCR4500ProjectorDevice::Private::~Private()
    {
        std::map<size_t,
                 pair< Ptr<Generator>, LcrPattern *> >::iterator it = hashMap.begin();
        std::map<size_t,
                 pair< Ptr<Generator>, LcrPattern *> >::iterator itE = hashMap.end();
        // mandatory ugly free because of C API of lcr-api
        for (; it != itE; ++it)
        {
            free(it->second.second);
        }
        close();
    }

    static vector<LcrPattern *> findBestSequence(vector< vector<LcrPattern *> > &sequences)
    {
        size_t numPatts = sequences.size();

        vector<LcrPattern *> bestSeq(numPatts, static_cast<LcrPattern *>(0));
        for (size_t i = 0; i < numPatts; ++i)
        {
            size_t seqSize = sequences[i].size();
            if (seqSize == 1)
            {
                bestSeq[i] = sequences[i][0];
            }
        }

        bool changed;
        do
        {
            // greedy heuristic
            changed = false;
            for (size_t i = 0; i < numPatts; ++i)
            {
                if (bestSeq[i])
                {
                    if ((i > 0) && !bestSeq[i - 1])
                    {
                        vector<size_t> optimalIds;
                        int            id = bestSeq[i]->flashImgNum * 3 +
                                            bestSeq[i]->patNum;
                        for (size_t j = 0; j < sequences[i - 1].size(); ++j)
                        {
                            int curId = sequences[i - 1][j]->flashImgNum * 3 +
                                        sequences[i - 1][j]->patNum;
                            if (abs(id - curId) <= 1) { optimalIds.push_back(j); }
                        }
                        if (optimalIds.size() == 1)
                        {
                            bestSeq[i - 1] = sequences[i - 1][optimalIds[0]];
                            sequences[i - 1].clear();
                            sequences[i - 1].push_back(bestSeq[i - 1]);
                            changed = true;
                        }
                    }
                    if ((i < numPatts - 1) && !bestSeq[i + 1])
                    {
                        vector<size_t> optimalIds;
                        int            id = bestSeq[i]->flashImgNum * 3 +
                                            bestSeq[i]->patNum;
                        for (size_t j = 0; j < sequences[i + 1].size(); ++j)
                        {
                            int curId = sequences[i + 1][j]->flashImgNum * 3 +
                                        sequences[i + 1][j]->patNum;
                            if (abs(id - curId) <= 1) { optimalIds.push_back(j); }
                        }
                        if (optimalIds.size() == 1)
                        {
                            bestSeq[i + 1] = sequences[i + 1][optimalIds[0]];
                            sequences[i + 1].clear();
                            sequences[i + 1].push_back(bestSeq[i + 1]);
                            changed = true;
                        }
                    }
                }
            }
        }
        while (changed);

        size_t n = 1;
        for (size_t i = 0; i < numPatts; ++i)
        {
            size_t seqSize = sequences[i].size();
            n *= seqSize;
        }

        int bestCost = numeric_limits<int>::max();
        // genere toutes les combinaisons
        for (size_t i = 0; i < n; ++i)
        {
            vector<LcrPattern *> perm;
            size_t               c = i;
            for (size_t j = 0; j < sequences.size(); ++j)
            {
                size_t id = c % sequences[j].size();
                c /= sequences[j].size();
                perm.push_back(sequences[j][id]);
            }

            int cost = 0;
            for (size_t j = 1; j < sequences.size(); ++j)
            {
                cost += abs((perm[j]->flashImgNum * 3 + perm[j]->patNum) -
                            (perm[j - 1]->flashImgNum * 3 + perm[j - 1]->patNum));
            }
            if (cost < bestCost)
            {
                bestCost = cost;
                bestSeq  = perm;
            }
        }

        // clone the sequence
        for (size_t i = 0; i < numPatts; ++i)
        {
            LcrPattern *tmp = bestSeq[i];
            bestSeq[i]  = static_cast<LcrPattern *>(malloc(sizeof(LcrPattern)));
            *bestSeq[i] = *tmp;
        }

        // fix the sequence !
        bestSeq[0]->swap  = true;
        bestSeq[0]->black = true;
        for (size_t i = 1; i < numPatts; ++i)
        {
            if (bestSeq[i]->flashImgNum == bestSeq[i - 1]->flashImgNum)
            {
                bestSeq[i]->swap = false;
            }
            else
            {
                bestSeq[i]->swap = true;
            }
            bestSeq[i]->black = true;
        }

        return bestSeq;
    }

    void LCR4500ProjectorDevice::Private::registerFirmware()
    {
        string   path(paramFirmwarePath);
        ifstream ifs(path.c_str(), ios::binary);
        if (!ifs)
        {
            logWarning() << "The firmware file does not exist or can't be opened";

            return;
        }

        // Computing firmware hash
        ifs.seekg(0, std::ios::end);
        streampos length = ifs.tellg();
        ifs.seekg(0, std::ios::beg);

        std::vector<char> buffer(static_cast<size_t>(length));
        ifs.read(&buffer[0], length);
        ifs.close();

        size_t firmwareHash = (Hasher() << buffer);

        string      pathXml(path + ".xml");
        FileStorage fs;

        string      s;
        size_t      h;
        LcrPattern *pat;
        bool        generateDescription = false;

        if (fs.open(pathXml, FileStorage::READ))
        {
            fs["firmwareHash"] >> s;
            istringstream(s) >> h;
            if (h != firmwareHash)
            {
                logWarning() <<
                    "Firmware hash mismatch .. The firmware has changed, regenerating the descrition file";
                generateDescription = true;
            }

            fs["generatorsHash"] >> s;
            istringstream(s) >> h;
            if (h != combiner->hash())
            {
                logWarning() <<
                    "Generators mismatch .. The generators have changed, regenerating the descrition file";
                generateDescription = true;
            }
        }
        else
        {
            logWarning() <<
                "No description file found for the firmware, will now generate one."
                         << "This may take some time.";
            generateDescription = true;
        }

        if (generateDescription)
        {
            bool canWrite = fs.open(pathXml, FileStorage::WRITE);
            if (!canWrite)
            {
                logWarning() << "Unable to write the firmware description file."
                             << "I will have to generate a new one next time again :(";
            }

            if (canWrite)
            {
                fs << "firmwareHash"
                   << (static_cast<std::ostringstream &>(
                        std::ostringstream().flush() << firmwareHash).str());
                fs << "generatorsHash"
                   << (static_cast<std::ostringstream &>(
                        std::ostringstream().flush() << combiner->hash()).str());
            }

            vector<unsigned char *> images;
            lcrUnpackFirmware(string(paramFirmwarePath).c_str(), images);
            const size_t nbImages = images.size();

            if (canWrite)
            {
                fs << "nbImages" << static_cast<int>(nbImages)
                   << "images" << "[";
            }

            Size imageSize(paramPatternWidth, paramPatternHeight);
            Mat  mergedImage(imageSize, CV_8UC3), binImage(imageSize, CV_8U);
            for (size_t i = 0; i < nbImages; ++i)
            {
                vector<Mat> channels;
                Mat         image(imageSize, CV_8UC3, images[i]);
                split(image, channels);

                // channels = BGR
                // 0 -> LCR_LED_BLUE
                // 1 -> LCR_LED_GREEN
                // 2 -> LCR_LED_RED
                // 0 and 1 -> LCR_LED_CYAN
                // 0 and 2 -> LCR_LED_MAGENTA
                // 1 and 2 -> LCR_LED_YELLOW
                // all -> LCR_LED_WHITE

                for (size_t j = 0; j < 3; ++j)
                {
                    // images are stored in GRB in the DLP
                    // images are stored in BGR in Opencv
                    // so starting position is (j+2)%3
                    int startPos = (j + 2) % 3;

                    // 0 -> LCR_LED_BLUE
                    int f0[] = { 0, 0, -1, 1, -1, 2 };
                    mixChannels(&channels[j], 1, &mergedImage, 1, f0, 3);

                    h   = Hasher() << mergedImage;
                    pat = createLcrPattern(static_cast<int>(i), startPos, 8,
                                           LCR_LED_BLUE);
                    hashImageMap[h].push_back(pat);
                    if (canWrite)
                    {
                        fs << (static_cast<std::ostringstream &>(
                                   std::ostringstream().flush() << h).str())
                           << *pat;
                    }

                    // 1 -> LCR_LED_GREEN
                    int f1[] = { -1, 0, 0, 1, -1, 2 };
                    mixChannels(&channels[j], 1, &mergedImage, 1, f1, 3);

                    h   = Hasher() << mergedImage;
                    pat = createLcrPattern(static_cast<int>(i), startPos, 8,
                                           LCR_LED_GREEN);
                    hashImageMap[h].push_back(pat);
                    if (canWrite)
                    {
                        fs << (static_cast<std::ostringstream &>(
                                   std::ostringstream().flush() << h).str())
                           << *pat;
                    }

                    // 2 -> LCR_LED_RED
                    int f2[] = { -1, 0, -1, 1, 0, 2 };
                    mixChannels(&channels[j], 1, &mergedImage, 1, f2, 3);

                    h   = Hasher() << mergedImage;
                    pat = createLcrPattern(static_cast<int>(i), startPos, 8,
                                           LCR_LED_RED);
                    hashImageMap[h].push_back(pat);
                    if (canWrite)
                    {
                        fs << (static_cast<std::ostringstream &>(
                                   std::ostringstream().flush() << h).str())
                           << *pat;
                    }

                    // 0 and 1 -> LCR_LED_CYAN
                    int f3[] = { 0, 0, 0, 1, -1, 2 };
                    mixChannels(&channels[j], 1, &mergedImage, 1, f3, 3);

                    h   = Hasher() << mergedImage;
                    pat = createLcrPattern(static_cast<int>(i), startPos, 8,
                                           LCR_LED_CYAN);
                    hashImageMap[h].push_back(pat);
                    if (canWrite)
                    {
                        fs << (static_cast<std::ostringstream &>(
                                   std::ostringstream().flush() << h).str())
                           << *pat;
                    }

                    // 0 and 2 -> LCR_LED_MAGENTA
                    int f4[] = { 0, 0, -1, 1, 0, 2 };
                    mixChannels(&channels[j], 1, &mergedImage, 1, f4, 3);

                    h   = Hasher() << mergedImage;
                    pat = createLcrPattern(static_cast<int>(i), startPos, 8,
                                           LCR_LED_MAGENTA);
                    hashImageMap[h].push_back(pat);
                    if (canWrite)
                    {
                        fs << (static_cast<std::ostringstream &>(
                                   std::ostringstream().flush() << h).str())
                           << *pat;
                    }

                    // 1 and 2 -> LCR_LED_YELLOW
                    int f5[] = { -1, 0, 0, 1, 0, 2 };
                    mixChannels(&channels[j], 1, &mergedImage, 1, f5, 3);

                    h   = Hasher() << mergedImage;
                    pat = createLcrPattern(static_cast<int>(i), startPos, 8,
                                           LCR_LED_YELLOW);
                    hashImageMap[h].push_back(pat);
                    if (canWrite)
                    {
                        fs << (static_cast<std::ostringstream &>(
                                   std::ostringstream().flush() << h).str())
                           << *pat;
                    }

                    // all -> LCR_LED_WHITE
                    int f6[] = { 0, 0, 0, 1, 0, 2 };
                    mixChannels(&channels[j], 1, &mergedImage, 1, f6, 3);

                    h   = Hasher() << mergedImage;
                    pat = createLcrPattern(static_cast<int>(i), startPos, 8,
                                           LCR_LED_WHITE);
                    hashImageMap[h].push_back(pat);
                    if (canWrite)
                    {
                        fs << (static_cast<std::ostringstream &>(
                                   std::ostringstream().flush() << h).str())
                           << *pat;
                    }

                    // 8bit image composed of channels[j]
                    h = Hasher() << channels[j];
                    hashImageMap[h].push_back(pat);
                    if (canWrite)
                    {
                        fs << (static_cast<std::ostringstream &>(
                                   std::ostringstream().flush() << h).str())
                           << *pat;
                    }
                    /*
                     *   if (i==0 && j==1) {
                     *   logInfo() << h << int(channels[j].at<uchar>(0,0)) << channels[j].rows <<
                     * channels[j].cols*channels[j].elemSize() << sizeof(size_t);
                     *   imshow("pattern", channels[j]);
                     *   waitKey(10);
                     *   }
                     */

                    // limited support for binary patterns with WHITE_LED
                    for (int k = 0; k < 8; ++k)
                    {
                        for (MatIterator_<uchar> itA = channels[j].begin<uchar>(),
                             itAe = channels[j].end<uchar>(),
                             itB = binImage.begin<uchar>(); itA != itAe; ++itA, ++itB)
                        {
                            *itB = 255 * ((*itA >> k) & 1);
                        }
                        h   = Hasher() << binImage;
                        pat = createLcrPattern(static_cast<int>(i), startPos * 8 + k, 1,
                                               LCR_LED_WHITE);
                        hashImageMap[h].push_back(pat);

                        if (canWrite)
                        {
                            fs << (static_cast<std::ostringstream &>(
                                       std::ostringstream().flush() << h).str())
                               << *pat;
                        }
                    }
                }

                free(images[i]);
            }

            if (canWrite) { fs << "]"; }

            // register generators
            const size_t size = combiner->numGenerators();
            if (canWrite)
            {
                fs << "nbGenerators" << static_cast<int>(size)
                   << "generators" << "[";
            }

            for (size_t i = 0; i < size; ++i)
            {
                Generator   *gen         = combiner->generator(i);
                const size_t numPatterns = gen->count();
                gen->reset();

                Mat                            tmp;
                size_t                         hash;
                vector< vector<LcrPattern *> > possibleSequences;

                possibleSequences.resize(numPatterns);
                map<size_t, vector<LcrPattern *> >::const_iterator it;
                for (size_t j = 0; j < numPatterns; ++j)
                {
                    tmp  = gen->get();
                    hash = (Hasher() << tmp);
                    it   = hashImageMap.find(hash);
                    if (it == hashImageMap.end())
                    {
                        fs.release();
                        remove(pathXml.c_str());
                        imshow("image", tmp);
                        waitKey(0);
                        logError() <<
                            "Impossible de trouver une image requise dans un des generateurs"
                                   << "image hash" << hash
                                   << "generator" << i << "pattern" << j;
                    }
                    else
                    {
                        logDebug() << "Succesfully found a match for pattern" << j <<
                            "of generator" << i;
                    }
                    possibleSequences[j] = it->second;
                }

                vector<LcrPattern *> bestSeq = findBestSequence(possibleSequences);
                hash                    = gen->hash();
                hashGeneratorsMap[hash] = bestSeq;

                if (canWrite)
                {
                    fs << (static_cast<std::ostringstream &>(
                               std::ostringstream().flush() << hash).str());
                    const size_t count = bestSeq.size();
                    fs << static_cast<int>(count);
                    for (size_t j = 0; j < count; ++j)
                    {
                        fs << *bestSeq[j];
                    }
                }
            }

            if (canWrite) { fs << "]"; }
        }
        else
        {
            int      nbImages = fs["nbImages"];
            FileNode node     = fs["images"];
            if (node.type() != FileNode::SEQ)
            {
                logWarning() <<
                    "Images are not stored in a sequence.. Wrong firmware description file";

                return;
            }

            FileNodeIterator it = node.begin(), it_end = node.end();
            for (int i = 0; i < nbImages; ++i)
            {
                for (int k = 0; k < 3; ++k)
                {
                    for (int j = 0; j < 8; ++j)
                    {
                        if (it == it_end)
                        {
                            logWarning() <<
                                "Wrong description of the firmware, missing value for the pattern";

                            return;
                        }
                        *it >> s;
                        it++;
                        istringstream(s) >> h;

                        if (it == it_end)
                        {
                            logWarning() <<
                                "Wrong description of the firmware, missing value for the pattern";

                            return;
                        }

                        // mandatory ugly malloc because of C API of lcr-api
                        pat = static_cast<LcrPattern *>(malloc(sizeof(LcrPattern)));
                        if (it == it_end)
                        {
                            logWarning() <<
                                "Wrong description of the firmware, missing value for the pattern";

                            return;
                        }
                        *it >> *(pat);
                        it++;
                        hashImageMap[h].push_back(pat);
                    }

                    // binary patterns !
                    for (int j = 0; j < 8; ++j)
                    {
                        if (it == it_end)
                        {
                            logWarning() <<
                                "Wrong description of the firmware, missing value for the pattern";

                            return;
                        }
                        *it >> s;
                        it++;
                        istringstream(s) >> h;

                        if (it == it_end)
                        {
                            logWarning() <<
                                "Wrong description of the firmware, missing value for the pattern";

                            return;
                        }

                        // mandatory ugly malloc because of C API of lcr-api
                        pat = static_cast<LcrPattern *>(malloc(sizeof(LcrPattern)));
                        if (it == it_end)
                        {
                            logWarning() <<
                                "Wrong description of the firmware, missing value for the pattern";

                            return;
                        }
                        *it >> *(pat);
                        it++;
                        hashImageMap[h].push_back(pat);
                    }
                }
            }

            int nbGens = fs["nbGenerators"];
            node = fs["generators"];
            if (node.type() != FileNode::SEQ)
            {
                logWarning() <<
                    "Generators are not stored in a sequence.. Wrong firmware description file";

                return;
            }

            it = node.begin(), it_end = node.end();
            for (int i = 0; i < nbGens; ++i)
            {
                if (it == it_end)
                {
                    logWarning() <<
                        "Wrong description of the firmware, missing value for the pattern";

                    return;
                }
                s = static_cast<string>(*it++);
                istringstream(s) >> h;

                if (it == it_end)
                {
                    logWarning() <<
                        "Wrong description of the firmware, missing value for the pattern";

                    return;
                }
                size_t count = static_cast<size_t>(static_cast<int>(*it++));

                vector<LcrPattern *> sequence(count);
                for (size_t j = 0; j < count; ++j)
                {
                    // mandatory ugly malloc because of C API of lcr-api
                    sequence[j] = static_cast<LcrPattern *>(malloc(sizeof(LcrPattern)));
                    if (it == it_end)
                    {
                        logWarning() <<
                            "Wrong description of the firmware, missing value for the pattern";

                        return;
                    }
                    *it >> *(sequence[j]);
                    it++;
                    /*
                     *   logInfo() << "Generator" << j;
                     *   logInfo() << "[";
                     *   logInfo() << sequence[j]->flashImgNum <<
                     *             sequence[j]->trigIn << sequence[j]->patNum <<
                     *             sequence[j]->depth <<
                     *             sequence[j]->led << int(sequence[j]->invert) <<
                     *             int(sequence[j]->black) << int(sequence[j]->swap) <<
                     *             int(sequence[j]->trigOut);
                     *   logInfo() << "]";
                     */
                }
                hashGeneratorsMap[h] = sequence;
            }
        }
    }

    bool LCR4500ProjectorDevice::Private::put(const Mat &image)
    {
        size_t h = Hasher() << image;

        map<size_t, vector<LcrPattern *> >::const_iterator it = hashImageMap.find(h);
        if (it == hashImageMap.end())
        {
            logWarning() << "The image can not be displayed with the DLP projector\n"
                "Only images registered in the firmware can be projected.\n"
                " Image hash is :" << h;
            logWarning() << "Available hashes are :";
            for (it = hashImageMap.begin(); it != hashImageMap.end(); ++it)
            {
                logWarning() << "\t" << it->first;
            }

            return false;
        }
        // logInfo() << "found" << it->second[0]->flashImgNum << it->second[0]->patNum <<
        // pubPtr()->exposureTime();

        lcrStopSequence();
        // lcrDumpSequence(it->second[0], 1);

        // printf("========================================\n");
        // lcrStatus();

        // should be adjusted based on properties
        int ret = lcrSendSequence(it->second[0], 1, true,
                                  static_cast<int>(pubPtr()->exposureTime()),
                                  static_cast<int>(pubPtr()->exposureTime() - 500));
        if (ret < 0)
        {
            logWarning() <<
                "The sequence could not be played by the LCR. lcrSendSequence returned" <<
                ret;

            return false;
        }

        lcrStartSequence();

        // lcrStatus();
        // printf("========================================\n\n\n");

        return true;
    }

    bool LCR4500ProjectorDevice::Private::put(Generator *const gen)
    {
        size_t                                             h  = gen->hash();
        map<size_t, vector<LcrPattern *> >::const_iterator it = hashGeneratorsMap.find(h);
        if (it == hashGeneratorsMap.end())
        {
            logWarning() << "Generator can not be displayed with the DLP projector\n"
                "Only generator registered in the firmware can be projected";

            return false;
        }

        lcrStopSequence();

        // should be adjusted based on properties
        int ret = lcrSendSequence(it->second, paramLoopSequence,
                                  static_cast<int>(pubPtr()->exposureTime()),
                                  static_cast<int>(pubPtr()->exposureTime() - 500));

        if (ret < 0)
        {
            logWarning() <<
                "The sequence could not be played by the LCR. lcrSendSequence returned" <<
                ret;

            return false;
        }

        lcrStartSequence();

        return true;
    }

    LCR4500ProjectorDevice::LCR4500ProjectorDevice(double exposureTime,
                                                   double gamma) :
        ProjectorDevice(new Private(this), exposureTime, gamma)
    { }

    LCR4500ProjectorDevice::~LCR4500ProjectorDevice()
    { }
}
