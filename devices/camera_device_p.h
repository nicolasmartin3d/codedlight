/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CAMERA_DEVICE_P_H
#define CAMERA_DEVICE_P_H

#include "../core/object_p.h"
#include "camera_device.h"

namespace cl3ds
{

    /** Base class for an opaque implementation of a CameraDevice. */
    struct CL3DS_DECLSPEC CameraDevice::AbstractPrivate : public Object::AbstractPrivate
    {
        AbstractPrivate(CameraDevice *device);
        ~AbstractPrivate();

        virtual bool open(int)        = 0;
        virtual void close()          = 0;
        virtual bool isOpened() const = 0;

        virtual bool get(cv::Mat &image) = 0;

        virtual bool setProperty(int    propId,
                                 double value) = 0;
        virtual double getProperty(int propId) = 0;

        virtual bool synchronized()        = 0;
        virtual bool setSynchronized(bool) = 0;
    };
}

#endif // CAMERA_DEVICE_P_H

