/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRANSLATION_STAGE_H
#define TRANSLATION_STAGE_H

#include <opencv2/core/core.hpp>
#include "../core/factory.h"
#include "../core/export.h"

namespace cl3ds
{
    class CL3DS_DECLSPEC TranslationStage : public Object
    {
        public:
            static Factory<std::string, TranslationStage> * classFactory();

            virtual ~TranslationStage();

            virtual bool open(int);
            virtual void close();
            virtual bool isOpened() const;

            virtual bool reset();
            virtual bool home();

            // override if you have a more efficient way of moving the stage !
            virtual bool move(double mm);

            virtual bool setPosition(double mm,
                                     bool   waitForCompletion=true);
            virtual bool position(double &mm);
            virtual double minPosition() const;
            virtual double maxPosition() const;

            virtual bool setProperty(int    propId,
                                     double value);
            virtual double getProperty(int propId);

        protected:
            struct AbstractPrivate;

            TranslationStage(AbstractPrivate *ptr);

        protected:
            void setPrivateImplementation(AbstractPrivate *pimpl);
            AbstractPrivate * privateImplementation();
            const AbstractPrivate * privateImplementation() const;

        private:
            struct Private;
    };
}

#endif // TRANSLATION_STAGE_H

