.. _chapter-installation:

=======================
Building & Installation
=======================

Getting the source code
=======================
.. _section-install-source:

First, get the source code :

* clone the git repository

  .. code-block:: bash

       git clone https://bitbucket.org/nicolasmartin3d/codedlight
* or download the latest
  `stable release <https://bitbucket.org/nicolasmartin3d/codedlight/get/2.0.tar.gz>`_.


.. _section-install-dependencies:

Dependencies
============

Software needed to get and compile source code :
""""""""""""""""""""""""""""""""""""""""""""""""

- `CMake <http://www.cmake.org>`_ 2.8.0 or later.
  **Required**

  ``CMake`` is used to generate platform independent compilation rules and
  greatly simplifies the build process.

- `git <http://git-scm.com/>`_ 1.9.0 or later.
  **Required**

  ``git`` is used to extract the source code of this library and some of its
  dependencies.

CodedLight also relies on a number of open source libraries, some of which are
optional. Some hardware may require additional (proprietary) drivers to work,
see :ref:`section-install-hardware` .

.. _section-install-software:

Libraries required to compile core functionalities
""""""""""""""""""""""""""""""""""""""""""""""""""

- `OpenCV <http://opencv.org>`_
  Both versions 2 and 3 are supported. Recommended 2.4.6 or later, or 3.0.alpha or later. **Required**

  ``OpenCV`` is required to perform linear algebra, elementary vision and image
  processing, and input/output operations. It also provides very basic
  windowing operations as well as camera support through the ``highgui``
  module. Thus, ``core``, ``imgproc``, ``flann`` and ``highgui`` modules are
  required for CodedLight to compile.

- `Ceres <http://ceres-solver.org>`__ 1.8.0 or later. **Required**
  
  ``Ceres`` is required to perform robust camera-projector system calibration, which is a mandatory step to
  enable 3D reconstructions from correspondence maps. 

- `MVG <https://bitbucket.org/nicolasmartin3d/mvg>`__ 1.0.0 or
  later. **Required**

  ``MVG`` adds several computer vision functionality to OpenCV and is also used to handle 3D meshes.

- `TinyTag
  <https://bitbucket.org/nicolasmartin3d/tinytag>`_. Needed for
  calibration purposes. **Optional**

  ``TinyTag`` is a simple library that implements fiducial markers generation and detection.
  It is only used when calibrating devices.

- `xTCLAP <https://bitbucket.org/nicolasmartin3d/xtclap>`_. Needed to build
  examples and tests. **Optional**

  ``xTCLAP`` is an extension to `TCLAP <http://tclap.sourceforge.net/>`_ . If
  not found, examples, tests and applications for ``MVG``, ``TinyTag`` and
  ``CodedLight`` will not be built.

.. _section-install-hardware:

Libraries required to support optional hardware
"""""""""""""""""""""""""""""""""""""""""""""""

- `LCR-API <https://bitbucket.org/nicolasmartin3d/lcr-api>`_.
  Needed to control the LightCrafter 4500 projector. **Optional**

- `PvAPI <http://www.alliedvisiontec.com/us/products/legacy.html>`_.
  Needed to control the AVT Gige cameras. **Optional**

  .. NOTE ::

       The Vimba SDK is not yet supported on all platforms. For this reason,
       only legacy PvAPI is supported.

- `libmodbus <http://libmodbus.org>`_.
  Needed to control some translation stage. **Optional**

.. _section-install-linux:

Building on Ubuntu or derivatives
=================================

Following instructions were tested on `Xubuntu 14.04 <http://xubuntu.org/news/tag/14-04>`_ .
There is a good chance that these instructions are also valid for any flavors of 
`Ubuntu <http://www.ubuntu.com/>`_ and `Debian <https://www.debian.org>`_ .

1. ``git`` and ``cmake``

    .. code-block:: bash

        sudo apt-get install cmake build-essential git

2. ``OpenCV``

    .. NOTE ::
       
        If ``CodedLight`` will be used to project patterns on a secondary (real or virtual) screen,
        it is recommended to use ``Qt`` as a backend for ``highgui``, which is not the default backend
        on most platforms. It enables ``CodedLight`` to better control the window used to display
        the patterns (e.g to remove decorations).

        Since ``GTK`` is the backend used in ``libopencv-highgui2.4``, it will not be used for the
        following instructions. If you plan on using CodedLight's facility to
        project patterns using an auxiliary screen, you can compile using those packages,
        or a version of ``OpenCV`` that does not use ``Qt``. In this case, any operation that requires
        access to the window will echo a warning and may fail.

    **If CodedLight will be used for video projection (requires Qt)**

    .. code-block:: bash

        # Install Qt5 
        sudo apt-get install qt5-default

        wget https://github.com/Itseez/opencv/archive/3.0.0-alpha.zip
        unzip 3.0.0-alpha.zip
        cd opencv-3.0.0-alpha
        mkdir build && cd build
        cmake -DWITH_QT=ON -DWITH_OPENGL=ON \
              -DBUILD_DOCS=OFF -DBUILD_EXAMPLES=OFF -DBUILD_TESTS=OFF \
              -DBUILD_PERF_TESTS=OFF -DBUILD_PRECOMPILED_HEADERS=OFF ..
        make -j4 && sudo make install

    **If CodedLight will not be used for video projection**

    .. code-block:: bash

        sudo apt-get install libopencv-dev

3. ``Ceres``  

    Steps needed to build Ceres are explained in
    details `here <http://ceres-solver.org/building.html#building-on-linux>`__ .

    .. code-block:: bash

        sudo apt-get install libgoogle-glog-dev libeigen3-dev libsuitesparse-dev
        wget http://ceres-solver.org/ceres-solver-1.9.0.tar.gz
        tar zxf ceres-solver-1.9.0.tar.gz
        cd ceres-solver-1.9.0
        mkdir build && cd build
        cmake -DBUILD_DOCS=OFF -DBUILD_EXAMPLES=OFF -DBUILD_TESTING=OFF 
              -DBUILD_SHARED_LIBS=ON ..
        make -j4 && sudo make install

4. ``xTCLAP`` *(Optional)* 

    .. code-block:: bash

        git clone https://bitbucket.org/nicolasmartin3d/xtclap
        cd xtclap 
        mkdir build && cd build && cmake ..
        make -j4 && sudo make install

5. ``MVG`` 

    .. code-block:: bash

        # OpenThreads is required by MVG
        sudo apt-get install libopenthreads-dev

        wget -O mvg-1.0.tar.gz 
            https://bitbucket.org/nicolasmartin3d/mvg/get/1.0.tar.gz 
        tar zxf mvg-1.0.tar.gz -C mvg --strip-components 1
        cd mvg
        mkdir build && cd build && cmake ..
        make -j4 && sudo make install

5. ``TinyTag`` *(Optional)* 

    .. code-block:: bash

        git clone https://bitbucket.org/nicolasmartin3d/tinytag
        cd tinytag
        mkdir build && cd build && cmake ..
        make -j4 && sudo make install


6. ``CodedLight`` 

    You are now ready to build and install CodedLight.

    .. code-block:: bash

        git clone https://bitbucket.org/nicolasmartin3d/codedlight
        cd codedlight

    or

    .. code-block:: bash

        wget -O codedlight-2.0.tar.gz 
            https://bitbucket.org/nicolasmartin3d/codedlight/get/2.0.tar.gz
        tar zxf codedlight-2.0.tar.gz -C codedlight --strip-components 1
        cd codedlight

    and

    .. code-block:: bash

        mkdir build && cd build && cmake ..
        make -j4 && sudo make install

.. _section-install-macosx:

Building on Mac OS X with MacPorts
==================================

.. TODO::

    Document this section.

.. _section-install-windows:

Building on Windows with Visual Studio
======================================

Building and installing libraries under Windows is not as easy as it is under Linux or Mac OS X.

This is in part due to the lack of a standardised way to install libraries, and the difficulty to
use command line tools.


  .. NOTE ::
      In the following instructions, when configuring the build environment using CMake,
      you may have to specify the version of Visual Studio as well as the toolset used.

      You may do so using the flag ``-G`` and ``-T`` accordingly.
      For example, to target "Visual Studio 2012" for a 64 bit architecture 
      with the "v110_xp" toolset, you would
      add the following flags to the command line or when prompted by CMake's GUI :
      ``-G "Visual Studio 2012 Win64" -T "v110_xp"``.

      If you target a specific version of Visual Studio and/or a specific toolset, you will
      have to add the corresponding flags to the list of arguments of ``cmake`` for **each**
      dependency listed here.

  .. NOTE ::
      In the following instructions, it is assumed that all dependencies and libraries
      will be installed or extracted in the environment variable ``INSTALL``
      (which may point to, say ``C:\CodedLight``, or any other path for which you have the rights
      to write to).

      However, by default, ``cmake`` will try to install libraries
      to ``C:\Program Files`` which may require administrator privileges. In this case, you
      may need to launch the DOS command line as an administrator for the following instructions
      to work.

  .. NOTE ::
      It is assumed that the ``PATH`` environment variable points to the
      installation path of ``cmake.exe`` and ``git.exe``, so that you can simply type ``cmake``
      or ``git`` at the prompt of the DOS command line. If this is not the case, 
      adjust your environment variable settings by following the instructions for your
      version of Windows.
 

1. ``git`` and ``cmake``

   Install `msysgit <http://msysgit.github.io>`_ and `cmake for Windows <http://www.cmake.org/download>`_ .


2. ``OpenCV``

    ``OpenCV`` can be installed with pre-built libraries under Windows or from source.
    As it was the case for the instructions to build under :ref:`section-install-linux`, if ``CodedLight``
    will be used to project patterns using an auxialiary screen, ``Qt`` must be used as a backend
    to ``highui``'s module.

    **If CodedLight will be used for video projection (requires Qt)**

    First, download and install `Qt <http://qt-project.org/downloads>`_ version 5 for your version of Windows.

    Then, download (or clone) and extract the chosen version of OpenCV (see :ref:`section-install-linux`).

    Now you need to compile OpenCV from source. The easy way is to use the DOS
    command line utility but you can also use the CMake GUI to reproduce the
    following steps. 
    

    .. code-block:: bat

        REM Go into your OpenCV source folder
        cd wherever_openCV_was_uncompressed

        REM Make an out-of-tree build folder
        md build
        cd build

        REM Configure OpenCV for Visual Studio with CMake to install INSTALL
        cmake -DCMAKE_PREFIX_PATH=wherever_you_installed_Qt ^
              -DWITH_QT=ON -DWITH_OPENGL=ON -DBUILD_EXAMPLES=OFF ^
              -DBUILD_DOCS=OFF -DBUILD_TESTS=OFF -DBUILD_PERF_TESTS=OFF ^
              -DBUILD_PRECOMPILED_HEADERS=OFF ^
              -DCMAKE_INSTALL_PREFIX=%INSTALL% ..
        
        REM Compile using Visual Studio compiler
        cmake --build . --target ALL_BUILD --config Release

        REM Install using Visual Studio compiler (may require privileges)
        cmake --build . --target INSTALL --config Release

    **If CodedLight will not be used for video projection**

    You can directly use the installer with these `instructions
    <http://docs.opencv.org/trunk/doc/tutorials/introduction/windows_install/windows_install.html#windows-install-prebuild>`__ .

3. ``Ceres``
    
    ``Ceres`` is tricky to get to compile under Windows, especially when using ``SuiteSparse`` which
    is required for good performance when using camera-projector calibration classes.
    Additional help may be found 
    `here <http://ceres-solver.org/building.html#building-on-windows-with-visual-studio>`__.

    First, you will need to install a version of ``SuiteSparse`` by following
    these `instructions <https://github.com/jlblancoc/suitesparse-metis-for-windows>`__ summed up here :

    .. code-block:: bat

        git clone https://github.com/jlblancoc/suitesparse-metis-for-windows.git
        cd suitesparse-metis-for-windows
        cmake -DBUILD_METIS=OFF -DLIB_POSTFIX="" ^
              -DSUITESPARSE_INSTALL_PREFIX=%INSTALL% ..
        cmake --build . --target ALL_BUILD --config Release
        cmake --build . --target INSTALL --config Release

    You also need to install `Eigen <http://eigen.tuxfamily.org/index.php>`_.
    Simply download the source archive and copy the content of the ``Eigen`` folder
    in ``%INSTALL%\include\eigen3``.

    Finally, you can compile ``Ceres`` from source. First, download and extract 
    `Ceres <http://ceres-solver.org/ceres-solver-1.9.0.tar.gz>`__.

    .. code-block:: bat

        cd wherever_you_extracted_Ceres
        md build
        cd build

        REM Indicates where Eigen and SuiteSparse can be found
        REM Ceres does not advise to use miniglog, but compiling and using 
        REM libglog under Windows is near impossible :(
        cmake -DSUITESPARSE_INCLUDE_DIR_HINTS=%INSTALL%/include/suitesparse ^
              -DSUITESPARSE_LIBRARY_DIR_HINTS=%INSTALL%/lib ^
              -DLAPACK_lapack_LIBRARY=%INSTALL%/lib/lapack_blas_windows/liblapack.lib ^
              -DBLAS_blas_LIBRARY=%INSTALL%/lib/lapack_blas_windows/libblas.lib ^
              -DEIGEN_INCLUDE_DIR=%INSTALL%/include/eigen3 ^
              -DMINIGLOG=ON -DSUITESPARSE=ON -DLAPACK=ON ^
              -DBUILD_DOCS=OFF -DBUILD_EXAMPLES=OFF -DBUILD_TESTING=OFF ^
              -DBUILD_SHARED_LIBS=ON ^
              -DCMAKE_INSTALL_PREFIX=%INSTALL% ..

        REM Compile and install
        cmake --build . --target ALL_BUILD --config Release
        cmake --build . --target INSTALL --config Release

4. ``xTCLAP`` *(Optional)* 

    .. code-block:: bat

        git clone https://bitbucket.org/nicolasmartin3d/xtclap
        cd xtclap
        md build
        cd build
        cmake -DCMAKE_INSTALL_PREFIX=%INSTALL% ..
        cmake --build . --target ALL_BUILD --config Release
        cmake --build . --target INSTALL --config Release

5. ``MVG`` 

    First, you will need to install a version of `OpenThreads <http://trac.openscenegraph.org/projects/osg//wiki/Support/KnowledgeBase/OpenThreads>`__ .

    .. code-block:: bat

        git svn clone
            https://svn.openscenegraph.org/osg/OpenThreads/tags/OpenThreads-2.6.0 
        cd OpenThreads-2.6.0
        md build
        cd build
        cmake -DCMAKE_INSTALL_PREFIX=%INSTALL% ..
        cmake --build . --target ALL_BUILD --config Release
        cmake --build . --target INSTALL --config Release

    Now, you can build ``MVG`` from source.  First, download and extract 
    `MVG <https://bitbucket.org/nicolasmartin3d/mvg/get/1.0.tar.gz>`__ .

    .. code-block:: bat

        cd wherever_you_extracted_mvg
        md build
        cd build

        cmake -DOpenCV_DIR=%INSTALL% ^
              -DOPENTHREADS_DIR=%INSTALL% ^
              -DCeres_DIR=%INSTALL%/CMake ^
              -DXTCLAP_INCLUDE_DIR=%INSTALL%/include ^
              -DCMAKE_INSTALL_PREFIX=%INSTALL% ..
        cmake --build . --target ALL_BUILD --config Release
        cmake --build . --target INSTALL --config Release

5. ``TinyTag`` *(Optional)* 

    .. code-block:: bat

        git clone https://bitbucket.org/nicolasmartin3d/tinytag
        cd tinytag
        md build
        cd build
        cmake -DMVG_DIR=%INSTALL%/CMake ^
              -DXTCLAP_INCLUDE_DIR=%INSTALL%/include ^
              -DCMAKE_INSTALL_PREFIX=%INSTALL% ..
        cmake --build . --target ALL_BUILD --config Release
        cmake --build . --target INSTALL --config Release


6. ``CodedLight`` 

    You are now ready to build and install CodedLight.

    clone the git repository :

    .. code-block:: bat

        git clone https://bitbucket.org/nicolasmartin3d/codedlight
        cd codedlight

    or download
    `CodedLight <https://bitbucket.org/nicolasmartin3d/codedlight/get/2.0.tar.gz>`_,
    extract it somewhere and :

    .. code-block:: bat

        cd wherever_you_extracted_CodedLight

    Finally, to build and install CodedLight :

    .. code-block:: bat

        md build
        cd build

        cmake -DMVG_DIR=%INSTALL%/CMake ^
              -DTinyTag_DIR=%INSTALL%/CMake ^
              -DXTCLAP_INCLUDE_DIR=%INSTALL%/include ^
              -DCMAKE_INSTALL_PREFIX=%INSTALL% ..
        cmake --build . --target ALL_BUILD --config Release
        cmake --build . --target INSTALL --config Release

Using CodedLight with CMake
===========================

Once the library is installed with ``make install``, it is possible to
use ``CMake`` with `FIND_PACKAGE()
<http://www.cmake.org/cmake/help/v2.8.10/cmake.html#command:find_package>`_
in order to link your code with ``CodedLight``. 

A minimal ``CMakeLists.txt`` to compile your file ``example.cpp`` that 
uses ``CodedLight`` can be :

.. code-block:: cmake

   CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

   PROJECT(example)

   FIND_PACKAGE(CodedLight REQUIRED)
   INCLUDE_DIRECTORIES(${CODEDLIGHT_INCLUDE_DIRS})

   # helloworld
   ADD_EXECUTABLE(example example.cc)
   TARGET_LINK_LIBRARIES(example ${CODEDLIGHT_LIBRARIES})

You can then generate a platform dependent set of rules using ``cmake``.
If you installed ``CodedLight`` in a non-standard location by specifying
a custom path with ``-D=CMAKE_INSTALL_PREFIX=<your custom installation path>``, then you might need to
indicate this path when calling ``FIND_PACKAGE`` :

.. code-block:: cmake

   FIND_PACKAGE(CodedLight REQUIRED PATHS "<your custom installation path>/CMake")


You may also require a specific version of ``CodedLight`` :

.. code-block:: cmake

   # Required v 2.0 or later
   FIND_PACKAGE(CodedLight 2.0 REQUIRED)
