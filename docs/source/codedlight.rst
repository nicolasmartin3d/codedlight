.. default-domain:: cpp

.. _chapter-codedlight:

===================
Coded light methods
===================

Here we document the parameters that control how coded light generators and
matchers work.

Gray code
=========

.. TODO::

    Document this section

Phase shifting
==============

The phase shifting method was popularized by [Wust] and encodes
a projector pixel position as the value of sinusoidal function shifted three
times. Recently, [Zhang] used a (three) phase shifting modification of this
method.
In details, the intensity values of a (8 bit) projector pixel are :

.. math:: :label: ps

    I^p_k(x) = 127.5 + 127.5 \cos(\phi(x) - k \delta)

for :math:`k \in \{0,2\}`, where :math:`\phi(x)=2 \pi x f`, :math:`f` is the **frequency** of the sinusoidal
signal, :math:`\delta` is the **phase shift** and :math:`\delta=\dfrac{2\pi}{3}`.

Based on the captured images of those three projected patterns, one can compute
the phase back :

.. math:: :label: ps3

    \phi(x') = \mathrm{atan}\left(\frac{\sqrt{3} (I^c_1(x') - I^c_2(x'))}{2 I^c_0(x') - I^c_1(x') - I^c_2(x')}\right)

The ``PhaseShiftGenerator`` class implemented in ``CodedLight`` extends this principle
to use multiple shifts and multiple frequencies.

In this case, the projector patterns are generated in the same way but with
:math:`k \in \{0,s\}` where :math:`s` is the number of shifts, :math:`\delta = \dfrac{2\pi}{s}` and :math:`f` varies
for each chosen frequency.

The phase is now computed using [Chen] :

.. math:: :label: psn

    \phi(x') = \mathrm{atan}\left(\frac{\sum_{k=0}^s \sin(\dfrac{2 k \pi}{s}) I^c_k(x'))}{\sum_{k=0}^s \cos(\dfrac{2 k \pi}{s}) I^c_k(x'))}\right)

Not that :eq:`psn` reduces to :eq:`ps3` when :math:`s=3`, and thus the algorithm
of [Zhang] is just a special case of the method implemented in the library.

One of the main issue with this method is that we can only compute :math:`\phi(x)`
which is the wrapped phase (ie :math:`0 \le \phi(x) \le 2 \pi`). There are
several ways to **unwrap** the phase [Judge]. Spatial approachs which use some
sort of region growing algorithms are not (yet) implemented in the library.
Instead, two temporal approachs are implemented in the library.

The first one uses an external match map which has unwrapped (unambiguous) phases 
but poor precision. This map can be computed with any other coded light method
like **Gray codes**. It can also be computed with a very low frequency phase
shifting algorithm, even though it is not a really good idea. In this case, the
final phase computed is :

.. math::

    x =  \left( \left\lfloor \frac{M(x')}{f^{-1}} \right\rfloor + \frac{\phi(x')}{2\pi} \right) f^{-1}

where :math:`M(x')` is the absolute correspondence of :math:`x'` given by the
external map. Note that the performance of this algorithm are greatly dependant on
the level of noise, as this kind of unwrapping is not very robust.

The other uses several wrapped phases at different frequencies to compute a final 
unwrapped phase that is a precise as the phase computed with the highest frequency.
With this method, the final phase is computed as [Huntley] :

.. math::
    :nowrap:

    \begin{eqnarray}
    \Phi_{n-1}(x') &=& \phi_{n-1}(x') \\
    \Phi_{t}(x') &=& \phi_{t}(x') - 2 \pi \, \mathrm{round} \left( \frac{\phi_{t}(x') - \frac{f_t}{f_{t+1}} \Phi_{t+1}(x')}{2 \pi} \right) \\
    x' &=& \frac{\Phi_0(x')}{2\pi} f_0^{-1}
    \end{eqnarray}

where :math:`f_t` is the :math:`\mathsf{t}^{\mathsf{th}}` frequency with :math:`f_0`
being the highest and :math:`f_{n-1}` the lowest, :math:`\phi_{t}(x)` the
wrapped phase computed from the :math:`\mathsf{t}^{\mathsf{th}}` sine
wave alone using :eq:`psn` and :math:`\Phi_{t}(x)` the absolute phase after :math:`t`
cumulative unwraps. This method is more robust, since the error can be bounded
by the period difference between two consecutive frequencies, but it requires
more patterns.

Generation
----------

.. function:: Direction PhaseShiftGenerator::direction() const
.. function:: void PhaseShiftGenerator::setDirection(const Direction &dir)

    This parameter defines the pattern **direction**, which can be one of :

    +   ``Generator::Horizontal``
    +   ``Generator::Vertical``
    +   ``Generator::Both``

    and which controls the total number of pattern that will be projected.
    If you are in a calibrated setup, you don't need to project both directions,
    only the sequence of *vertical* or *horizontal* patterns are needed (the
    direction which is orthogonal to the **baseline** of your setup).

.. function:: cv::Size PhaseShiftGenerator::size() const
.. function:: void PhaseShiftGenerator::setSize(cv::Size size)

    This defines the size of the generated pattern and should be the exact same
    size as your projector's resolution.

.. function:: const std::vector<double> & PhaseShiftGenerator::periods() const
.. function:: void PhaseShiftGenerator::setPeriods(const std::vector<double> &periods)

    This option defines how many sine waves will be projected and the frequency
    of each one of them. In :eq:`ps`, the period of one sine wave is :math:`f^{-1}`
    and is the size in pixels that a full sine period spans.
    Thus passing a vector of two doubles :math:`\{8, 16\}` will produce a series
    of shifts of two sine waves of frequencies :math:`\frac{1}{8}` and
    :math:`\frac{1}{16}`.

.. function:: const std::vector<int> & PhaseShiftGenerator::numShifts() const
.. function:: void PhaseShiftGenerator::setNumShifts(const std::vector<int> &numShifts)

    This option defines the number of shifts that will generated for each sine wave.
    The vector used must be of same size than the one defining the periods.

.. function:: const Generation::Method & PhaseShiftGenerator::generationMethod() const
.. function:: void PhaseShiftGenerator::setGenerationMethod(const Generation::Method &mode)

    This option controls how the sine wave are generated and optionally modulated.
    By default, the phase shifting method does not require any modulation. However,
    following the works of [Chen][Martin], modulation was proven to provide 
    added robustness in some cases.

    The type can be of an instance of :

    +   ``PhaseShiftGenerator::Generation::Default`` : no modulation
    +   ``PhaseShiftGenerator::Generation::BlockModulation`` : box squared modulation
    +   ``PhaseShiftGenerator::Generation::SinusoidalModulation`` : sinusoidal modulation

    .. TODO::

        Document this option

Matching
--------

.. function:: const PhaseShiftGenerator & PhaseShiftMatcher::generator() const
.. function:: void PhaseShiftMatcher::setGenerator(const PhaseShiftGenerator &)

    The phase shifting matcher needs to know about the number of shifts and
    frequencies that were used while generating the patterns. The exact
    same generator as the one used during the creation of the projector patterns
    must be passed.

.. function:: const Unwrapping::Method & PhaseShiftMatcher::unwrappingMethod() const
.. function:: void PhaseShiftMatcher::setUnwrappingMethod(const Unwrapping::Method &type)

    This option controls how the unwrapping is done and can be one of :

    +   ``PhaseShiftMatcher::Unwrapping::AllFrequencies`` : uses the multiple frequencies unwrapping algorithm
    +   ``PhaseShiftMatcher::Unwrapping::ExternalMap`` : uses an external map to unwrap the phases computed using the highest frequency

Bibliography
============

.. [Wust] Wust, C and Capson, D,
   **Surface profile measurement using color fringe projection**,
   *Machine Vision and Applications*, 1991.

.. [Huntley] Huntley, JM and Saldner, HO,
    **Error-reduction methods for shape measurement by temporal phase unwrapping**,
    *Journal of the Optical Society of America A*, 1997.

.. [Chen] Chen, T and Seidel, HP and Lensch, H,
    **Modulated phase-shifting for 3D scanning**,
    *Computer Vision and Pattern Recognition*, 2008.

.. [Liu] Liu, Kai and Wang, Yongchang and Lau, Daniel L and Hao, Qi and Hassebrook, Laurence G.
    **Gamma model and its analysis for phase measuring profilometry**,
    *Journal of the Optical Society of America A*, 2010.

.. [Martin] Martin, N,
    **Reconstruction active par projection de lumière non structurée**,
    *Université de Montréal*, 2014.

.. [Zhang] Zhang, S and Yau, S,
    **High-speed three-dimensional shape measurement system using a modified two-plus-one phase-shifting algorithm**,
    *Optical Engineering*, 2007

.. [Judge] Judge T.R. and Bryanston-Cross P. J.,
    **A review of phase unwrapping techniques in fringe analysis**,
    *Optics and Lasers in Engineering*, 1994
    

