.. include:: macro.rst

.. _chapter-tutorial:

========
Tutorial
========

Here we present the general classes that should be used to generate and match
patterns, as well as calibrate a projector-camera setup.

In this tutorial, we show how to use the library to reconstruct an object using
the **phase shifting** structured light methods. Applications that come with
``CodedLight`` will be used to ease the process. For the purpose of this tutorial,
we will be using the images available in the ``datas`` folder.  You can repeat
this set of instructions with your own captured images. 

.. _section-tut-create-gen-config:

Creating a generator configuration file
=======================================

The application ``cl3ds_create_generator_config`` knows about all the generators 
provided in ``CodedLight`` and can be used to generate XML configuration file
by querying the user about the parameters. We use the ``-l`` flag to list
all the available generators :

.. code-block:: text

    > cl3ds_create_generator_config -l
    Available generator methods are : 
        combiner
        dummy
        canvas
        gamma
        graycode
        phaseshift
        seqbuffer
        seqreader

When used interactively, this application can configure a generator by asking
the user questions and then serializing it to a XML file. Here we use the ``-m``
flag to provide the name of the generator we would like to create :

.. code-block:: text

    > cl3ds_create_generator_config -m phaseshift 
    Configuring Phase Shift generator parameters
    Type : size
    Range : [ 0x0 , 10000x10000 ] 
    Description : Size of the pattern to generate
    Default value : 800x600
    > 912 1140
    Type : int
    Valid values : [ 1, 2, 3 ] 
    Description : Direction for the pattern
    Default value : 1
    > 3
    Type : vector of ints
    Range : [ 3 , 2147483647 ] 
    Description : Number of shifts each sine wave is shifted
    Default value : [ 10, 3, 3, 3, 3, 3, 3, 3 ]
    > 5 21 3 3 3 3
    Type : vector of doubles
    Description : Periods of each sine wave
    Default value : [ 16, 32, 64, 128, 256, 512, 1024, 2048 ]
    > 5 16 48 144 432 1296
    Which Phase Shift generation type should be used ?
    Available identifiers are : 
        block
        default
        sinusoidal
    Which object should be created ?
    > default

.. CAUTION::

    When asked for a vector of values, the correct way to answer is
    to first enter the length of the vector, as the program has no (easy) way
    to guess it. Then you can enter the values separated by spaces.

would generate a file called ``phaseshift_generator.xml`` whose content is :

.. code-block:: xml

    <?xml version="1.0"?>
    <opencv_storage>
    <generator>
      <name>phaseshift</name>
      <param>
        <!--
        Type : size
        Range : [ 0x0 , 10000x10000 ] 
        Description : Size of the pattern to generate
        Default value : 800x600
        -->
        <size>
          912 1140</size>
        <!--
        Type : int
        Valid values : [ 1, 2, 3 ] 
        Description : Direction for the pattern
        Default value : 1
        -->
        <direction>3</direction>
        <!--
        Type : vector of ints
        Range : [ 3 , 2147483647 ] 
        Description : Number of shifts each sine wave is shifted
        Default value : [ 10, 3, 3, 3, 3, 3, 3, 3 ]
        -->
        <numShifts>
          21 3 3 3 3</numShifts>
        <!--
        Type : vector of doubles
        Description : Periods of each sine wave
        Default value : [ 16, 32, 64, 128, 256, 512, 1024, 2048 ]
        -->
        <periods>
          16. 48. 144. 432. 1296.</periods>
        <generationMethod>
          <name>default</name>
          <param>
            </param></generationMethod></param></generator>
    </opencv_storage>

One can also use the flag ``-d`` to automatically use the default values for the
parameters of the generators, and ``-o`` to specify the name of the XML
configuration file that will be created :

.. code-block:: bash

   cl3ds_create_generator_config -d -m phaseshift \
                                 -o phaseshift_default_generator.xml

Will generate (without querying the user) a file called
``phaseshift_default_genenerator.xml``
containing the parameters of a ``PhaseShiftGenerator`` with default values.

.. TIP::

    A complete list of all flags that a program accepts can be obtained by
    using the help flag ``-h`` : ``cl3ds_create_generator_config -h``. This flag
    is accepted by every applications that ship with ``CodedLight``.

The XML configuration file for a generator can then be used by many 
applications like ``cl3ds_generate`` or ``cl3ds_match``.

.. _section-tut-check-patterns:

Checking and saving the patterns generated
==========================================

The command :

.. code-block:: bash

    cl3ds_generate -g datas/scan/phaseshift_generator.xml

will open a window to *slideshow* every pattern in the sequence generated by
the generator stored in ``datas/scan/phaseshift_generator.xml`` (that is provided
in the datas, but is the same than the one we created in the previous step).

.. table:: 
    :class: borderless 

    +--------------------------------------------------------+--------------------------------------------------------+--------------------------------------------------------+
    | .. thumbnail:: /images/phaseshift_0024.png             | .. thumbnail:: /images/phaseshift_0027.png             | .. thumbnail:: /images/phaseshift_0030.png             |
    |    :width: 95%                                         |    :width: 95%                                         |    :width: 95%                                         |
    |    :align: center                                      |    :align: center                                      |    :align: center                                      |
    |    :group: generation                                  |    :group: generation                                  |    :group: generation                                  |
    |    :title: One of the phase shifting pattern generated |    :title: One of the phase shifting pattern generated |    :title: One of the phase shifting pattern generated |
    +--------------------------------------------------------+--------------------------------------------------------+--------------------------------------------------------+
    | .. thumbnail:: /images/phaseshift_0057.png             | .. thumbnail:: /images/phaseshift_0060.png             | .. thumbnail:: /images/phaseshift_0063.png             |
    |    :width: 95%                                         |    :width: 95%                                         |    :width: 95%                                         |
    |    :align: center                                      |    :align: center                                      |    :align: center                                      |
    |    :group: generation                                  |    :group: generation                                  |    :group: generation                                  |
    |    :title: One of the phase shifting pattern generated |    :title: One of the phase shifting pattern generated |    :title: One of the phase shifting pattern generated |
    +--------------------------------------------------------+--------------------------------------------------------+--------------------------------------------------------+

You can experiment with the parameters of a generator by using the interactive
application ``cl3ds_create_generator_config`` or by manually editing the XML file
that it produced.

Once you've done configuring the generator, you can use ``cl3ds_generate`` to visually
analyse the effects of the parameters. 

For example,  changing the line  :

.. code-block:: xml

        <direction>3</direction>

to 

.. code-block:: xml

        <direction>1</direction>

in ``phaseshift_generator.xml`` will produce sinusoidal patterns oriented only in the
horizontal direction (in contrast to both horizontal and vertical).

The flag ``-o`` can be used to specify a path to save the generated
patterns on the disk. If omitted, the patterns are only shown in the window
that pops-up when executing ``cl3ds_generate``.

The argument that must be passed to ``-o`` is a string pattern, which must
contains a ``%d`` format that will be filled by the current index of the
saved pattern. For example, the command :

.. code-block:: bash

    cl3ds_generate -g datas/scan/phaseshift_generator.xml -o phaseshift_%03d.png

will save the *66* generated patterns in the files :

.. code-block:: text

    phaseshift_000.png
    phaseshift_001.png
    ...
    phaseshift_065.png

.. NOTE::

    The flag ``-o`` is used by many applications in ``CodedLight`` to indicate
    either the file to which the result will be stored, or the string pattern
    that must include one or more formatter. Refer to the help message of the
    program by specifying the flag ``-h``.

By default the slideshow displays two patterns per second. This behavior can
be changed by using the ``-f`` flag with a speed in frames per seconds between
*1* and *1000*. 

.. TIP::

    If one is only concerned with saving the generated patterns to the disk,
    ``cl3ds_generate`` can be called with ``-f -1`` which will generate and save the
    patterns to the disk as fast as possible without displaying them on the screen.

.. _section-tut-project-patterns:

Projecting the patterns on the scene
====================================

.. TODO::

    Document this section.

.. _section-tut-match-patterns:

Matching the sequence of patterns
=================================

Once the patterns have been projected on the scene and captured by a camera
looking at it, one can use the corresponding coded light matcher to compute
a correspondence map.

As is the case with a generator, a matcher has a lot of parameters that
control the way the correspondence are computed.
A matcher also needs to be told where to look for the captured and projected
patterns. This information is passed through two generators (one for the
camera and one for the projector) which *generates* the patterns when
the matcher asks for them. Of course, in the case of a generator that reads
images from the disk, the patterns are never really generated as much as 
read.

Most of the time, the parameters of a matcher includes the parameters
of the generator that produce the patterns that were projected. It is not
always the case however.

In the case of the phase shifting matcher, the parameters of the phase shifting
generator are required, as well as information that parameterize the *phase
unwrapping process*. Thus, for this matcher, only the generator responsible
for the captured patterns is required since the projected patterns are 
implicitly specified by the parameters of the generator. 

.. CAUTION::
   It is important to differentiate the parameters of the matcher, and the
   parameters of the generator that produces the camera and projector patterns.
   While the camera and projector generator are most of the time (but not
   necessarily) instances of ``SequenceReader``, the matcher often needs
   to know about the specific values (e.g frequencies, number of sines, etc..)
   used during the generation of the projected patterns. Thus, passing it
   only the images is not sufficient since it is very hard (and unnecessary) 
   to compute those values from the patterns.

   To summarize, the matcher always needs to be passed some kind of numeric
   values : the actual parameters matcher. It also needs a generator that provides
   the captured patterns. Sometimes it will require a generator that produces
   the projected patterns, when it is required to have the actual images
   and it is impossible or innefficient to construct them solely from the
   parameters of the generator.


Here, we use the images stored in the files ``cam_000.png``, ``cam_001.png``, 
etc, in the ``datas/scan/`` folder. The camera generator will be a
``SequenceReader``, while the projector generator will be a ``DummyGenerator``
(which as its name indicate, does not generate anything).

We can create the configuration for this matcher with the application ``cl3ds_create_matcher_config`` :

.. code-block:: text

    > cl3ds_create_matcher_config -m phaseshift -c seqreader -p dummy 
    Configuring Phase Shift matcher parameters.
    Are the parameters of the object embedded in an existing XML file ? (y/n)
    > y
    What is the path to the XML configuration for this object ?
    > phaseshift_generator.xml
    What is the name of the node which embedds this object parameters in the XML file ?
    > generator
    Which Phase Shift unwrapping type should be used ?
    Available identifiers are : 
        allFrequencies
        externalMap
    Which object should be created ? 
    > allFrequencies
    Configuring Sequence Reader generator parameters.
    Type : string
    Description : String format used to determine the path of images to read
    Default value : ./img_%04d.png
    > datas/scan/cam_00_%04d.png
    Type : int
    Description : Number of patterns to read
    Default value : 0
    > 66
    Type : int
    Description : Offset from which to start in the format
    Default value : 0
    > 0
    Type : int
    Description : Steps between name of two images in the format
    Default value : 1
    >1

.. CAUTION::

    In our example, the path used for the ``SequenceReader`` is relative.
    Thus, you have to make sure to execute whatever application that's using
    this generator in the correct path. You can also specify an absolute
    path and not have to worry about the directory you execute the application
    from.

Here we used another way of defining the parameters of an object. Rather
than embedding them directly in the configuration file and being asked
about all of them interactively, we simply told the application to go and
look for them in an external file : ``phaseshift_generator.xml``. This
is the file we previously created in :ref:`section-tut-create-gen-config`.

We also specified the node that contains the parameters of the generator :
``generator``. This is because there can be several nodes in an XML file
and the program has no way to select the right one.

.. HINT::

    Linking to a file that embeds the parameters of one object is handy because
    it saves us from repeating the parameters and making a mistake in one of them.
    It also enables us to change the parameters in one file and see the changes
    reflected in all files that links to it, thus keeping a matcher in sync with 
    its associated generator.

.. TIP::

    It is of course possible to pass those arguments on the command line
    through a pipe : 
    ``echo y phaseshift_generator.xml generator allFrequencies datas/scan/cam_00_%04d.png 66 0 1 |``
    ``cl3ds_create_matcher_config -m phaseshift -c seqreader -p dummy``.
    This is one way to script and automate the creation of configuration files.
    In the future, it might even be possible to pass the values directly on the
    command line by naming them...

    As explained above, another way to create this file is to first, create a 
    default value generator with the ``-d`` flag, and to manually edit it
    with those values.
    
Now we are ready to compute the correspondence map between the camera and the
projector with ``cl3ds_match`` :

.. code-block:: bash

    cl3ds_match -m datas/scan/phaseshift_matcher.xml -k "cam match" \
                -o cam_match.png

This command should create a file named ``cam_match.png`` which corresponds to
the matrix associated to the key *cam match* in the :cpp:class:`MatTable`
returned by :cpp:func:`Matcher::match` (see also
:ref:`subsection-ref-matching`). 

.. NOTE::

    This image is one of the most used named matrix in :cpp:class:`MatTable`
    and represents the camera-projector correspondence map. It can be
    directly accessed with :cpp:func:`MatTable::camMatchMap`.

.. thumbnail:: /images/cam_match.png
   :width: 50 %              
   :align: center           
   :group: matching        
   :title: The camera-projector correspondence map saved in ``cam_match.png``

|vspace| |br|
This correspondence map is one of the information we need to construct a 3D mesh
of the object we scanned. The other information is the geometric calibration
of the camera-projector system.
    
.. _section-tut-calib:

Calibrating the camera-projector system
=======================================

To transform a correspondence map in a 3D point cloud, the camera-projector system
needs to be accurately calibrated. Several methods are available to perform
this calibration (see :ref:`section-ref-calib`). ``CodedLight`` also provides
methods to perform this step. Here we explain how to use a calibration method
that makes use of a board with a printed pattern on it which is moved by a
translation stage at different steps. If you already have the geometric calibration
of both the camera and projector, you can skip to next section.

For this tutorial, we will be using the images available in the ``datas/calib/``
folder. You can use your own images (captured whatever process you control) 
with the calibration application. You can also let the application capture
the images during the calibration (referred to as the **online** mode).

.. TODO::

    Document the online mode of ``bench_calibration``.

For this step to work, you will need to print a pattern of fiducial markers
that should be placed on a board that will be moved by the translation stage.
The fiducial markers detection is done internally by the ``TinyTag`` library
(see :ref:`section-install-software`). 

There are several reasons for using a board of fiducial markers instead of a
checkerboard (as is often the case in camera calibration) :

+   it does not suffer from an orientation ambiguity 
+   if some markers are not detected, the calibration can still continue
+   it works well even when the board is far away from the camera
+   it can be more precise, especially when the contrast of the image is poor

.. thumbnail:: /images/tags.png
   :width: 50 %              
   :align: center           
   :group: calibrating        
   :title: The fiducial markers pattern

|vspace| |br|
The board is moved at different positions on the translation stage.
For each position, the markers are detected and a sequence of phase shifting
patterns are projected by the projected on the board, providing correspondences
between the position of the markers in the camera and in the projector.
The set of all markers correspondences is used to compute the calibration
parameters of the camera and the projector.

.. table:: 
    :class: borderless 

    +-------------------------------------------------------+-------------------------------------------------------+-------------------------------------------------------+
    | .. thumbnail:: /images/tags_000_00.png                | .. thumbnail:: /images/tags_007_00.png                | .. thumbnail:: /images/tags_014_00.png                |
    |    :width: 95%                                        |    :width: 95%                                        |    :width: 95%                                        |
    |    :align: center                                     |    :align: center                                     |    :align: center                                     |
    |    :group: calibrating                                |    :group: calibrating                                |    :group: calibrating                                |
    |    :title:                                            |    :title:                                            |    :title:                                            |
    |                                                       |                                                       |                                                       |
    |    The board captured at **Z** = 280mm                |    The board captured at **Z** = 300mm                |    The board captured at **Z** = 320mm                |
    +-------------------------------------------------------+-------------------------------------------------------+-------------------------------------------------------+

To invoke the translation stage calibration, you can use the following command :

.. code-block:: bash

    cl3ds_bench_calibration -g datas/calib/phaseshift_generator.xml \
                            -m datas/calib/phaseshift_matcher.xml \
                            -i datas/calib/tags.png -d 4.37 \
                            --camera-imagesTag-path datas/calib/tags_%03d_%02d.png \
                            --camera-imagesSTL-path datas/calib/cam_%03d_%02d_%04d.png \
                            --num-cameras 1 --zmin 280 --zmax 320 --nbsteps 15 \
                            -o geom_params_%02d.xml

Several options are provided here :

+   the path to the projector pattern generator and matcher (in this case a ``PhaseShiftMatcher`` as described in the ``datas/calib`` folder)
+   the path to the image of the fiducial markers pattern that was printed and attached to the board
+   the real dimension of a fiducial marker (in mm)
+   the path to the captured (negative) images of the board with (only) the fiducial markers, with two ``%d`` format (for each step, each camera)
+   the path to the captured images of the board with the projected patterns onto it, with three ``%d`` format (for each step, each camera, each pattern)
+   the number of cameras : ``N=1`` in this example
+   the parameters of the translation : minimum and maximum position of the board (as a single **Z** direction), as well as the number of steps taken
+   the path to the geometric calibration parameters that will be computed, with a single ``%d`` format (0 for the first camera, 1 for the second, etc.. and ``N`` for the projector)

The application should save two XML files, containing the following camera geometric calibration parameters :

.. code-block:: xml

    <?xml version="1.0"?>
    <opencv_storage>
    <K type_id="opencv-matrix">
      <rows>3</rows>
      <cols>3</cols>
      <dt>d</dt>
      <data>
        2.4804220418002842e+03 0. 5.8316399456865236e+02 0.
        2.4918073167908956e+03 5.1080404893847054e+02 0. 0. 1.</data></K>
    <R type_id="opencv-matrix">
      <rows>3</rows>
      <cols>3</cols>
      <dt>d</dt>
      <data>
        -9.9673214424881784e-01 6.3174714966857089e-03
        8.0530256270809675e-02 -8.5807899972832925e-03
        -9.9957694847769329e-01 -2.7790180190948695e-02
        8.0320624152058484e-02 -2.8390379108272541e-02
        9.9636468409408829e-01</data></R>
    <t type_id="opencv-matrix">
      <rows>3</rows>
      <cols>1</cols>
      <dt>d</dt>
      <data>
        2.1748221885920056e+01 4.6682362718107193e+01
        -4.8325950443053739e+02</data></t>
    <M type_id="opencv-matrix">
      <rows>3</rows>
      <cols>4</cols>
      <dt>d</dt>
      <data>
        -2.4254762843388662e+03 -8.8625133927436828e-01
        7.8079303190938379e+02 -2.2787477408118008e+05
        1.9646424731059941e+01 -2.5052550745116364e+03
        4.3969934051981704e+02 -1.3052745856505123e+05
        8.0320624152058484e-02 -2.8390379108272541e-02
        9.9636468409408829e-01 -4.8325950443053739e+02</data></M>
    <coeffs type_id="opencv-matrix">
      <rows>4</rows>
      <cols>1</cols>
      <dt>d</dt>
      <data>
        -2.0540827502698966e-01 -4.4910035868412684e-01
        2.3914483651200518e-03 1.8861464372572721e-03</data></coeffs>
    <imageSize type_id="opencv-matrix">
      <rows>1</rows>
      <cols>2</cols>
      <dt>d</dt>
      <data>
        1292. 964.</data></imageSize>
    </opencv_storage>

and the following projector geometric calibration parameters :

.. code-block:: xml

    <?xml version="1.0"?>
    <opencv_storage>
    <K type_id="opencv-matrix">
      <rows>3</rows>
      <cols>3</cols>
      <dt>d</dt>
      <data>
        1.4918730121451599e+03 0. 4.1686837758149642e+02 0.
        3.0727236981602659e+03 1.3308090675608746e+03 0. 0. 1.</data></K>
    <R type_id="opencv-matrix">
      <rows>3</rows>
      <cols>3</cols>
      <dt>d</dt>
      <data>
        -9.9991810625142141e-01 -9.5402123846780366e-04
        1.2762077967463306e-02 4.3031141769995829e-04
        -9.9916002294253303e-01 -4.0976375944692042e-02
        1.2790450447691710e-02 -4.0967528567799796e-02
        9.9907860850905605e-01</data></R>
    <t type_id="opencv-matrix">
      <rows>3</rows>
      <cols>1</cols>
      <dt>d</dt>
      <data>
        4.1403393610904843e+01 1.0350010581119822e+02
        -4.8251827425264241e+02</data></t>
    <M type_id="opencv-matrix">
      <rows>3</rows>
      <cols>4</cols>
      <dt>d</dt>
      <data>
        -1.4864189027451264e+03 -1.8501345706265727e+01
        4.3552367830410009e+02 -1.3937800460179028e+05
        1.8343875524731779e+01 -3.1246626392434632e+03
        1.2036737899799734e+03 -3.2411246675107762e+05
        1.2790450447691710e-02 -4.0967528567799796e-02
        9.9907860850905605e-01 -4.8251827425264241e+02</data></M>
    <coeffs type_id="opencv-matrix">
      <rows>4</rows>
      <cols>1</cols>
      <dt>d</dt>
      <data>
        1.3931655149870503e-01 -1.1454583607395185e-04
        5.1797209820866212e-02 4.2049064051333793e-04</data></coeffs>
    <imageSize type_id="opencv-matrix">
      <rows>1</rows>
      <cols>2</cols>
      <dt>d</dt>
      <data>
        912. 1140.</data></imageSize>
    </opencv_storage>

These two files and the correspondence map will be used in the next and final
step to compute a 3D mesh of the object.

.. _section-tut-mesh:

Reconstructing the 3D model
===========================

In this step, we will construct a 3D modek of the object that was scanned,
by first transforming a correspondence map to a 3D point cloud, meshing
those points together as a 3D surface with optional texture.

The triangulation and meshing is performed internally by the ``MVG`` library.
To perform this step, you can use the following command :

.. code-block:: bash

    cl3ds_mesh -c datas/calib/geom_params_00.xml \
               -p datas/calib/geom_params_01.xml \
               -r datas/scan/cam_match.png -m datas/scan/mask.png \
               -t datas/scan/texture.png \
               --zmin 280 --zmax 305 -i --max-edge-length 0.5 \
               -o mesh.ply

Here again, we provided several options on the command line :

+   the path to the camera and projector geometric calibration parameters
+   the path to the computed correspondence map
+   the path to a camera mask which is a monochrome image with white, resp. black pixels for coordinates for which we would like, resp. would not like to compute the corresponding 3d point
+   the path to a captured color texture image 
+   the **Z** range we are interested in (note that the **Z** direction is aligned with the translation stage direction, and correspond to a subset of the interval we used to calibrate our system)
+   a flag to reverse the computed normals (some 3D viewer require this, others don't)
+   the maximum length of the edge in the model (in the same unit as the calibration values, i.e mm)
+   the path to the saved 3D model

This is a screenshot of the final reconstructed model :

.. thumbnail:: /images/cam_recon.png
   :width: 50 %              
   :align: center           
   :group: matching        
   :title: The final 3D model reconstructed from the sequence of captured patterns

|vspace| |br|
