# - Try to find libPvAPI
# Once done this will define
#  PvAPI_FOUND - System has libpvapi
#  PvAPI_INCLUDE_DIRS - The libpvapi include directories
#  PvAPI_LIBRARIES - The libraries needed to use PvAPI

IF (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    #clang can not link with gcc compiled PvAPI binaries ...
ELSE (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    find_path(PvAPI_INCLUDE_DIR PvApi.h
              HINTS ${PvAPI_INCLUDE_DIR_HINTS}
                    /usr/local/include
                PATH_SUFFIXES PvAPI)

    find_library(PvAPI_LIBRARY NAMES PvAPI
                 HINTS ${PvAPI_LIBRARY_DIR_HINTS}
                        /usr/local/lib)

    set(PvAPI_LIBRARIES ${PvAPI_LIBRARY})
    if (UNIX AND NOT APPLE)
        list(APPEND PvAPI_LIBRARIES z rt)
    endif()
    if (APPLE)
        list(APPEND PvAPI_LIBRARIES "-framework CoreFoundation")
    endif ()
    set(PvAPI_INCLUDE_DIRS ${PvAPI_INCLUDE_DIR})

    include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set PvAPI_FOUND to TRUE
# if all listed variables are TRUE
    find_package_handle_standard_args(PvAPI DEFAULT_MSG PvAPI_LIBRARIES PvAPI_INCLUDE_DIRS)
ENDIF (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")

mark_as_advanced(PvAPI_INCLUDE_DIRS PvAPI_LIBRARIES)
