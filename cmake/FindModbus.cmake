# - Try to find libmodbus
# Once done this will define
#  MODBUS_FOUND - System has libceres
#  MODBUS_INCLUDE_DIRS - The libceres include directories
#  MODBUS_LIBRARIES - The libraries needed to use LibXml2

find_path(MODBUS_INCLUDE_DIR modbus.h
          HINTS ${MODBUS_INCLUDE_DIR_HINTS}
				/usr/include
				/usr/local/include
          PATH_SUFFIXES modbus)

find_library(MODBUS_LIBRARY NAMES modbus
             HINTS ${MODBUS_LIBRARY_DIR_HINTS}
				   /usr/lib
				   /usr/local/lib)

set(MODBUS_LIBRARIES ${MODBUS_LIBRARY})
set(MODBUS_INCLUDE_DIRS ${MODBUS_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set MODBUS_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(Modbus DEFAULT_MSG MODBUS_LIBRARY MODBUS_INCLUDE_DIR) 
mark_as_advanced(MODBUS_INCLUDE_DIR MODBUS_LIBRARY)
