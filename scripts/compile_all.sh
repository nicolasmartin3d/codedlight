#! /bin/bash

configs[1]=linux_static_x64_noQt
prefixs[1]=/home/CodedLight_v2.1_proto2/Install/linux/static/Release/x64
opts_shared[1]=OFF
opts_extra[1]="-DCMAKE_DISABLE_FIND_PACKAGE_LCRAPI=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_PVAPI=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_Modbus=TRUE"

configs[2]=linux_static_x64_Qt
prefixs[2]=/home/CodedLight_v2.1_proto2/Install/linux_qt/static/Release/x64
opts_shared[2]=OFF
opts_extra[2]=""

configs[3]=linux_static_in_shared_x64_noQt
prefixs[3]=/home/CodedLight_v2.1_proto2/Install/linux/shared/Release/x64
opts_shared[3]=OFF
opts_extra[3]="-DCMAKE_DISABLE_FIND_PACKAGE_LCRAPI=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_PVAPI=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_Modbus=TRUE"

configs[4]=linux_shared_x64_Qt
prefixs[4]=/home/CodedLight_v2.1_proto2/Install/linux_qt/shared/Release/x64
opts_shared[4]=ON
opts_extra[4]=""

mkdir -p ../../Build/codedlight
cd ../../Build/codedlight

for i in $(seq 1 4); do 
	c=${configs[$i]}
	rm -rf $c
	mkdir -p $c
	cd $c
		
	p=${prefixs[$i]}
	cmake -DCMAKE_INSTALL_PREFIX=$p ${opts_extra[$i]} \
	 -DMVG_DIR=$p/share \
	 -DTinyTag_DIR=$p/share \
	 -DPvAPI_INCLUDE_DIR_HINTS=$p/include \
	 -DPvAPI_LIBRARY_DIR_HINTS=$p/lib \
	 -DLCRAPI_DIR=$p/share/lcrapi \
	 -DXTCLAP_INCLUDE_DIR=$p/include \
	 -DBUILD_TEST=ON -DBUILD_APPLICATIONS=ON -DBUILD_SHARED_LIBS=${opts_shared[$i]} ../../../Source/codedlight
	 
    make -j4
    make install

    cd ..
done
