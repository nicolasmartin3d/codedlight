/*
 * License Agreement for CodedLight
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <limits>
#include <opencv2/highgui/highgui.hpp>
#include <xtclap/CmdLine.h>
#include <mvg/logger.h>

#include "../patterns/generator.h"
#include "../devices/camera_device.h"
#include "../devices/projector_device.h"

using namespace cv;
using namespace std;
using namespace TCLAP;
using namespace cl3ds;

int main(int   argc,
         char *argv[])
{

    CmdLine cmd("Projects patterns on the given screen and captures projected "
                "frames with the given camera");

    ValueArg<string> configPathArg("g", "config-path",
                                   "Path to the pattern generator configuration",
                                   true, "", "string", cmd);

    MultiArg<string> cameraConfigPathsArg("C", "camera-config-path",
                                          "Path to the camera configuration",
                                          true, "string", cmd);

    ValueArg<string> projectorConfigPathArg("P", "projector-config-path",
                                            "Path to the projector configuration",
                                            true, "", "string", cmd);

    ValueArg<string> camOutputPatternArg("", "cam-output-pattern",
                                         "Path for catpured images (should include a %% formatter)\n",
                                         false, "cam_%02d_%04d.png", "string", cmd);
    ValueArg<string> projOutputPatternArg("", "proj-output-pattern",
                                          "Path for projected images (should include a %% formatter)\n",
                                          false, "proj_%04d.png", "string", cmd);

    cmd.parse(argc, argv);

    vector<string> cameraConfigPaths   = cameraConfigPathsArg.getValue();
    string         projectorConfigPath = projectorConfigPathArg.getValue();
    string         configPath          = configPathArg.getValue();
    string         camOutPattern       = camOutputPatternArg.getValue();
    string         projOutPattern      = projOutputPatternArg.getValue();

    FileStorage fs;

    // Generator setup
    if (!fs.open(configPath, FileStorage::READ))
    {
        logError() << "Config file at path " << configPath << " could not be found";

        return 1;
    }
    Ptr<Generator> generator;
    fs["generator"] >> generator;
    if (generator.empty())
    {
        logError() << "The generator can not be read";

        return 1;
    }
    Size   projSize = generator->size();
    size_t nbImages = generator->count();
    fs.release();

    // so that patterns are generated only once, and when saved are just as they were projected
    // it does use a little more memory .. but it is safer (if the generator makes no guarantee to
    // produce
    // the same pattern each time get() is called)
    // Ptr<Memoizer> projGenerator = makePtr<Memoizer>(generator);
    Ptr<Generator> projGenerator = generator;

    // Black image
    Mat blackImage(projSize, CV_8U, Scalar::all(0));

    // Projector setup
    Ptr<ProjectorDevice> projDevice;
    if (!fs.open(projectorConfigPath, FileStorage::READ))
    {
        logError() << "Unable to open projector config file :"
                   << projectorConfigPath;

        return 1;
    }
    fs["projector"] >> projDevice;
    if (projDevice.empty())
    {
        logError() << "The projector can not be read";

        return 1;
    }
    fs.release();

    // Cameras setup
    size_t                      nbCameras = cameraConfigPaths.size();
    vector< Ptr<CameraDevice> > camDevices(nbCameras);
    for (size_t i = 0; i < nbCameras; ++i)
    {
        if (!fs.open(cameraConfigPaths[i], FileStorage::READ))
        {
            logError() << "Unable to open camera config file :"
                       << cameraConfigPaths[i];

            return 1;
        }
        fs["camera"] >> camDevices[i];
        if (camDevices[i].empty())
        {
            logError() << "The camera can not be read";

            return 1;
        }
        fs.release();
    }

    string camWinName = "cam_";
    for (size_t i = 0; i < nbCameras; ++i)
    {
        ostringstream oss;
        oss << camWinName << i;
        namedWindow(oss.str() /*, CV_GUI_EXPANDED*/);

        if (!camDevices[i]->isOpened())
        {
            logError() << "Could not grab from camera" << i;

            return 1;
        }
    }

    logInfo() << "Grabbing" << nbCameras << "cameras";
    // check to see if we are in sync or async mode
    bool synchronized = (nbCameras == 1 &&
                         projDevice->setSynchronized(true));
    if (synchronized)
    {
        for (size_t i = 0; i < nbCameras; ++i)
        {
            if (!camDevices[i]->setSynchronized(true))
            {
                synchronized = false;
            }
        }
    }

    sleep_for(500); // just to be sure everyone is setup

    vector< vector<Mat> > camImages(nbCameras, vector<Mat>(nbImages));

    // capturing ambiant image
    vector<Mat> blacks(nbCameras);
    if (!projDevice->put(blackImage))
    {
        logError() << "Could not put an image in projector";

        return -1;
    }
    for (size_t cam = 0; cam < nbCameras; ++cam)
    {
        if (!camDevices[cam]->get(blacks[cam]))
        {
            logError() << "Could not get an image from camera" << cam;

            return -1;
        }
    }

    projGenerator->reset();
    if (synchronized)
    {
        if (!projDevice->put(projGenerator))
        {
            logError() << "Could not put an image in projector";

            return -1;
        }
        for (size_t i = 0; i < nbImages; ++i)
        {
            logInfo() << "Capturing image" << i;
            for (size_t cam = 0; cam < nbCameras; ++cam)
            {
                if (!camDevices[cam]->get(camImages[cam][i]))
                {
                    logError() << "Could not get an image from camera" << cam;

                    return -1;
                }
            }
        }
    }
    else
    {
        // makes sure no one is in synchronized mode ...
        projDevice->setSynchronized(false);
        for (size_t i = 0; i < nbCameras; ++i)
        {
            camDevices[i]->setSynchronized(false);
        }

        for (size_t i = 0; i < nbImages; ++i)
        {
            Mat img = projGenerator->get();
            if (!projDevice->put(img))
            {
                logError() << "Could not put an image in projector";

                return -1;
            }

            for (size_t cam = 0; cam < nbCameras; ++cam)
            {
                if (!camDevices[cam]->get(camImages[cam][i]))
                {
                    logError() << "Could not get an image from camera" << cam;

                    return -1;
                }

                ostringstream oss;
                oss << camWinName << cam;
                imshow(oss.str(), camImages[cam][i]);
                waitKey(1);
            }
        }
    }

    // subtracting black noise
    for (size_t cam = 0; cam < nbCameras; ++cam)
    {
        for (size_t i = 0; i < nbImages; ++i)
        {
            camImages[cam][i] -= blacks[cam];
        }
    }

    logInfo() << "Saving projector images";
    projGenerator->reset();
    ProgressLogger logProg = logProgress(static_cast<unsigned int>(nbImages));
    for (size_t i = 0; i < nbImages; ++i)
    {
        string projOutPath = format(projOutPattern.c_str(), i);
        imwrite(projOutPath, projGenerator->get());

        logProg++;
    }

    logInfo() << "Saving camera images";
    logProg.setMaxValue(static_cast<unsigned int>(nbImages * nbCameras));
    for (size_t cam = 0; cam < nbCameras; ++cam)
    {
        for (size_t i = 0; i < nbImages; ++i)
        {
            string camOutPath = format(camOutPattern.c_str(), cam, i);
            imwrite(camOutPath, camImages[cam][i]);
            logProg++;
        }
    }

    // calling them explicitly since multi threaded drivers seems to never exit otherwise...
    projDevice.release();
    for (size_t cam = 0; cam < nbCameras; ++cam)
    {
        camDevices[cam].release();
    }

    return 0;
}
