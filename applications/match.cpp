/*
 * License Agreement for CodedLight
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <xtclap/CmdLine.h>
#include <mvg/logger.h>
#include "../patterns/generator.h"
#include "../patterns/matcher.h"

using namespace cv;
using namespace std;
using namespace TCLAP;
using namespace cl3ds;

int main(int   argc,
         char *argv[])
{

    CmdLine cmd("Creates a camera projector correspondence map");

    ValueArg<string> configPathArg("m", "config-path-matcher",
                                   "Path to the pattern generator configuration",
                                   true, "", "string", cmd);

    MultiArg<string> outputsArg("o", "output-path",
                                "Path to save images for corresponding named mats:\n",
                                true, "string", cmd);

    MultiArg<string> mapsArg("k", "image-key",
                             "Keys of image (in MatTable) to save\n",
                             true, "string", cmd);

    ValueArg<string> camMaskPathArg("", "cam-mask",
                                    "Path to the camera mask",
                                    false, "", "string", cmd);

    ValueArg<string> projMaskPathArg("", "proj-mask",
                                     "Path to the camera mask",
                                     false, "", "string", cmd);

    ValueArg<string> camParamPathArg("", "cam-param",
                                     "Path to the camera geometric parameters",
                                     false, "", "string", cmd);

    ValueArg<string> projParamPathArg("", "proj-param",
                                      "Path to the projector geometric parameters",
                                      false, "", "string", cmd);

    cmd.parse(argc, argv);

    string         configPath    = configPathArg.getValue();
    string         camMaskPath   = camMaskPathArg.getValue();
    string         projMaskPath  = projMaskPathArg.getValue();
    vector<string> outputPaths   = outputsArg.getValue();
    vector<string> mapNames      = mapsArg.getValue();
    string         camParamPath  = camParamPathArg.getValue();
    string         projParamPath = projParamPathArg.getValue();

    if (outputPaths.size() != mapNames.size())
    {
        logError() << "There should be as many paths as named mats";
    }

    if (configPath == "")
    {
        logError() << "No config file for the matcher given";
    }

    Ptr<Matcher>   matcher;
    Ptr<Generator> camGenerator;
    Ptr<Generator> projGenerator;

    FileStorage fs(configPath, FileStorage::READ);
    if (!fs.isOpened())
    {
        logError() << "Config file could not be found";
    }

    fs["matcher"] >> matcher;
    if (matcher.empty())
    {
        logError() << "The matcher can not be created.";
    }

    fs["camGenerator"] >> camGenerator;
    if (camGenerator.empty())
    {
        logError() << "The generator for the camera can not be created.";
    }

    fs["projGenerator"] >> projGenerator;
    if (projGenerator.empty())
    {
        logError() << "The generator for the camera can not be created.";
    }

    Mat camMask;
    if (!camMaskPath.empty())
    {
        camMask = imread(camMaskPath, -1);
        if (camMask.empty())
        {
            logError() << "Could not load the camera mask at path :" << camMaskPath;
        }
    }

    Mat projMask;
    if (!projMaskPath.empty())
    {
        projMask = imread(projMaskPath, -1);
        if (projMask.empty())
        {
            logError() << "Could not load the projector mask at path :" << projMaskPath;
        }
    }

    CameraGeometricParameters camParameters;
    if (!camParamPath.empty())
    {
        camParameters.read(camParamPath);
    }

    CameraGeometricParameters projParameters;
    if (!projParamPath.empty())
    {
        projParameters.read(projParamPath);
    }

    MatTable result = matcher->match(camGenerator, projGenerator,
                                     camMask, projMask,
                                     camParameters, projParameters);

    for (vector<string>::const_iterator pathIt = outputPaths.begin(),
         pathItE = outputPaths.end(), mapIt = mapNames.begin();
         pathIt != pathItE; ++pathIt, ++mapIt)
    {
        Mat m = result[*mapIt];
        if (!m.empty())
        {
            imwrite(*pathIt, m);
        }
        else
        {
            logWarning() << "No such map to save :" << *mapIt;
        }
    }

    return 0;
}
