/*
 * License Agreement for CodedLight
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <xtclap/CmdLine.h>
#include "../patterns/generators.h"

using namespace cv;
using namespace std;
using namespace TCLAP;
using namespace cl3ds;

int main(int   argc,
         char *argv[])
{
    CmdLine cmd("Create a configuration file for a generator to be used with "
                "applications that needs a generator xml description input.\n"
                "In interactive mode, questions will be asked to determine "
                "the generator configuration.\n"
                "The configuration file will be saved in <outputPath>.");

    ValueArg<string> outputPathArg("o", "output-path",
                                   "Path where the configuration file will be "
                                   "saved (default: <method>_generator.xml)",
                                   false, "", "string", cmd);

    ValueArg<string> methodArg("m", "method",
                               "Generator method name, if not given, it "
                               "will be asked",
                               false, "", "string", cmd);

    SwitchArg defaultArg("d", "default-values",
                         "Generate a default configuration file for the "
                         "selected generator without asking the user",
                         cmd, false);

    SwitchArg listArg("l", "list-generators",
                      "Print the list of valid generators and exit",
                      cmd, false);

    cmd.parse(argc, argv);

    bool   defaultValues  = defaultArg.getValue();
    bool   listGenerators = listArg.getValue();
    string genMethod      = methodArg.getValue();
    string outPath        = outputPathArg.getValue();

    vector<string> genMethods = Generator::classFactory()->keys();
    if (listGenerators)
    {
        logInfo(false) << "Available generator methods are : ";
        for (vector<string>::const_iterator it = genMethods.begin(),
             itE = genMethods.end(); it != itE; ++it)
        {
            logInfo(false) << "\t" << *it;
        }

        return 0;
    }

    if (defaultValues && genMethod.empty())
    {
        logError() << "No generator method provided, but default values flag "
            "requires it.";
    }

    istream *is = (!defaultValues ? &cin : 0);
    ostream *os = (!defaultValues ? &cout : 0);

    Ptr<Generator> gen;
    configure(gen, is, os, genMethod);
    if (gen.empty())
    {
        logError() << "The generator could not be configured!";
    }

    if (outPath.empty())
    {
        outPath = format("%s_generator.xml", gen->objectName().c_str());
    }

    FileStorage fs(outPath, FileStorage::WRITE);
    if (!fs.isOpened())
    {
        logError() << "Config file could not be opened";
    }
    fs << "generator" << gen;

    return 0;
}

