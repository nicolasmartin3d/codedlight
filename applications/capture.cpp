/*
 * License Agreement for CodedLight
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <opencv2/highgui/highgui.hpp>
#include <xtclap/CmdLine.h>
#include <mvg/logger.h>

#include "../devices/camera_device.h"

using namespace cv;
using namespace std;
using namespace TCLAP;
using namespace cl3ds;

int main(int   argc,
         char *argv[])
{

    CmdLine cmd("Capture the given number of images from a camera or stop "
                "when the 'q' key is hit.");

    ValueArg<string> cameraConfigPathArg("C", "camera-config-path",
                                         "Path to the camera configuration",
                                         true, "", "string", cmd);

    ValueArg<int> nbImagesArg("n", "nb-images",
                              "Number of images to grab (500 if not given)",
                              false, 500, "int", cmd);

    ValueArg<string> outFormatPathArg("o", "output-format-path",
                                      "Path for the captured images. Must include a %d format that will be filled\n"
                                      "with the image index. If not given, images will be saved in cam_%04d.png.\n",
                                      false, "cam_%04d.png", "string", cmd);

    cmd.parse(argc, argv);

    size_t nbImages         = static_cast<size_t>(nbImagesArg.getValue());
    string outFormatPath    = outFormatPathArg.getValue();
    string cameraConfigPath = cameraConfigPathArg.getValue();

    const string camWinName = "cam";
    namedWindow(camWinName /*, CV_GUI_EXPANDED*/);

    // Camera setup
    FileStorage fs(cameraConfigPath, FileStorage::READ);
    if (!fs.isOpened())
    {
        logError() << "Config file at path " << cameraConfigPath <<
            " could not be opened";
    }

    Ptr<CameraDevice> device;
    fs["device"] >> device;
    if (device.empty())
    {
        logError() << "Could not read camera";
    }

    vector<Mat> camImages(nbImages);
    size_t      count = 0;
    for (size_t i = 0; i < nbImages; ++i, ++count)
    {
        if (device->get(camImages[i]))
        {
            imshow(camWinName, camImages[i]);
            if (waitKey(1) == 'q') { break; }
        }
        else
        {
            logError() << "Could not get an image from camera";
        }
    }

    ProgressLogger logProg = logProgress(static_cast<unsigned int>(count));
    for (size_t i = 0; i < count; ++i)
    {
        string camOutPath = format(outFormatPath.c_str(), i);
        imwrite(camOutPath, camImages[i]);
        logProg++;
    }
}
