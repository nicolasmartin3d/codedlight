/*
 * License Agreement for CodedLight
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <limits>
#include <opencv2/highgui/highgui.hpp>
#include <xtclap/CmdLine.h>
#include "../patterns/generators.h"

using namespace cv;
using namespace std;
using namespace TCLAP;
using namespace cl3ds;

int main(int   argc,
         char *argv[])
{
    CmdLine cmd("Projects patterns on the screen and optionally writes it "
                "to the disk.");

    ValueArg<int> fpsArg("f", "fps",
                         "Number of images to display per seconds "
                         "(if f < 0 or f > 1000, no images are displayed).",
                         false, 2, "int", cmd);

    ValueArg<string> configPathArg("g", "config-path",
                                   "Path to the pattern generator configuration",
                                   true, "", "string", cmd);

    ValueArg<string> outputPatternArg("o", "output-pattern",
                                      "Path to save the generated patterns :\n",
                                      false, "", "string", cmd);

    cmd.parse(argc, argv);

    string       configPath  = configPathArg.getValue();
    string       outPattern  = outputPatternArg.getValue();
    int          fps         = fpsArg.getValue();
    const string projWinName = "proj";

    Ptr<Generator> generator;

    FileStorage fs(configPath, FileStorage::READ);
    if (!fs.isOpened())
    {
        logError() << "Config file could not be found";
    }
    fs["generator"] >> generator;

    size_t i = 0;
    generator->reset();
    while (generator->hasNext())
    {
        const Mat &img = generator->get();

        if ((fps > 0) && (fps < 1000))
        {
            imshow(projWinName, img);
            waitKey(static_cast<int>(1000 / fps));
        }
        if (outPattern != "")
        {
            string str = format(outPattern.c_str(), i);
            imwrite(str, img);
        }
        ++i;
    }

    return 0;
}

