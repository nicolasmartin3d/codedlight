/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PINHOLE_REPR_ERROR_H
#define PINHOLE_REPR_ERROR_H

#include <ceres/rotation.h>
#include <mvg/mvg.h>
#include "../core/object_p.h"
#include "calib.h"

namespace cl3ds
{
    /** This class is used to convert a Camera to Ceres double
     *  arrays internal representation and vice versa. */
    struct CL3DS_DECLSPEC PinholeProjectionParameters
    {
        PinholeProjectionParameters(
            const CameraGeometricParameters &cam=CameraGeometricParameters());
        operator CameraGeometricParameters() const;

        double rot[4];
        double trans[3];
        double focalX[1];
        double aspectRatio[1];
        double principalPoint[2];
        double skew[1];
        double distCoeffs[4];
        int    imageWidth;
        int    imageHeight;
    };

    /** Reprojection error for pinhole cameras as used in Ceres. */
    struct CL3DS_DECLSPEC PinholeProjection
    {
        PinholeProjection(const cv::Point2d &imagePoint);

        /** Actual error computation from every parameter optimized. */
        template <typename T>
        bool operator()(const T *const rot,
                        const T *const trans,
                        const T *const focalX,
                        const T *const aspectRatio,
                        const T *const principalPoint,
                        const T *const skew,
                        const T *const distCoeffs,
                        const T *const pt3d,
                        T             *residuals) const
        {
            // Rotate and translate the point
            T P[3];
            ceres::QuaternionRotatePoint(rot, pt3d, P);
            for (int i = 0; i < 3; ++i) { P[i] += trans[i]; }

            // Project in the normalized frame
            T x = P[0] / P[2];
            T y = P[1] / P[2];

            T u, v;
            // Apply second and fourth order radial distortion
            const T &k1 = distCoeffs[0];
            const T &k2 = distCoeffs[1];
            const T &p1 = distCoeffs[2];
            const T &p2 = distCoeffs[3];
            T        r2 = x * x + y * y;
            T        r4 = r2 * r2;
            u = x *
                (T(1.0) + k1 * r2 + k2 *
                 r4) + T(2.0) * p1 * x * y + p2 * (r2 + T(2.0) * x * x);
            v = y *
                (T(1.0) + k1 * r2 + k2 *
                 r4) + p1 * (r2 + T(2.0) * y * y) + T(2.0) * p2 * x * y;

            T xp, yp;
            // Project in the image frame
            const T &fx = focalX[0];
            T        fy = fx * aspectRatio[0];
            const T &cx = principalPoint[0];
            const T &cy = principalPoint[1];
            const T &s  = skew[0];
            xp = fx * u + s * v + cx;
            yp = fy * v + cy;

            // The error is the difference between the predicted and observed position
            residuals[0] = xp - T(pt2d[0]);
            residuals[1] = yp - T(pt2d[1]);

            return true;
        }

        double pt2d[2];
    };

    /** Camera-projector reprojection error when using several steps as is the case
     *  when doing linear stage calibration. */
    struct CL3DS_DECLSPEC CameraProjectorLinearStageProjection : public PinholeProjection
    {
        CameraProjectorLinearStageProjection(const cv::Point2d &imagePoint,
                                             const cv::Point2d &gridPoint,
                                             double             zmin,
                                             double             zmax,
                                             int                zId);

        template <typename T>
        bool operator()(const T *const rot,
                        const T *const trans,
                        const T *const focalX,
                        const T *const aspectRatio,
                        const T *const principalPoint,
                        const T *const skew,
                        const T *const distCoeffs,
                        const T *const scale,
                        const T *const zstep,
                        T             *residuals) const
        {
            // Rotate and translate the point
            T P[3] = {
                T(gridPt[0]) * scale[0],
                T(gridPt[1]) * scale[0],
                zmin + T(zId) * zstep[0]
            };

            return PinholeProjection::operator()(rot,
                                                 trans,
                                                 focalX,
                                                 aspectRatio,
                                                 principalPoint,
                                                 skew,
                                                 distCoeffs,
                                                 P,
                                                 residuals);
        }

        double gridPt[2];
        int    zId;
        double zmin, zmax;
    };

    /** Base class for an opaque implementation of a CameraDevice. */
    struct CL3DS_DECLSPEC CameraProjectorCalibrator::AbstractPrivate :
        public Object::AbstractPrivate
    {
        AbstractPrivate(CameraProjectorCalibrator *calibrator);
        ~AbstractPrivate();
    };
}
#endif
