/*
 * CodedLight - A library to compute 3D meshes from images using coded light methods.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of CodedLight.
 *
 * CodedLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CodedLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CodedLight.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CALIB_H
#define CALIB_H

#include <map>
#include <vector>
#include <opencv2/core/core.hpp>
#include <mvg/mvg.h>
#include "../core/factory.h"
#include "../core/export.h"

namespace cl3ds
{
    /** This class controls the options that can be set to fix some variables during the
     *  calibration. */
    struct CL3DS_DECLSPEC PinholeProjectionOption
    {
        PinholeProjectionOption();
        virtual ~PinholeProjectionOption();

        /** Named parameters. */
        PinholeProjectionOption & skewFixed();
        PinholeProjectionOption & distCoeffsFixed();

        bool m_structureFixed;
        bool m_motionFixed;
        bool m_focalXFixed;
        bool m_aspectRatioFixed;
        bool m_principalPointFixed;
        bool m_skewFixed;
        bool m_distCoeffsFixed;
        bool m_rotFixed;
        bool m_transFixed;
    };

    /** This class controls the options that can be set to fix some variables during the
     *  calibration. */
    struct CL3DS_DECLSPEC CameraProjectorLinearStageProjectionOption :
        public PinholeProjectionOption
    {
        CameraProjectorLinearStageProjectionOption(
            const PinholeProjectionOption &opt=PinholeProjectionOption());
        ~CameraProjectorLinearStageProjectionOption();

        bool m_zStepFixed;
        bool m_scaleFixed;
    };

    class CL3DS_DECLSPEC CameraProjectorCalibrator : public Object
    {
        public:
            static Factory<std::string, CameraProjectorCalibrator> * classFactory();

            virtual ~CameraProjectorCalibrator();

        protected:
            struct AbstractPrivate;
            CameraProjectorCalibrator(AbstractPrivate *ptr);

        protected:
            void setPrivateImplementation(AbstractPrivate *pimpl);
            AbstractPrivate * privateImplementation();
            const AbstractPrivate * privateImplementation() const;

        private:
            struct Private;
    };

    /** Camera-projector calibration using pictures of a grid of fiducial
     *  markers taken at different steps using a linear stage. */
    class CL3DS_DECLSPEC CameraProjectorLinearStageCalibrator :
        public CameraProjectorCalibrator
    {
        public:
            CameraProjectorLinearStageCalibrator(cv::Size camSize=cv::Size(),
                                                 cv::Size projSize=cv::Size());
            ~CameraProjectorLinearStageCalibrator();

            void setGridPoints(const std::map<int, cv::Point2f> &gridPoints);

            void setScale(double scale);
            void setZRange(double zmin,
                           double zmax,
                           double zstep);

            void setResolutions(cv::Size camSize,
                                cv::Size projSize);

            void addStep(int step,
                         const std::map<int, cv::Point2f> &camPoints,
                         const cv::Mat &camMatch,
                         const cv::Mat &camMask=cv::Mat());

            void setData(const std::vector< std::vector< cv::Point2d > > &imagePoints,
                         const std::vector< std::vector<int> >           &tagIds,
                         const std::vector< std::vector<int> >           &zIds);

            double calibrate(CameraGeometricParameters                  &camParameters,
                             CameraGeometricParameters                  &projParameters,
                             CameraProjectorLinearStageProjectionOption &camOption,
                             CameraProjectorLinearStageProjectionOption &projOption,
                             const std::string                          &pointsOutputPattern=std::string(),
                             bool                                        verbose=true);

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("linear_stage_calibrator")
    };

    /** Camera-projector calibration using the correspondence maps computed
     *  with any kind of coded light method. Several correspondence maps can
     *  be used (computed from scenes with different objects) as long as the camera
     *  projector geometry stays the same. */
    class CL3DS_DECLSPEC CameraProjectorCorrespondenceMapCalibrator :
        public CameraProjectorCalibrator
    {
        public:
            CameraProjectorCorrespondenceMapCalibrator(const cv::Mat &camK=cv::Mat(),
                                                       const cv::Mat &projK=cv::Mat(),
                                                       cv::Size       camSize=cv::Size(),
                                                       cv::Size       projSize=
                                                           cv::       Size());
            ~CameraProjectorCorrespondenceMapCalibrator();

            void setInitialParameters(const cv::Mat &camK,
                                      const cv::Mat &projK,
                                      cv::Size       camSize,
                                      cv::Size       projSize);

            void addCorrespondenceMap(const cv::Mat &match,
                                      const cv::Mat &mask=cv::Mat(),
                                      int            maxPoints=5000);

            double calibrate(CameraGeometricParameters &camParameters,
                             CameraGeometricParameters &projParameters,
                             PinholeProjectionOption   &camOption,
                             PinholeProjectionOption   &projOption,
                             const std::string         &pointsOutputPattern=std::string(),
                             bool                       verbose=true);

            CL3DS_PRIV_IMPL
            CL3DS_CLASS_NAME("match_calibrator")
    };
}
#endif
